<!DOCTYPE html>
<html lang="en">
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<head>
  <?php 
if($page_title == "Success"){
    ?>
   <meta http-equiv="refresh" content="3;url= <?php echo base_url(); ?>">
   <?php
}
?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo base_url();?>">
    <title><?php echo $header_title;?></title>
    <meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
    <meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/custom-animation.css">
    <link rel="stylesheet" href="assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="assets/js/plugin/magnific/magnific-popup.css">
      <link href="assets/js/plugin/revolution/css/settings.css" rel="stylesheet" type="text/css">
    <link href="assets/js/plugin/revolution/css/layers.css" rel="stylesheet" type="text/css">
    <link href="assets/js/plugin/revolution/css/navigation.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/custom-animation.css">
    <link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="assets/css/error.css">
    <link rel="stylesheet" href="assets/css/inline.css">
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
    <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
                            <li><a href="rentBicycle"><b>RENT BICYCLE</b></a></li>
                                <li> <a href="cyclothonRegister"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: <?php echo nagarCyclingEmail;?></a></li>
                                <li>CONTACT: <?php echo nagarCyclingContact;?></li>
                                <li>
                                    <ul>
                                         <li><a href="<?php echo facebookURL;?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="<?php echo instagramURL;?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!--Banner section start-->
    <div class="cy_bread_wrapper" style="<?php if($page_title == "home"){?>display:none;<?php } ?>" st>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1><?php echo $page_title;?></h1>
                </div>
            </div>
        </div>
    </div>
    <!--Menus Start-->
    <?php
    $home_active="";
    $registration_active="";
     $contact_active="";
     $routes_active="";
    $about_active="";
    $join_active="";
      $events_active="";
      $sponsors_active="";
    switch($page_title){
        case "home":
         $home_active="active";
         break;
         case "Registration":
         $registration_active="active";
         break;
          case "Contact":
         $contact_active="active";
         break;
         case "Routes":
         $routes_active="active";
         break;
          case "RENT A BICYCLE":
         $registration_active="active";
         break;
         case "About":
         $about_active="active";
         break;
         case "Join the club":
         $join_active="active";
         break;
         case "Rides":
         $registration_active="active";
         break;
         case "Events":
         $events_active="active";
         break;
         case "Our Sponsor":
         $sponsors_active="active";
         break;
    }
    
    ?>
  <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <?php
            $this->config->load('image',TRUE); 
            $ahmednagarCyclingClubLogo=$this->config->item('ahmednagarCyclingClubLogo','image');  
            ?>
            <a href="<?php echo base_url();?>"><img src="<?php echo $ahmednagarCyclingClubLogo;?>" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="#" class="<?php echo $home_active;?>">home</a></li>
                                <li><a href="about" class="<?php echo $about_active;?>">about</a></li>
                                 <li><a href="join" class="<?php echo $join_active;?>">join</a></li>
                                <li class="dropdown"><a href="" class="<?php echo $registration_active;?>">Register</a>
                                    <ul class="sub-menu">
                                        <li> <a href="cyclothonRegister"><b>Register</b></a></li>
                                        <li><a href="rentBicycle"><b>Rent Bicycle</b></a></li>
                                        <li> <a href="rides"><b>Rides</b></a></li>
                                        
                                    </ul>
                                </li>
                                 <li><a href="routes" class="<?php echo $routes_active;?>">routes</a></li>
                                  <li><a href="events" class="<?php echo $events_active;?>">events</a></li>
                                <li><a href="sponsors" class="<?php echo $sponsors_active;?>">sponsors</a></li>
                                <li><a href="contact" class="<?php echo $contact_active;?>">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>