<?php
$this->load->view('common/header.php');
?>

   
    <!--About section start-->
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
			
                <div class="col-lg-12 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">IMPORTANT INSTRUCTIONS</h1>
                        

						<p>•	Carry water with you at all times and refill your bottle at the nearest hydration point.</p>
						<p>•	Throw your trash in the Garbage Bin placed at each hydration point. PLEASE DO NOT LITTER OUR BEAUTIFUL CITY.</p>
						<p>•	Smile at the photographers who will capture your pictures while cycling. Do not take any risks by trying stunts.</p>
						<p>•	Look out for signage/arrows and volunteers along your route.</p>
						<p>•	Recheck your fitness levels. If you do not feel well, opt out or just take it easy.</p>
						<p>•	If your cycle gets a flat, call the emergency contact number on your bib.</p>
						<p>•	Eat healthy, sleep well and hydrate before the event.</p>
						<p>•	There are no baggage counters, so keep your stuff with you or a friend.</p>
						<p>•	Wear your bib number on your chest for identification.</p>
						<p>•	After reaching the finishing point, stretch again and rehydrate.</p>


                       <!-- <a href="#" class="cy_button">read more</a>-->
                    </div>
                </div>
               
            </div>
        </div>
    </div>
	
	  <div class="cy_price_wrapper">
        <div class="container">
          
            <div class="row">
			 <div class="cy_about_data">
                <h1 class="cy_heading">TERMS & CONDITIONS</h1>

				<p style="color:#fff;">•	Please choose the event category carefully based on your fitness level.</p>
				<p style="color:#fff;">•	Participant, once registered under any category, cannot cancel his/her registration. There will be no refund of registration fees paid by the participant in case of his/her failure to participate in the event. This is non-negotiable.</p>
				<p style="color:#fff;">•	This is not a race but each distance category must be completed within the set time limit.</p>
				<p style="color:#fff;">•	The registration form must be completed with accurate and truthful information.</p>
				<p style="color:#fff;">•	The routes are subject to change based on anticipated/unanticipated circumstances and weather conditions.</p>
				<p style="color:#fff;">•	Helmet, front headlight, and rear lights/blinkers are compulsory for all cycles. Cyclists without helmets will not be allowed to participate.</p>
				<p style="color:#fff;">•	Cyclists assume all risks associated with participating in the Cyclothon including, but not limited to, falls, contact with other participants, the effects of the weather, including high heat or humidity, traffic and the condition of the road, arson or terrorist threats and all other risks associated with a public event. </p>
				<p style="color:#fff;">•	You understand that the Cyclothon takes place on roads where there is vehicular traffic and though our volunteers take care of managing the traffic, you are responsible for watching the traffic and cycling in accordance with all safety rules and standards.  </p>
				<p style="color:#fff;">•	You agree that the Ahmednagar Cycling Club and its volunteers will not be liable for any loss, damage, illness, or injury that might occur as a result of your participation in the event. </p>
				<p style="color:#fff;">•	You agree to abide by the rules and instructions provided by the organizers in the best interest of your health and safety. </p>
				<p style="color:#fff;">•	You agree to stop cycling if instructed by one of the event organizers. </p>
				<p style="color:#fff;">•	Once registered online fees are not refundable. </p>

			</div>	
            </div>
        </div>
    </div>
  

	
    <!--Footer section start-->
	<?php
$this->load->view('common/footer');
        ?>