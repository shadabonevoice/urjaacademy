<?php
function getTotalRegistered() {
    $CI = get_instance();
    $CI->load->model('Common_model');    
    $registered_data = $CI->Common_model->getTotalRegistered();
    return $registered_data;
}
function getTotalRentBicycle() {
    $CI = get_instance();
    $CI->load->model('Common_model');    
    $bicycle_rent_data = $CI->Common_model->getTotalRentBicycle();
    return $bicycle_rent_data;
}
function getTotalRegisteredPromo() {
    $CI = get_instance();
    $CI->load->model('Common_model');    
    $bicycle_rent_data = $CI->Common_model->getTotalRegisteredPromo();
    return $bicycle_rent_data;
}
