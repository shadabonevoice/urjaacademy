<?php
$this->load->view('common/header.php');
?>

   <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!--Gallery section css start-->
    <div class="cy_gallery_wrapper cy_section_padding padder_bottom100">
        <div class="container">
          
            <div class="row">
            <div class="col-lg-12 col-md-12">
				
				<div class="row">
					<div class="col-lg-6 col-md-12">
					<center><h4 class="cy_heading">Title Sponsor</h4></center>
						<div class="row">
						<span  class="col-md-3"></span>
							<div class="col-lg-6 col-md-6">
								<div class="cy_gal_img">
									<img src="assets/images/sponsors2/giants_starknn01.png" alt="sponsors" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="assets/images/sponsors2/giants_starknn01.png" class="fa fa-search"></a>
										
									</div>
								</div>
								
							</div>
						<span  class="col-md-3"></span>
						</div>
						
					</div>	
					
					
				<div class="col-lg-6 col-md-12">
				<center><h4 class="cy_heading">Associate Partner</h4></center>
				<center>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="cy_gal_img">
                                <img src="assets/images/sponsors2/lakme_salon02.png" alt="sponsors" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="assets/images/sponsors2/lakme_salon02.png" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="cy_gal_img">
                                <img src="assets/images/sponsors2/patanjali02.png" alt="sponsors" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="assets/images/sponsors2/patanjali02.png" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                       
                    </div>
					</center>
                </div>
				</div>
					
                </div>
                
               
                 
				<div class="col-lg-12 col-md-12">
					
					
						<div class="row">
							<div class="col-lg-3 col-md-6">
							<center><h4 class="cy_heading">Fitness Partner</h4></center>
								<div class="cy_gal_img">
									<img src="assets/images/sponsors2/golds_gym03.png" alt="sponsors" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="assets/images/sponsors2/golds_gym03.png" class="fa fa-search"></a>

									</div>
								</div>
							</div>
							
							<div class="col-lg-3 col-md-6">
							<center><h4 class="cy_heading">Radio Partner</h4></center>
								<div class="cy_gal_img">
									<img src="assets/images/sponsors2/radi_partner04.png" alt="sponsors" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="assets/images/sponsors2/radi_partner04.png" class="fa fa-search"></a>

									</div>
								</div>
							</div>
							
							<div class="col-lg-3 col-md-6">
							<center><h4 class="cy_heading">Energy Drink Partner</h4></center>
								<div class="cy_gal_img">
									<img src="assets/images/sponsors2/energy_drink05.png" alt="sponsors" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="assets/images/sponsors2/energy_drink05.png" class="fa fa-search"></a>

									</div>
								</div>
							</div>
							
							<div class="col-lg-3 col-md-6">
							<center><h4 class="cy_heading">Hydration Partner</h4></center>
								<div class="cy_gal_img">
									<img src="assets/images/sponsors2/oxycool06.png" alt="sponsors" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="assets/images/sponsors2/oxycool06.png" class="fa fa-search"></a>

									</div>
								</div>
							</div>
							
						</div>
						
                </div>
                
				 
			 <div class="col-lg-12 col-md-12">
				
				<div class="row">
					<div class="col-lg-6 col-md-12">
					<center><h4 class="cy_heading">Powered By</h4></center>
					<div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="cy_gal_img">
                                <img src="assets/images/sponsors2/ilovengr07.png" alt="sponsors" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="assets/images/sponsors2/ilovengr07.png" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="cy_gal_img">
                                <img src="assets/images/sponsors2/IMA07.png" alt="sponsors" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="assets/images/sponsors2/IMA07.png" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                       
                    </div>
						
					</div>	
					
					
				<div class="col-lg-6 col-md-12">
				<center><h4 class="cy_heading">Supported By</h4></center>
				<center>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="cy_gal_img">
                                <img src="assets/images/sponsors2/nagar_rising08.png" alt="sponsors" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="assets/images/sponsors2/nagar_rising08.png" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="cy_gal_img">
                                <img src="assets/images/sponsors2/sol_sisters08.png" alt="sponsors" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="assets/images/sponsors2/sol_sisters08.png" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                       
                    </div>
					</center>
                </div>
				</div>
					
                </div>
				
				
				
				
				 <div class="col-lg-12 col-md-12">
					
						<div class="row">
						<span  class="col-md-4"></span>
							<div class="col-lg-3 col-md-6">
							<center><h4 class="cy_heading">Medical Partner</h4></center>
								<div class="cy_gal_img">
									<img src="assets/images/sponsors2/bhagirathi09.png" alt="sponsors" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="assets/images/sponsors2/bhagirathi09.png" class="fa fa-search"></a>
									</div>
								</div>
							</div>
							<span  class="col-md-5"></span>
						</div>
						
					</div>	
            
				 
				 
				</div>
           
        </div>
    </div>
    <!--Footer section start-->
<?php
$this->load->view('common/footer');
        ?>