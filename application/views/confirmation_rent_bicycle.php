<?php
$this->load->view('common/header.php');
?>
    <!-- checkout section start -->
    <div class="cy_checkout_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 offset-lg-2">
                    <div class="cy_checkout_box">
                        <ul id="progressbar">
                            <li class="active">Checkout Conformation</li>
                         <!--   <li>Payment</li>
                            <li>Receipt</li> -->   
                        </ul>


					<form action="<?php echo $action; ?>/_payment" method="post" id="payuForm" name="payuForm">
					 <div class="woocommerce-billing-fields step">
		                <input type="hidden" name="key" value="<?php echo $mkey; ?>" />
		                <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
		                <input type="hidden" name="txnid" value="<?php echo $tid; ?>" />
						<!--Rest Fields-->
						
						<input type="hidden" name="age" value="<?php echo $age; ?>" />
						
						
		               
						<!--Rest Fields-->
						
		                <div class="form-group">
		                    <label class="control-label">Total Payable Amount</label>
		                    <input class="form-control" name="amount" value="<?php echo $amount; ?>"  readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Your Name</label>
		                    <input class="form-control" name="firstname" id="firstname" value="<?php echo $name; ?>" readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Email</label>
		                    <input class="form-control" name="email" id="email" value="<?php echo $mailid; ?>" readonly/>
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Phone</label>
		                    <input class="form-control" name="phone" value="<?php echo $phoneno; ?>" readonly />
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Gender</label>
							 <input class="form-control" name="productinfo" value="<?php echo $productinfo; ?>" readonly />
		                 
		                </div>
		                <div class="form-group">
		                    <label class="control-label">Address</label>
		                    <input class="form-control" name="address1" value="<?php echo $address; ?>" readonly/>     
		                </div>
		                <div class="form-group">
		                    <input name="surl" value="<?php echo $sucess; ?>" size="64" type="hidden" />
		                    <input name="furl" value="<?php echo $failure; ?>" size="64" type="hidden" />  
		                    <!--for test environment comment  service provider   -->
		                    <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
		                    <input name="curl" value="<?php echo $cancel; ?> " type="hidden" />
		                </div>
		                <div class="form-group float-right">
		                	<input type="submit" name="paynow" id="paynow" value="Pay Now" class="btn btn-success" />
		                </div>
						</div>
		            </form> 
        		
					
        		</div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->
	  <?php
$this->load->view('common/footer');
        ?>