<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcomerentbicycle extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('upload');
		$this->load->library('email');
		$this->load->helper('form');
		$this->load->library('session');  
		$this->load->model('common_model');
		
    }

	public function index()
	{
		
		$data['header_title']="Ahmednagar Cyclothon - Get a bike, get a life.";
		$data['page_title']="RENT A BICYCLE";
		$this->load->view('registration_rent_bicycle',$data);	
		//$this->load->view('footer');	
		
        	
	}

	public function check()
	{
		
		$amount =  $this->input->post('payble_amount');
	    $product_info = $this->input->post('product_info');
	    $customer_name = $this->input->post('customer_name');
	    $customer_emial = $this->input->post('customer_email');
	    $customer_mobile = $this->input->post('mobile_number');
	    $customer_address = $this->input->post('customer_address');
		$age = $this->input->post('age');
	
		
	    
	    	//payumoney details
	    
			$MERCHANT_KEY = "v98vlTWB"; //change  merchant with yours
	        $SALT = "hOTRLB5LMS";  //change salt with yours 
		
		
					//Live
	        //$MERCHANT_KEY = "v98vlTWB"; //change  merchant with yours
	       // $SALT = "hOTRLB5LMS";  //change salt with yours 

	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        
	         $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
	         $hash = strtolower(hash('sha512', $hashstring));
	         
	       $success = base_url() .'index.php/Statusrentbicycle';  
	        $fail = base_url() .'index.php/Statusrentbicycle';
	        $cancel = base_url() .'index.php/Statusrentbicycle';
	        
	       // print_r($_POST);exit;
	       $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'productinfo' => $product_info,
	            'mailid' => $customer_emial,
	            'phoneno' => $customer_mobile,
	            'address' => $customer_address,
				'age' => $age,
	            'action' => "https://secure.payu.in", //for live change action  https://secure.payu.in
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel            
	        );
			
			$this->session->set_userdata($data);
			//print_r($data); exit;
	        $this->load->view('confirmation_rent_bicycle',$data);  
			$this->load->view('footer');			
     
	}

	public function help()
	{
		
		$this->load->view('help');
		$this->load->view('footer');
	}
}
