<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cyclothon_register extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('upload');
		$this->load->library('email');
		$this->load->helper('form');
		$this->load->library('session');  
		$this->load->model('common_model');
		
    }

	public function index()
	{
		$data['header_title']="Ahmednagar Cyclothon - Get a bike, get a life.";
		$data['page_title']="Registration";
		$this->load->view('registration',$data);	
			
		
        	
	}

	
}
