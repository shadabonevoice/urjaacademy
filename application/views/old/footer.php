<!--Footer section start-->
    <div class="cy_footer_wrapper cy_section_padding padder_bottom75">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget cy_footer_about">
                        <img src="<?php echo base_url();?>assets/images/footer/footer_logo.png" alt="logo" class="img-fluid"/>
                        <p style="text-align:justify;">
						Ahmednagar Cycling Club actively encourages cyclists of all abilities to meet their personal goals. Whether you cycle for fun, for the health benefits or for the competition, Ahmednagar Cycling Club welcomes you.

						</p>
                    </div>  
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget widget_categories">
					<h1 class="widget-title">Important Links</h1>
						<ul style="list-style-type: none;">
							<li style="padding: 8px 0px;text-align: left;"><a href="<?php echo base_url();?>index.php/join">JOIN</a></li>
							<li style="padding: 8px 0px;text-align: left;"><a href="<?php echo base_url();?>index.php/rides">RIDES</a></li>
							<li style="padding: 8px 0px;text-align: left;"><a href="<?php echo base_url();?>index.php/routes">ROUTE</a></li>
							<li style="padding: 8px 0px;text-align: left;"><a href="cyclothonRegister">REGISTRATION</a></li>
							<li style="padding: 8px 0px;text-align: left;"><a href="<?php echo base_url();?>index.php/terms">TERMS & CONDITIONS</a></li>
						</ul>
					</div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h1 class="widget-title">Venue Details</h1>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url();?>assets/images/svg/map-mark.svg" alt="map-mark"></span>
                        <div class="cy_post_info">
                        <p>IMA Bhavan- Near Railway over bridge, Kalyan Road - Ahmednagar</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url();?>assets/images/svg/phone.svg" alt="phone"></span>
                        <div class="cy_post_info">
                        <p>Contact :- 8308054000</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url();?>assets/images/svg/email.svg" alt="email"></span>
                        <div class="cy_post_info">
                        <p><a href="#">support@nagarcycling.com</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                   <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAhmednagar-Cycling-Club-1900441513602454%2F&tabs=timeline&width=300&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1330903350339636" width="300" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>  
            </div>
        </div>
    </div>
    <!--Bottom footer start-->
    <div class="cy_btm_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <P>Copyright &copy;2018  <b style="color:#ff0000;">Ahmednagar Cycling Club</b>. All Rights Reserved. Developed By<a style="color:#e6c33e;" href="http://www.onevoicetransmedia.com" target="_blank"> OneVoice Transmedia Pvt. Ltd.</a></P>
                </div>
            </div>
        </div>
    </div>
	<!--Go to top start-->
	<div class="cy_go_to">
		<div class="cy_go_top">
			<img src="<?php echo base_url();?>assets/images/go_to_top.png" alt="back to top">
		</div>	
	</div>
    <!--scripts start-->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/js/tether.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.countTo.js"></script>
    <script src="<?php echo base_url();?>assets/js/appear.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugin/owl/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugin/magnific/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>

</body>


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:12:58 GMT -->
</html>