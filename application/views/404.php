<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->
<!-- 
Template Name: Cycling- Html Template
Version: 1.0.0
Author: Kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:13:47 GMT -->
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!--Top bar start-->
   <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
								<li><a href="register.html"><b>Register Here</b></a></li>
                                <li><a href="#">EMAIL: Info@yourmail.com</a></li>
                                <li>PHONE: 18009001010</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!--Banner section start-->
	<div class="cy_bread_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<h1>404</h1>
				</div>
			</div>
		</div>
	</div>
	<!--Menus Start-->
	 <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="index.html"><img src="images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="index.html">home</a></li>
                                <li><a href="about.html">about</a></li>
								 <li><a href="join.html">join</a></li>
								 <li><a href="rides.html">rides</a></li>
								 <li><a href="routes.html">routes</a></li>
								  <li><a href="events.html">events</a></li>
								 
								
                                <li><a href="sponsors.html">sponsors</a></li>
                                <li><a href="contact.html">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
	<!--error section start-->
	<div class="cy_error_wrapper cy_section_padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 offset-lg-2">
					<div class="cy_error_data">
						<h1>404</h1>
						<h2>page you are looking is not found</h2>
						<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.<p>
						<a href="#" class="cy_button">Home Page</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!--Footer section start-->
	<!--Footer section start-->
    <div class="cy_footer_wrapper cy_section_padding padder_bottom75">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget cy_footer_about">
                        <img src="images/footer/footer_logo.jpg" alt="logo" class="img-fluid"/>
                        <p style="text-align:justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text </p>
                    </div>  
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget widget_categories">
					<h1 class="widget-title">Important Links</h1>
						<ul style="list-style-type: none;">
							<li style="padding: 8px 0px;"><a href="join.html">JOIN</a></li>
							<li style="padding: 8px 0px;"><a href="rides.html">RIDES</a></li>
							<li style="padding: 8px 0px;"><a href="routes.html">ROUTES</a></li>
							<li style="padding: 8px 0px;"><a href="register.html">REGISTRATION</a></li>
							<li style="padding: 8px 0px;"><a href="terms.html">TERMS & CONDITIONS</a></li>
						</ul>
					</div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h1 class="widget-title">Venue Details</h1>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="images/svg/map-mark.svg" alt="map-mark"></span>
                        <div class="cy_post_info">
                        <p>Dwarka Lawns, Ahead of IMA Bhavan, Kalyan Road</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="images/svg/phone.svg" alt="phone"></span>
                        <div class="cy_post_info">
                        <p>Contact1 :- +1-512-555-0190</p>
                        <p>Contact2 :- +1-512-555-0190</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="images/svg/email.svg" alt="email"></span>
                        <div class="cy_post_info">
                        <p><a href="#">help@yourmail.com</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                   <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAhmednagar-Cycling-Club-1900441513602454%2F&tabs=timeline&width=300&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1330903350339636" width="300" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>  
            </div>
        </div>
    </div>
    <!--Bottom footer start-->
    <div class="cy_btm_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <P>Copyright &copy;2018  <b style="color:#ea64b2;">AhmednagarCyclothon</b>. All Rights Reserved. Developed By<a style="color:#e6c33e;" href="www.onevoicetransmedia.com" target="_blank"> OneVoice Transmedia Pvt. Ltd.</a></P>
                </div>
            </div>
        </div>
    </div>
		<!--Go to top start-->
	<div class="cy_go_to">
		<div class="cy_go_top">
			<img src="images/go_to_top.png" alt="Back to top">
		</div>	
	</div>
    <!--scripts start-->
    <script src="js/jquery.js"></script>
    <script src="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/appear.js"></script>
    <script src="js/plugin/owl/owl.carousel.min.js"></script>
    <script src="js/plugin/magnific/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/custom.js"></script>
</body>


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:13:47 GMT -->
</html>