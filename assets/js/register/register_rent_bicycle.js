
  $( function() {
    $( "#birthdate_rent_bicycle" ).datepicker({
          readOnly: true,
          changeMonth: 1,
          changeYear: true,
          numberOfMonths: 1,
           maxDate:0,
          minDate:null,
          yearRange: '1940:2005',
          defaultDate: new Date(2005,01,00)
        }).on( "change", function() {
          var birthdate_rent_bicycle=$('#birthdate_rent_bicycle').val();
           if(birthdate_rent_bicycle == ""){
          $('#age').val('');
         }
         else{
         //start to calculate age
         $('#age').val(getAge(birthdate_rent_bicycle));
         var rent_age=$('#age').val();
         if(rent_age < 14){
          $( "#birthdate_rent_bicycle").val('');
           $( "#age").val('');
         }
         //end to calculate age

          
         }
          var edit_birth_date=$("#birthdate_rent_bicycle").val();
          var fromsplit=edit_birth_date.split("/");
          if(fromsplit[0] != ''){
            $("#birthdate_rent_bicycle").val(fromsplit[0]+'/'+fromsplit[1]+'/'+fromsplit[2]);
          }
         
          
            
        });
        

  } );
  $("#birthdate_rent_bicycle").on("keyup", function (e) {
    $('#birthdate_rent_bicycle').val('');
  });