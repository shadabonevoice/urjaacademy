<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Urja extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('upload');
		$this->load->library('email');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('session');  
		$this->load->model('common_model');
		
    }

	public function index()
	{		
		$data['page_title']="Urja Academy";
		$this->load->view('urja/index',$data);		
        	
	}
	
	public function terms()
	{	
		$data['page_title']="Terms & Conditions";
		$this->load->view('urja/terms',$data);        	
	}
	
	public function register()
	{	
		// unset($_SESSION);
		$data['page_title']="Registration";
		$this->load->view('urja/registration',$data);        	
	}
	
	public function thankyou()
	{	
		unset($_SESSION);
		$data['page_title']="Thank You";
		$this->load->view('urja/thankyou',$data);        	
	}

	public function check()
	{
		if(!$_POST['fname'] || empty($_POST['mobilenumber'])){
			redirect($this->register());
		}
			//check if mob exist
			$check = $this->input->post('mobilenumber');
			$col = $this->input->post('mobile');
			$mobilenumber = $this->input->post('mobilenumber');			
				$chk_array=array('mobilenumber'=>$check);			
				$this->db->where($chk_array);
				$result = $this->db->get('tbl_register');
			if($result->num_rows()>0){  
				$this->session->set_flashdata('error', 'Mobile number already registered..!');
				$this->session->mark_as_temp('error', 5);
				redirect(site_url('index.php/Urja/register'));
			}
		
		// unset($this->session->userdata());
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required'); 
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required'); 
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required'); 
		$this->form_validation->set_rules('fullname', 'Full Name in Devnagari', 'trim|required'); 
		$this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required'); 
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required'); 
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); 
		$this->form_validation->set_rules('address', 'Postal Address', 'trim|required'); 
		$this->form_validation->set_rules('mobilenumber', 'Mobile Number', 'trim|required'); 
		$this->form_validation->set_rules('pincode', 'Pincode', 'trim|required'); 
		// $this->form_validation->set_rules('fathername', 'Father', 'trim|required'); 
		if(@$_POST['telephone']){
			$this->form_validation->set_rules('telephone', 'Telephone', 'trim'); 			
		}
		if(@$_POST['fathername']){
			$this->form_validation->set_rules('fathername', 'Father', 'trim'); 			
		}
		if(@$_POST['fathermobilenumber']){
			$this->form_validation->set_rules('fathermobilenumber', 'Father Mobile Number', 'trim'); 			
		}
		if(@$_POST['fathertelephone']){
			$this->form_validation->set_rules('fathertelephone', 'Father Telephone', 'trim'); 			
		}
		if(@$_POST['mothername']){
			$this->form_validation->set_rules('mothername', 'Mother Name', 'trim'); 			
		}
		if(@$_POST['mothermobilenumber']){
			$this->form_validation->set_rules('mothermobilenumber', 'Mother Mobile Number', 'trim'); 			
		}
		if(@$_POST['mothertelephone']){
			$this->form_validation->set_rules('mothertelephone', 'Mother Telephone', 'trim'); 			
		}
		if(@$_POST['guardianname']){
			$this->form_validation->set_rules('guardianname', 'Guardian Name', 'trim'); 			
		}
		if(@$_POST['guardianmobilenumber']){
			$this->form_validation->set_rules('guardianmobilenumber', 'Guardian Mobile Name', 'trim'); 			
		}
		if(@$_POST['guardiantelephone']){
			$this->form_validation->set_rules('guardiantelephone', 'Guardian Telephone', 'trim'); 			
		}
		if(@$_POST['medicalcondition']){
			$this->form_validation->set_rules('medicalcondition', 'Medical Condition', 'trim'); 			
		}
		if(@$_POST['personalstatement']){
			$this->form_validation->set_rules('personalstatement', 'Personal statement', 'trim'); 			
		}
		if(@$_POST['academiceducation']){
			$this->form_validation->set_rules('academiceducation', 'Academic Condition', 'trim'); 			
		}
		if(@$_POST['drama']){
			$this->form_validation->set_rules('drama', 'Drama', 'trim'); 			
		}
		if(@$_POST['dance']){
			$this->form_validation->set_rules('dance', 'Dance', 'trim'); 			
		}
		if(@$_POST['music']){
			$this->form_validation->set_rules('music', 'Music', 'trim'); 			
		}
		if(@$_POST['presentschool']){
			$this->form_validation->set_rules('presentschool', 'Present School', 'trim'); 			
		}
		if(@$_POST['work']){
			$this->form_validation->set_rules('work', 'Work & Performance', 'trim'); 			
		}
		if(@$_POST['skill']){
			$this->form_validation->set_rules('skill', 'Skills', 'trim'); 			
		}
		
		if ($this->form_validation->run() == FALSE) { 
			$this->load->view('urja/registration');
		}
		$data = array( 
			'fname'	=>  $_POST['fname'] , 
			'mname'	=>  $_POST['mname'] , 
			'lname'	=>  $_POST['lname'] , 
			'fullname'	=>  $_POST['fullname'] , 
			'dob'	=>  date('Y-m-d', strtotime($_POST['dob'])) , 
			'gender'	=>  $_POST['gender'] , 
			'email'	=>  $_POST['email'] , 
			'address'	=>  $_POST['address'] , 
			'mobilenumber'	=>  $_POST['mobilenumber'] , 
			'pincode'	=>  $_POST['pincode'] , 
			'telephone'	=>  ($_POST['telephone']) ? $_POST['telephone'] : '' , 
			'fathername'	=>  ($_POST['fathername']) ? $_POST['fathername'] : '' , 
			'fathermobilenumber'	=>  ($_POST['fathermobilenumber']) ? $_POST['fathermobilenumber'] : '' ,  
			'fathertelephone'	=>  ($_POST['fathertelephone']) ? $_POST['fathertelephone'] : '' , 
			'mothername'	=>  ($_POST['mothername']) ? $_POST['mothername'] : '' , 
			'mothermobilenumber'	=>  ($_POST['mothermobilenumber']) ? $_POST['mothermobilenumber'] : '' , 
			'mothertelephone'	=>  ($_POST['mothertelephone']) ? $_POST['mothertelephone'] : '' , 
			'guardianname'	=>  ($_POST['guardianname']) ? $_POST['guardianname'] : '' , 
			'guardianmobilenumber'	=>  ($_POST['guardianmobilenumber']) ? $_POST['guardianmobilenumber'] : '' , 
			'guardiantelephone'	=>  ($_POST['guardiantelephone']) ? $_POST['guardiantelephone'] : '' , 
			'medicalcondition'	=>  ($_POST['medicalcondition']) ? $_POST['medicalcondition'] : '' ,  
			'personalstatement'	=>  ($_POST['personalstatement']) ? $_POST['personalstatement'] : '' , 
			'academiceducation'	=>  ($_POST['academiceducation']) ? $_POST['academiceducation'] : '' , 
			'drama'	=>  ($_POST['drama']) ? $_POST['drama'] : '' , 
			'dance'	=>  ($_POST['dance']) ? $_POST['dance'] : '' , 
			'music'	=>  ($_POST['music']) ? $_POST['music'] : '' , 
			'presentschool'	=>  ($_POST['presentschool']) ? $_POST['presentschool'] : '' , 
			'work'	=>  ($_POST['work']) ? $_POST['work'] : '' , 
			'skill' =>  ($_POST['skill']) ? $_POST['skill'] : ''  
		);
		$insertData = $this->db->insert('tbl_register', $data);
		if(!$insertData ){
		   echo $errNo   = $this->db->_error_number();
		   echo $errMess = $this->db->_error_message();
		   $this->session->set_userdata('error', 'Database error please try again..!');
		   // Do something with the error message or just show_404();
		}else{
	        $success_msg= "Dear ".$_POST['fname'].' '.$_POST['lname'].", You have successfully registered for Urja Academy.";
			$this->common_model->sendSMS($_POST['mobilenumber'], $success_msg);  

				// $from_email = "sayyedmustaquim2@gmail.com"; 
				 // $to_email = $this->input->post('email'); 
		   
				 //Load email library 
				 // $this->load->library('email'); 
		   
				 // $this->email->from($from_email, 'Urja Academy'); 
				 // $this->email->to($to_email);
				 // $this->email->subject('Urja Academy'); 
				 // $this->email->message('Dear '.$_POST['fname'].' '.$_POST['lname'].' you have successfully Register. Thank you'); 		   
				 //Send mail 
				 // $this->email->send();	

		//email
		// $this->load->config('email');
        // $this->load->library('email');
        
        // $from = $this->config->item('smtp_user');
        // $to = $this->input->post('email');
        // $subject = 'Urja Academy Register';
        // $message = $success_msg;

        // $this->email->set_newline("\r\n");
        // $this->email->from($from);
        // $this->email->to($to);
        // $this->email->subject($subject);
        // $this->email->message($message);

        // if ($this->email->send()) {
            // echo 'Your Email has successfully been sent.';
        // } else {
            // show_error($this->email->print_debugger());
        // }
		//

				 
				// $this->session->set_userdata('success', 'Register Successfully!');	
				redirect(site_url('index.php/urja/thankyou'));
		}
		$data['page_title']="Registration";
		$this->load->view('urja/registration',$data);	  
	}
	
	public function check_duplicate(){
		$check = $this->input->post('check');
		$col = $this->input->post('col');
		
		$data['status'] = 'fail';
		$data['message'] = '';
		if($check != ''){
			if($col == 'mobile'){
				$chk_array=array('mobilenumber'=>$check);
			}else{
				$chk_array=array('email'=>$check);
			}
			
			$this->db->where($chk_array);
			$result = $this->db->get('tbl_register');
			if($result->num_rows()>0){  
				$data['status'] = 'fail';
				$data['message'] = $col.' already exist..!';
			}else{
				$data['status'] = 'success';
				$data['message'] = '';
			}
		}
		echo json_encode($data);
	}
	
}
