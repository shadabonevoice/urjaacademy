<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_club extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('upload');
		$this->load->library('email');
		$this->load->helper('form');
		$this->load->library('session');  
		$this->load->model('common_model');
		
    }

	public function index()
	{
		
		
		$this->load->view('registration_club');	
		$this->load->view('footer');	
		
        	
	}

	public function check()
	{
		
		$amount =  $this->input->post('payble_amount');
	    $product_info = $this->input->post('productinfo');
	    $customer_name = $this->input->post('customer_name');
	    $customer_emial = $this->input->post('customer_email');
	    $customer_mobile = $this->input->post('mobile_number');
	    $customer_address = $this->input->post('customer_address');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$postcode = $this->input->post('postcode');
		$country = $this->input->post('country');
		$gender = $this->input->post('gender');
		$birthdate = $this->input->post('birthdate');
		$age = $this->input->post('age');
		$bloodgrp = $this->input->post('bloodgrp');
		$tshirt = $this->input->post('tshirt');
		$uremrname = $this->input->post('uremrname');
		$uremrnumber = $this->input->post('uremrnumber');
		
	    
	    	//payumoney details
	    
	    
	        $MERCHANT_KEY = "rjQUPktU"; //change  merchant with yours
	        $SALT = "e5iIg1jwi8";  //change salt with yours 

	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        
	         $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
	         $hash = strtolower(hash('sha512', $hashstring));
	         
	       $success = base_url() .'index.php/Status_club';  
	        $fail = base_url() .'index.php/Status_club';
	        $cancel = base_url() .'index.php/Status_club';
	        
	       // print_r($_POST);exit;
	       $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'productinfo' => $product_info,
	            'mailid' => $customer_emial,
	            'phoneno' => $customer_mobile,
	            'address' => $customer_address,
				'state' => $state,
				'city' => $city,
				'postcode' => $postcode,
				'country' => $country,
				'gender' => $gender,
				'birthdate' => $birthdate,
				'age' => $age,
				'bloodgrp' => $bloodgrp,
				'tshirt' => $tshirt,
				'uremrname' => $uremrname,
				'uremrnumber' => $uremrnumber,
	            'action' => "	https://test.payu.in/", //for live change action  https://secure.payu.in
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel            
	        );
			
			$this->session->set_userdata($data);
			//print_r($data); exit;
	        $this->load->view('confirmation_club',$data);   
     
	}

	public function help()
	{
		
		$this->load->view('help');
		$this->load->view('footer');
	}
}
