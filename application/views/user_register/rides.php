<?php
$this->load->view('common/header.php');
?>
    <!--Events Sevtion Start-->
    <div class="cy_event_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Upcoming Event</h1>
                </div>
            </div>
           <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/20km.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                         <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 6:00 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">17 Feb</span>
                                    <span class="ev_yr">2019</span>

                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2  style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 20KM Cyclothon</h2>
							   <center>
							 <a href="<?php echo base_url(); ?>" class="cy_button">Registration Fee: </b>&#8360; 500/-</a>
                            </center>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/50km.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 5:45 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                   <span class="ev_date">17 Feb</span>
                                    <span class="ev_yr">2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2  style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 50KM Cyclothon</h2>
							  <center>
							 <a href="<?php echo base_url(); ?>" class="cy_button">Registration Fee: </b>&#8360; 600/-</a>
                            </center>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/100km.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 5:30 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">17 Feb</span>
                                    <span class="ev_yr">2019</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 100km Cyclothon</h2>
							 <!--<h2 style="text-align:center;"><a href="<?php echo base_url(); ?>index.php/welcome"><b style="color:#ff0000"> Registration Fee: </b>&#8360; 600/-</a></h2>-->
							 <center>
							 <a href="<?php echo base_url(); ?>" class="cy_button">Registration Fee: </b>&#8360; 700/-</a>
                            </center>
							<!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.</p>-->
                           
                        </div>
                    </div>
                </div>
            </div> 
			</div>
    </div>
    <!--Footer section start-->
	<?php
$this->load->view('common/footer');
        ?>