<?php
$this->load->view('common/header.php');
?>
    <!-- checkout section start -->
    <div class="cy_checkout_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 offset-lg-2">
                    <div class="cy_checkout_box">
                        <ul id="progressbar">
                            <li class="active">Transaction Success</li>
                         <!--   <li>Payment</li>
                            <li>Receipt</li> -->   
                        </ul>

                     <div class="col-lg-8 col-md-12 offset-lg-2">
                            <div class="row">
                            
                            <div class="cy_about_data" style="padding-top: 10px">
                                <p style="text-align:center;">
                                
                                    <?php 
                                        echo "<p style='text-align:center;'>Thank You! Your transaction is successful.</br>";
                                        echo "Your Transaction ID for this transaction is <b>".$this->session->userdata('usertxnid')."</b></br>";
                                        echo "We have received a payment of <b> Rs. " . $amount . ".</b></p>";
                                    ?>
                                </p>
                            </div>
                                
                                    
                                    
                                 
                            </div>
                        </div>  

                         </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->
  <?php
$this->load->view('common/footer');
        ?>


