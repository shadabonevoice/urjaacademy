<?php $this->load->view('common/header.php'); ?>   
    <!--About section start-->
	<div class="cy_price_wrapper">
        <div class="container">          
            <div class="row">
				<div class="cy_about_data">
                    <img src="<?php echo base_url(); ?>assets/images/logo_natak.png" width="200px" alt="about" />                   
                </div>
				<div class="cy_about_data">
					<!--<h3><b>ऊर्जा अकॅडमी</b></h3><br>-->
					<h4><b>अखेरचे काही प्रवेश शिल्लक</b></h4><br>
					<p>
						'नाटकशाळा' - अभिनय, लेखन, दिग्दर्शन, संगीत, प्रकाशयोजना, नेपथ्य व निर्मिती यांसारख्या नाट्यकलेशी
						निगडित सर्व अंगांची ओळख व त्याविषयीचे परिपूर्ण मार्गदर्शन करून देणारी एक प्रशिक्षण संस्था.<br>
						नाट्यक्षेत्रातील अनेक दिग्गज कलाकार आणि तंत्रज्ञ यांच्या सहकार्याने इथे एक वर्षाचा सर्वोत्तम
						अभ्यासक्रम शिकवला जाणार आहे. तसेच वर्षभरात काही नाट्यनिर्मिती सुद्धा केल्या जाणार आहेत.
						वयवर्ष सोळा पूर्ण केलेल्या कोणत्याही व्यक्तीला यात प्रवेश घेता येईल.
					</p><br>
					<h4><b>नाटक शिकायचंय !</b></h4><br>
					<h4><b>मग चला नाटकशाळेत !!</b></h4>
				</div>	
				<div class="cy_about_data">
					<button type="button" class="btn btn-lg btn-lg btn-warning"><a style="color:white;" href="<?php echo site_url('index.php/urja/register');?>">नोंदणी करा</a></button>
				</div>
				<div class="cy_about_data">
					<p style="font-size: 18px; float: right;">Initiative of Urja Academy</p>				
				</div>
			</div>
        </div>
    </div>
		
    <!--Footer section start-->
<?php $this->load->view('common/footer'); ?>
	