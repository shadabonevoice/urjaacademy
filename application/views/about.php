<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
	
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url(); ?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
       <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
								<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>PHONE: 8308054000</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>about us</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url(); ?>index.php/index"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/about" class="active">about</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/join">join</a></li>
								<li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
                                    </ul>
                                </li>
								<li><a href="<?php echo base_url(); ?>index.php/routes">route</a></li>
								<li><a href="<?php echo base_url(); ?>index.php/events">events</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
   
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">who we are</h1>
                        <p>
						Ahmednagar Cyclothon is officially affiliated with <b>Cycling Federation of India(CFI)</b>,  <b>Kalpattaru Enterprises India Limited</b> and <b>Cycling Association of Maharashtra(CAOM)</b>.
						Ahmednagar Cycling Club is a friendly cycling club based in Ahmednagar, Maharashtra. It is dedicated to encouraging
						enjoyment of all types of cycling within the Ahmednagar Community. The Club was framed to promote the reason for 
						keeping our city green, while keeping our bodies healthy and clean. We invite individuals
						from the all the communities to our club occasions, for example, 
						gather rides, Cyclothons, etc. and group benefit exercises.
					
						</p>
                      
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_about_img">
                        <img src="<?php echo base_url(); ?>assets/images/about/about1.jpg" alt="about" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="cy_sponsors_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="cy_ride_text">
                        <h3>Next Ride Starts On</h3>
                        <h2>February 17, 2019</h2>
						<center>
						<table style="color:#fff;float: left;">
						<tr>
							<td>
								<h6 style="color:#fff;">Category:</h6>
							</td>
							<td></td>
							<td></td>
							<td>
								100KM
							</td>
							<td></td>
							<td></td>
							<td>
								50KM
							</td>
							<td></td>
							<td></td>
							<td>
								20KM
							</td>
						</tr>
						
						<tr>
						<td>
							<h6 style="color:#fff;">Reporting Time:</h6>
							</td>
							<td></td>
							<td></td>
							<td>
								5:30AM
							</td>
							<td></td>
							<td></td>
							<td>
								5:45AM
							</td>
							<td></td>
							<td></td>
							<td>
								6:00AM
							</td>
						
						</tr>
						
						<tr>
						
						<td>
							<h6 style="color:#fff;">Flag-Off Time:</h6>
							</td>
							<td></td>
							<td></td>
							<td>
								6:00AM
							</td>
							<td></td>
							<td></td>
							<td>
								6:15AM
							</td>
							<td></td>
							<td></td>
							<td>
								6:30AM
							</td>
						</tr>
						
						</table>
                        </center>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_sponsor_slider cy_section_padding">
                        <h1 class="cy_heading">our sponsors</h1>
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/patanjali02.png" alt="sponsor" />
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/lakme_salon02.png" alt="sponsor" />
                            </div>
							<div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/golds_gym03.png" alt="sponsor" />
                            </div>
                           <!-- <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/radi_partner04.png" alt="sponsor" />
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/energy_drink05.png" alt="sponsor" />
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/oxycool06.png" alt="sponsor" />
                            </div>-->
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/ilovengr08.png" alt="sponsor" />
                            </div>
                         <!--   <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/IMA07.png" alt="sponsor" />
                            </div>-->
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/nagar_rising08.png" alt="sponsor" />
                            </div>
							
							 <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/sole_sisters08.png" alt="sponsor" />
                            </div>
							<!--<div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/bhagirathi_09.png" alt="sponsor" />
                            </div>-->
							<div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/giant_starkenn01.png" alt="sponsor" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
   <!--Team section start-->
    <div class="cy_team_wrapper padder_top100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">meet our team</h1>
                </div>
            </div>
            <div class="row">
				<div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/sunil_menon.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Sunil Menon</a></h2>
								<h4 style="color:#fff;">Race Director</h4>
                               
                            </div>
                        </div>
                    </div>
                </div>
			
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/gaurav_firodia.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Gaurav Firodia</a></h2>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/kalyani_firodia.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Kalyani Firodia</a></h2>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/mahesh_mulay.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Mahesh Mulay</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
			
			<!-- 2nd row-->
			 <div class="row">
			 
			   <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/ravi_patre.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Ravi Patre</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/abhijeet_pathak.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Abhijeet Pathak</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/dinesh_sanklecha.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Dinesh Sanklecha</a></h2>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/jyoti_pisore.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Jyoti Pisore</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
			
		<div class="row">
		
		  <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/ravi_pisore.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Ravi Pisore</a></h2>
                             
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/renuka_pathak.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Renuka 	</a></h2>
                             
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/tushar_patwa.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Tushar Patwa</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        
			
        </div>
    </div>
    <!--Club History section css start-->
    <!--<div class="cy_club_wrapper cy_section_padding padder_bottom124">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">our club history</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="cy_club_history">
                        <ul class="cy_timeline">
                            <li>
                                <div class="cy_tl-item dir-r">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content">
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img.jpg" alt="history" class="img-fluid" />
                                        </div>
                                        <div class="cy_club_data">
                                            <span> 4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="cy_tl-item">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content text-right">
                                        <div class="cy_club_data">
                                            <span>4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img1.jpg" alt="history" class="img-fluid" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="cy_tl-item dir-r">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content">
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img2.jpg" alt="history" class="img-fluid" />
                                        </div>
                                        <div class="cy_club_data">
                                            <span>4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="cy_tl-item">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content text-right">
                                        <div class="cy_club_data">
                                            <span>4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img3.jpg" alt="history" class="img-fluid" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
 	<!--Footer section start-->
   