<?php
$this->load->view('common/header.php');
?>
    <!--Menus Start-->

   
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!-- checkout section start -->
    <div class="cy_checkout_wrapper">
        <div class="container">
            <div class="row">
			
			
             <div class="col-lg-8 col-md-12 offset-lg-2" >
                    <div class="cy_checkout_box">
					<?php
    	$countRentBicycle=getTotalRentBicycle();
    	$total_rent_bicycle_left=totalBicycleRent - count($countRentBicycle);
    	?>
						<?php 
							// $maxidd	= $this->db->query('SELECT MAX(rent_id) AS `maxidd` FROM `tbl_rentbicycle`')->row()->maxidd;
							// if(empty($maxidd)){
							// 	$bid11 = 125;
							// }else{
							// 	$bid = $this->db->select('bicycle_count as bid')->from('tbl_rentbicycle')->where('rent_id',$maxidd)->get(); 
							// 	$bid1 = $bid->row()->bid;
							// 	$bid11 = $bid1 - 1;
							// }
							
							if($total_rent_bicycle_left < 1){
						?>
								<img src="assets/images/thanku.png" alt="Thank U" style="width:100%; height:auto;">
								
						<?php }else{?>	
						
						<ul id="progressbar">
							<li class="active">Register to Rent a Bicycle</li>
					   
						</ul>
						
						<h4 class="cy_heading" style="color:#2f3942;text-align:center;">Only <?php echo $total_rent_bicycle_left; ?> bicycles left for Rent</h4>	
							
						<br><br>

				
						<form method="post" id="product_info" name="product_info" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/Welcomerentbicycle/check">  
						 <div class="woocommerce-billing-fields step">	
						   
						   <div class="form-group">  
								Full Name (Only alphabets)			   
							  <input type="text"  name="customer_name" id="customer_name" class="form-control" placeholder="Full Name (Only alphabets)" required />
							</div>
							<span id="errorurname"></span>
							
							<div class="form-group"> 
								Mobile Number				
							  <input type="number"  name="mobile_number" id="mobile_number" maxlength="10" class="form-control" placeholder="Mobile Number(10 digits)" required />
							</div>
							
							<div class="form-group"> 
							Email
							  <input type="email"  name="customer_email" id="customer_email" class="form-control" placeholder="Email" required/>
							</div>
							
							<div class="form-group">
							Address
							  <textarea class="form-control" name="customer_address" id="customer_address" placeholder="Address" required ></textarea>
							</div>
							
						
						
							<div class="form-group checkbox" style="bottom: 20px;">
							   <label style="bottom: 20px;">Gender</label><br>
									 <input type="radio" name="product_info" value="male" checked style="display:inline-block;"> Male
									  <input type="radio" name="product_info" value="female" style="display:inline-block;"> Female
							</div>
							
							</br>

								<div class="form-group"> 
									
								</div>
						<div class="form-group">
									Date of Birth
										<input type="text" class="form-control" id="birthdate_rent_bicycle" name="birthdate_rent_bicycle" placeholder="Date of Birth(MM/DD/YYY)" required style="width: 35%;" />
										<p style="padding-top: 7px;"><b>Note : </b> Age should be greater than 14, on or before 16th Feb 2019</p>
									</div>
									<div class="form-group">
									Your Age
										<input type="text" placeholder="Your Age" maxlength="2" name="age" id="age" class="form-control" readonly="">
									</div>
							
							
									<div class="form-group">
									Total Amount(INR)
										<input type="text" value="350" readonly name="payble_amount" id="payble_amount"  class="form-control">
									</div>
								   
								   <p>
								  <b style="color:red;"> Note* </b> - * You will have to pay INR. 1,000/- as Security Deposit for the bicycle at the EXPO. Which will be refunded on handing over the bicycle after the race.
								   </p>
									<p>
									* Please carry valid Government issued ID proofs and Address proofs.
									</p>
								</br>
							
								<center>
									<div class="form-group">
									  <button type="submit" class="btn btn-success">Submit</button>
									  <button class="btn btn-secondary" type="reset">Reset</button>
									</div>
								</center>
							</div>
						</form> 
			<?php }?>
			            
         </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->
<?php
$this->load->view('common/footer');
?>
<link rel="stylesheet" href="assets/calender/jquery-ui.css">


  <script src="assets/calender/jquery-ui.js"></script>
  <script src="assets/js/register/register_rent_bicycle.js"></script>
		
		
		
