<?php
$this->load->view('common/header.php');
?>

   
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!-- checkout section start -->

    
    	<?php
    	
    	//SELECT * FROM `tbl_register` WHERE DATE_FORMAT(`create_at`, '%Y')='2018'
    	$getTotalRegisteredData=getTotalRegistered();
    	$total_registerations_left=totalRegisterations - count($getTotalRegisteredData);
    	?>
		
		
			<div class="container" style="<?php if($total_registerations_left > 0){?>display:none;<?php } ?>">

				<div class="row">

				<!--style="background-color: #93b3be;"-->
					<div class="col-lg-8 col-md-12 offset-lg-2">
						<div class="cy_checkout_box">
						<img src="assets/images/thanku.png" alt="Thank U" style="width:100%; height:auto;">
						</div>
					</div>
				</div>
			</div>	
		<div class="cy_checkout_wrapper" style="<?php if($total_registerations_left < 1){?>display:none;<?php } ?>">

<h4 class="cy_heading" style="color:#2f3942;text-align:center;">Only <?php echo $total_registerations_left;?> registrations left</h4>
        <div class="container">

            <div class="row">

			<!--style="background-color: #93b3be;"-->
                <div class="col-lg-8 col-md-12 offset-lg-2" style="background-color: #93b3be;">
                    <div class="cy_checkout_box">
					<!--<img src="<?php// echo base_url();?>assets/images/thanku.png" alt="Thank U" style="width:100%; height:auto;">-->
                       <ul id="progressbar">
                            <li class="active">Register for Cyclothon</li>
                        </ul>
						

        	
           <form method="post" id="product_info" name="product_info" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/Welcome/check">  
			 <div class="woocommerce-billing-fields step">	
               
               <div class="form-group">  
					Full Name (Only alphabets)			   
                  <input type="text"  name="customer_name" id="customer_name" class="form-control" placeholder="Full Name (Only alphabets)" required />
                </div>
			<span id="errorurname"></span>
				
                <div class="form-group"> 
					Mobile Number				
                  <input type="number"  name="mobile_number" id="mobile_number" class="form-control" placeholder="Mobile Number(10 digits)" required />
                </div>
				
                <div class="form-group"> 
				Email
                  <input type="email"  name="customer_email" id="customer_email" class="form-control" placeholder="Email" required/>
                </div>
				
                <div class="form-group">
				Address
                  <textarea class="form-control" name="customer_address" id="customer_address" placeholder="Address" required ></textarea>
                </div>
				
				<div class="form-group">
				Enter Your State
                  <input type="text" class="form-control" name="state" id="state" placeholder="Enter Your State" required></textarea>
                </div>
				
				<div class="form-group">
				Enter Your City
                  <input type="text" class="form-control" name="city" id="city" placeholder="Enter Your City" required></textarea>
                </div>
				
				<div class="form-group">
				Enter Your Postcode
                  <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Enter Your Postcode" required></textarea>
                </div>
				
				<div class="form-group">
				Enter Your Country
                  <input type="text" class="form-control" name="country" id="country" placeholder="Enter Your Country" required></textarea>
                </div>
			
				 <div class="form-group checkbox" style="bottom: 20px;">
				   <label style="bottom: 20px;">Gender</label><br>
						 <input type="radio" name="gender" value="male" checked style="display:inline-block;"> Male
						  <input type="radio" name="gender" value="female" style="display:inline-block;"> Female
				</div>
				
				</br>
						<div class="form-group"> 
								
						</div>
						<div class="form-group date"> 
							Date of Birth
							<input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="Date of Birth(MM/DD/YYY)" required />
						  </div>
						
					
					
						<div class="form-group">
						Enter Your Age
							<input type="text" placeholder="Enter Your Age" name="age" id="age" class="form-control" required>
						</div>
					
					
					
						<div class="form-group">
						Enter Your Blood Group.
							<input type="text" placeholder="Enter Your Blood Group." name="bloodgrp" id="bloodgrp"  class="form-control">
						</div>
				
				
						
					
						<div class="form-group">
						Select Race Category
							<select required class="form-control"  placeholder="Select Race Category" name="product_info" id="product_info"   onchange="myFunction(event)">
								<option value="">Select Race Category</option>
								<option  value="20KM" >20KM</option>
								<option  value="50KM" >50KM</option>
								<option  value="100KM" >100KM</option>
							</select>
						</div>
				
						<div class="form-group">
						Total Amount
							<input type="text" placeholder="Total Amount." readonly name="payble_amount" id="payble_amount"  class="form-control">
						</div>
					   
					
					
						<div class="form-group">
						Select T-Shirt Size
							<select required class="form-control" name="tshirt" onchange="myFunction(event)">
								<option value="">Select T-Shirt Size</option>
								<option value="S" >S</option>
								<option value="M" >M</option>
								<option value="L" >L</option>
								<option value="XL" >XL</option>
								<option value="XXL" >XXL</option>
								<option value="XXXL" >XXXL</option>
							</select>
						</div>
					
						<div class="form-group">
						Emergency Contact Name
							<input type="text" placeholder="Emergency Contact Name" name="uremrname" id="uremrname" class="form-control" required>
						</div>
					
						<div class="form-group">
						Emergency Contact Number
							<input type="text" placeholder="Emergency Contact Number" name="uremrnumber" id="uremrnumber"  class="form-control" required>
						</div>
				
				
				
					<center>
						<div class="form-group">
						  <button type="submit" class="btn btn-success">Submit</button>
						  <button class="btn btn-secondary" type="reset">Reset</button>
						</div>
					</center>
				</div>
            </form> 
			
			
			
			            
         </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->
	





	<script>
		function myFunction(e) {
		
		
		var ab=e.target.value;
		
		if(ab == "20KM")
		{
		var amt = 400;
		document.getElementById("payble_amount").value = amt;
		}
		else if(ab == "50KM")
		{
		var amt1 = 500;
		document.getElementById("payble_amount").value = amt1;
		}
		else if(ab == "100KM")
		{
		var amt2 = 600;
		document.getElementById("payble_amount").value = amt2;
		}
		else{
		//document.getElementById("amount").value = "Total Amount";
		}

		  
		}
		</script>
		<?php
$this->load->view('common/footer');
		?>
	
		
		
