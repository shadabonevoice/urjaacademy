<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	
   <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
	
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url(); ?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
     <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
							<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>PHONE: 8308054000</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>contact</h1>
                </div>
            </div>
        </div>
    </div>
    <!--Menus Start-->
 <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url(); ?>index.php/index"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url(); ?>index.php/join">join</a></li>
								<li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url(); ?>index.php/routes">route</a></li>
								  <li><a href="<?php echo base_url(); ?>index.php/events">events</a></li>
								 
								
                                <li><a href="<?php echo base_url(); ?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/contact" class="active">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!--Contact section start-->
    <div class="cy_contact_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Get In touch</h1>
                </div>
            </div>
            <div class="row padder_top50">
                <div class="col-lg-4 col-md-12">
                    <div class="cy_con_box">
                        <img src="<?php echo base_url(); ?>assets/images/contact/con_img1.jpg" alt="phone" class="img-fluid" />
                        <div class="cy_con_overlay">
                            <div class="cy_con_data">
                                <h3>phone</h3>
                               <p>Contact :- 8308054000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_con_box">
                        <img src="<?php echo base_url(); ?>assets/images/contact/con_img2.jpg" alt="email" class="img-fluid" />
                        <div class="cy_con_overlay">
                            <div class="cy_con_data">
                                <h3>Email</h3>
                                <p>support@nagarcycling.com</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_con_box">
                        <img src="<?php echo base_url(); ?>assets/images/contact/con_img3.jpg" alt="address" class="img-fluid" />
                        <div class="cy_con_overlay">
                            <div class="cy_con_data">
                                <h3>Venue</h3>
								
                                <p>IMA Bhavan- Near Railway over </p>
								<p>bridge, Kalyan Road - Ahmednagar</p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Map section start-->
    <div class="cy_form_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h1 class="cy_heading">How can we help you ?</h1>
                    <div class="cy_form padder_top50">
					
					<center>
						<?php if($this->session->flashdata('success'))
							{?>
						<h4 style="color:green;" id="successbuy5"><?php echo $this->session->flashdata('success');  ?></h4>
						<?php }
							else
							{ 
							echo $this->session->flashdata('error');   
							}?> 
					</center>
                        <form enctype="multipart/form-data" name="contact_submit" id="contact_submit" method="post" action="<?php echo base_url(); ?>index.php/contact">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 padder_left">
                                    <div class="form-group">
                                        <input type="text" name="first_name" maxlength="40" class="form-control require" placeholder="Name" required>
                                    </div>
                                </div>
                               <!-- <div class="col-lg-6 col-md-6 padder_right">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control require" placeholder="Email" data-valid="email" data-error="Email should be valid." required>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 padder_left padder_right">
                                    <div class="form-group">
                                        <input type="text" name="subject" class="form-control" placeholder="Subject" required>
                                    </div>
                                </div>-->
                                <div class="col-lg-12 col-md-12 padder_left padder_right">
                                    <div class="form-group">
                                        <textarea name="message" maxlength="300" class="form-control" placeholder="Message" required></textarea>
                                    </div>
                                </div>
                                <div class="response"></div>
                                <div class="col-lg-12 col-md-12 padder_left padder_right">
                                    <input type="submit" name="contact_submit" id="contact_submit" class="cy_button submitForm">
                                   <!-- <button type="button" name="contact_submit" id="contact_submit" class="cy_button submitForm">Submit</button>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.3229330922723!2d74.75725211446817!3d19.09348388707785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bdcb01572612b15%3A0x6471b31788420748!2sAhmadnagar+Club+Ltd.!5e0!3m2!1sen!2sin!4v1518087860708" width="auto" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->


<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script>
			$(document).ready(function(){
			      setTimeout(function() {
			        $('#successbuy5').fadeOut('fast');
			      }, 8000); // <-- time in milliseconds
			  });
		</script>
    <script>

        $(document).ready(function() {
            var myCenter = new google.maps.LatLng(28.419536, -81.462931);

            function initialize() {
                var mapProp = {
                    center: myCenter,
                    zoom: 17,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("contact_map"), mapProp);
                var icon = {
                    url: 'images/map_pin.png'
                };
                var marker = new google.maps.Marker({
                    position: myCenter,
                    map: map,
                    title: 'Cycling Template',
                    icon: icon
                });
                marker.setMap(map);
                var infowindow = new google.maps.InfoWindow({
                    content: "<span> Cycling Template </span>"
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        });
    </script>
	
	
