<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->
<!-- 
Template Name: Cycling- Html Template
Version: 1.0.0
Author: Kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:12:38 GMT -->
<head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
	
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url(); ?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<style>
	.cy_about_wrapper.cy_about_page {
    padding-bottom: 0px;
} 

.cy_price_box {
    
    height: auto;
}

.widget.widget_categories ul li {
    width: 100%;
    padding: 8px 20px;
    text-align: center;
}

.cy_achieve_overlay:after {
    position: absolute;
    width: 57.56%;
    right: -4%;
    top: 0;
    height: 100%;
    background-color:transparent;
    content: "";
    transform: skew(-14deg);
    -webkit-transform: skew(-14deg);
    -moz-transform: skew(-14deg);
    -o-transform: skew(-14deg);
    -ms-transform: skew(-14deg);
}

.cy_counter_wrapper {
    float: left;
    padding: 30px 15px;
    position: relative;
    z-index: 1;
}
	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
       <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
							<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>PHONE: 8308054000</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Events</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url(); ?>index.php/index"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url(); ?>index.php/join">join</a></li>
								<li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url(); ?>index.php/routes">route</a></li>
								  <li><a href="<?php echo base_url(); ?>index.php/events" class="active">events</a></li>
								 
								
                                <li><a href="<?php echo base_url(); ?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
   
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="cy_about_data">
                       <h1 class="cy_heading">Ahmednagar  Cyclothon  Event Info</h1>
                        
                    </div>
                </div>
              <!--  <div class="col-lg-6 col-md-12">
                    <div class="cy_about_img">
                        <img src="images/about/about1.jpg" alt="about" class="img-fluid" />
                    </div>
                </div>-->
            </div>
        </div>
    </div>
	
	<div class="cy_achieve_wrapper">
        <div class="cy_achieve_overlay"></div>
        <div class="container">
            <div class="row">
			   <div class="col-lg-6 col-md-12">
                    <div class="cy_counter_wrapper">
                        <div class="row">
                            
							
                            <div class="col-lg-12 col-md-12">
                                <div class="cy_count_box" style="margin-top:0px;">
								 
								<p style="text-transform: none;"> Come out and ride with us to promote awareness of cycling as an alternate mode of
									transportation that will help keep our precious city clean and green and lead to a
									healthier lifestyle.
															</p>
															<br><br>
								<center><p><b> Date: Sunday, February 17, 2019</b></p><br>
								<p><b>Race Categories </b></p><br>
								<p><b>20KM, 50KM &100KM </b></p><br>
								<p style="text-transform: none;">(Routes, reporting times and flag off times to be announced) </p><br>
								
								<p><b>RACE DIRECTOR</b></p>
								<p>Ironman Sunil Menon</p>
								
								
								<br><br>
								</center>
								
								 
								 
                                </div>
                            </div>
							
							
                        </div>
                    </div>
                </div>
           
               <div class="col-lg-6 col-md-12">
			   <br>
			   
			   
			   <span class="col-md-3"></span>
									<div class="col-lg-10 col-md-12">
										<div class="cy_price_box">
											
											<div class="cy_price_body">
											  <div class="cy_price_head">
												<h3>Organised By:</h3>
												
											</div>
											<div class="widget widget_categories">
											   
												<ul>
											<li style="color:#fff;"><b>Gaurav Firodia </b></li>
											<li style="color:#fff;"><b>Kalyani Firodia</b> </li>
											<li style="color:#fff;"><b>Dr. Abhijeet Pathak</b></li>
											<li style="color:#fff;"><b>Dr. Renuka Pathak </b></li>
											<li style="color:#fff;"><b>Dr. Mahesh Mulay </b></li>
											<li style="color:#fff;"><b>Dinesh Sanklecha</b></li>
											<li style="color:#fff;"><b>Tushar Patwa</b></li>
											<li style="color:#fff;"><b>Ravi Patre</b></li>
											<li style="color:#fff;"><b>Ravindra Pisore</b></li>
											<li style="color:#fff;"><b>Jyoti Pisore</b></li>
												</ul>
											</div>
											  
											</div>
										</div>
									</div>	
							<span class="col-md-1"></span>			
										
									</div>
              </div>
			  
			  
			 
        </div>
		
		 
    </div>
	<br><br>
	 <center>
	<h5 style="text-transform: none;"><b>Cyclothon participants will get:</b> T-shirt, Bib, Finisher medal, Participation certificate, Breakfast, and Hydration points.
								</h5>
   </center>
 <br><br>