<?php
$this->load->view('common/header.php');
?>
    <!-- checkout section start -->
    <div class="cy_checkout_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 offset-lg-2">
                    <div class="cy_checkout_box">
                        <ul id="progressbar">
                            <li class="active">Transaction Failed</li>
                         <!--   <li>Payment</li>
                            <li>Receipt</li> -->   
                        </ul>

					 <div class="col-lg-8 col-md-12 offset-lg-2">
							<div class="row">
							
							<div class="cy_about_data" style="padding-top: 10px">
								<p style="text-align:center;">
								
									<?php 
										echo "<p>Your order status is ". $status ."..</br>";
										echo "Your transaction id for this transaction is ".$txnid.". <br>Contact our customer support.</p>";
										echo "<a href='".base_url()."' class='btn btn-sm float-left btn-info'> < - Go Back</a>";
									?>
								</p>
							</div>
							
							
								 
							</div>
						</div>	

						 </div>
                </div>
            </div>
        </div>
    </div>
    <?php
$this->load->view('common/footer');
        ?>