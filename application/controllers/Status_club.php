<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include ("class.phpmailer.php");
include ("class.smtp.php");
class Status_club extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('security');
        $this->load->library('upload');
        $this->load->library('email');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('common_model');
    }
    public function index() {
        $status = $this->input->post('status');
        if (empty($status)) {
            redirect('Welcome_club');
        }
        $firstname = $this->input->post('firstname');
        $amount = $this->input->post('amount');
        $txnid = $this->input->post('txnid');
        $posted_hash = $this->input->post('hash');
        $key = $this->input->post('key');
        $productinfo = $this->input->post('productinfo');
        $email = $this->input->post('email');
        $salt = "e5iIg1jwi8"; //  Your salt
        $add = $this->input->post('additionalCharges');
        $myphone = $this->input->post('phone');
        $myaddr = $this->input->post('address1');
        $mystate = $this->input->post('state');
        $mycity = $this->input->post('city');
        $mycountry = $this->input->post('country');
        $mygender = $this->input->post('gender');
        $mypostcode = $this->input->post('zipcode');
        $mybirthdate = $this->input->post('birthdate');
        $myage = $this->input->post('age');
        $mybloodgrp = $this->input->post('bloodgrp');
        $mytshirt = $this->input->post('tshirt');
        $myemergency_name = $this->input->post('uremrname');
        $myemergency_number = $this->input->post('uremrnumber');
        If (isset($add)) {
            $additionalCharges = $this->input->post('additionalCharges');
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $mystate . '|' . $mycity . '|' . $mycountry . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $mystate . '|' . $mycity . '|' . $mycountry . '|' . $txnid . '|' . $key;
        }
        $data['hash'] = hash("sha512", $retHashSeq);
        $data['amount'] = $amount;
        $data['firstname'] = $firstname;
        $data['txnid'] = $txnid;
        $data['posted_hash'] = $posted_hash;
        $data['status'] = $status;
        $data['mailid'] = $email;
        $data['phoneno'] = $myphone;
        $data['address'] = $myaddr;
        $data['state'] = $mystate;
        $data['city'] = $mycity;
        $data['zipcode'] = $mypostcode;
        $data['country'] = $mycountry;
        $data['gender'] = $mygender;
        $data['birthdate'] = $mybirthdate;
        $data['age'] = $myage;
        $data['bloodgrp'] = $mybloodgrp;
        $data['tshirt'] = $mytshirt;
        $data['uremrname'] = $myemergency_name;
        $data['uremrnumber'] = $myemergency_number;
		//print_r($data);exit();
		

		
        if ($status == 'success') {
			//print_r($data);exit();
			$this->InsertAndEmail();
			
			
            $this->load->view('success_club', $data);
			
			

        } else {
			
            $this->load->view('failure_club', $data);
			$this->load->view('footer');
        }
		
    }
	
	
	
	
			public function InsertAndEmail()
		{
		    $sess_data = $this->session->userdata();
            //$myid= rand(10,500);
            $maxid = $this->db->query('SELECT MAX(registration_no) AS `maxid` FROM `tbl_clubregister`')->row()->maxid;
            $mailname = $sess_data['name'];
            $mailids = $sess_data['mailid'];
			$trans_id = $sess_data['tid'];
			
			
            $mycat_slice = 'ACC';
            $randomno = rand(100, 999);
            $rider_id = $mycat_slice . $randomno;
			
			
			
			$query = $this->db->get_where('tbl_clubregister', array('transaction_id' => $trans_id));
			$foundRows = $query->num_rows();
			if($foundRows >= 1) {
			 redirect(base_url('index.php/index'));
			} else {
			
            $insert_array = array('rider_id' => $rider_id, 'transaction_id' => $sess_data['tid'], 'total_amount' => $sess_data['amount'], 'name' => $sess_data['name'], 'aadhar_no' => $sess_data['productinfo'], 'email_addr' => $sess_data['mailid'], 'mobile_no' => $sess_data['phoneno'], 'full_address' => $sess_data['address'], 'state' => $sess_data['state'], 'city' => $sess_data['city'], 'postcode' => $sess_data['postcode'], 'country' => $sess_data['country'], 'gender' => $sess_data['gender'], 'birthdate' => $sess_data['birthdate'], 'age' => $sess_data['age'], 'bloodgrp' => $sess_data['bloodgrp'],'tshirtsize' => $sess_data['tshirt'], 'create_at' => date('Y-m-d H:i:s'));
            
            if ($this->common_model->add_records('tbl_clubregister', $insert_array)) {
                $this->session->set_flashdata('success_club', 'Your have sucessfully registered for Club Membership');
				
				
                $mail = new PHPMailer();
                $mail->IsSMTP(); // set mailer to use SMTP
                $mail->Host = "smtpout.secureserver.net"; // specify main and backup server
                $mail->Port = 80; // set the port to use
                $mail->SMTPAuth = true; // turn on SMTP authentication
                $mail->Username = "sana@unitglo.com"; // your SMTP username or your gmail username
                $mail->Password = "sana!@#"; // your SMTP password or your gmail password
                $from = "no-reply@nagarcycling.com"; // Reply to this email
                //$to = $mailids; // Recipients email ID
                $name = "Nagar Cycling"; // Recipient's name
                $mail->From = $from;
                $mail->FromName = "Ahmednagar Cycling Club"; // Name to indicate where the email came from when the recepient received
                //$mail->AddAddress ( $to, $name );
                $mailids2 = "ahmednagarcyclothon@nagarcycling.com";// ahmednagarcyclothon@nagarcycling.com
                $to = array($mailids, $mailids2);
                foreach ($to as $ad) {
                    $mail->AddAddress($ad);
                }
                $mail->AddReplyTo($from, "Ahmednagar Cycling Club");
                // $mail->WordWrap = 50; // set word wrap
                $mail->IsHTML(true); // send as HTML
                $mail->Subject = "Ahmednagar Cycling Club";
				$mypath = base_url();
				$fullpath = $mypath.'assets/images/logo.png';
				$mail->AddEmbeddedImage('logo.png','my-attach','logo.png');

                $mail->Body = "
				
											<html><body>
											   
											<table width='100%' bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0'>
											   
											<tr><td>
											   
											<table align='center' width='100%' border='0' cellpadding='0' cellspacing='0' style='max-width:650px; background-color:#fff; font-family:Verdana, Geneva, sans-serif;'>
												
											<thead>
											  <tr height='80'>
											  <th colspan='4' style='background-color:#f5f5f5; border-bottom:solid 1px #bdbdbd; font-family:Verdana, Geneva, sans-serif; color:#333; font-size:34px;' >
											  <center><img alt='Logo' src='http://www.nagarcycling.com/assets/images/logo.png'></center>
											  </th>
											  </tr>
														 </thead>
												
											<tbody>
												<tr align='center' height='50' style='font-family:Verdana, Geneva, sans-serif;'>
												   <td style='background-color:#ff0000; text-align:center;'><a href='http://www.nagarcycling.com' style='color:#fff; text-decoration:none;font-size:28px;'>Ahmednagar Cycling Club</a></td>
												  
												   </tr>
												  
												   <tr>
												   <td colspan='4' style='padding:15px;'>
												 
												   
												   <p style='font-size:20px;color:#000;'>Hi' ".$mailname.",</p>
												  
												   <p style='font-size:20px;color:#000;'>Thank You!</p>
												   <p style='font-size:20px;color:#000;'>You had successfully registered for Club Membership.!</p>
												   <p style='font-size:15px; font-family:Verdana, Geneva, sans-serif;color:#000;'> Your Rider ID is:".$rider_id.".</p>
												    <p style='font-size:15px; font-family:Verdana, Geneva, sans-serif;color:#000;'> Team NagarCyclingClub</p>
												   
												   
												   </td>
												   </tr>
												  
												   <tr height='80'>
												   <td colspan='4' align='center' style='background-color:#f5f5f5; border-top:solid #000000 4px; font-size:24px; '>
												   <label style='font-size:20px;'>For more updates follow us on : 
														  <a href='https://www.facebook.com/nagarcycling/' target='_blank'><img style='vertical-align:middle' src='https://cdnjs.cloudflare.com/ajax/libs/webicons/2.0.0/webicons/webicon-facebook-m.png' /></a>
														  <a href='https://www.instagram.com/nagarcycling/' target='_blank'><img style='vertical-align:middle' src='https://cdnjs.cloudflare.com/ajax/libs/webicons/2.0.0/webicons/webicon-instagram-m.png' /></a>
												   
														  </label>
												   </td>
												   </tr>
												  
														  </tbody>
												
											</table>
											   
											</td></tr>
											</table>
											</body></html>
				
				"
											 ; // HTML Body
                $mail->AltBody = "This is the body when user views in plain text format"; // Text Body
                $mail->Send();
				$this->sms_send($rider_id);
            } else {
                $this->session->set_flashdata('error', 'Error While Submitting');
            }
			}
		
		}
		
		
		
		public function sms_send($rider_id)
	{
		$sess_data = $this->session->userdata();
			
		 $msgno = $sess_data['phoneno'];
		  $msgname = $sess_data['name'];
		$message1 = 
		"Hi ".$msgname.",
		Thank You!
		
		You had successfully 
		registered for 
		Club Membership.
		
		Your Rider ID is: ".$rider_id."
		
		Team NagarCyclingClub"
		;
		$authKey = "164123AdNFxfY7595e3505";
	   $mobileNumber = $msgno;
	    $senderId = "nagarc";
	    $message = $message1;
	    //Define route
	    $route = "4";
	    $postData = array(
	        'authkey' => $authKey,
	        'mobiles' => $mobileNumber,
	        'message' => $message,
	        'sender' => $senderId,
	        'route' => $route
	    );
	    $url="http://sms.provanic.com/api/sendhttp.php?";
	    $ch = curl_init();
	    curl_setopt_array($ch, array(
	        CURLOPT_URL => $url,
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_POST => true,
	        CURLOPT_POSTFIELDS => $postData
	        //,CURLOPT_FOLLOWLOCATION => true
	    ));
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	   //get response
	    $output = curl_exec($ch);
	    if(curl_errno($ch))
	    {
	        echo 'error:' . curl_error($ch);
	    }
	    curl_close($ch);
	  // echo $output;exit;
	}
		

}