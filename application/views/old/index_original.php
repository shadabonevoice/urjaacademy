<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
	
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url(); ?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/magnific/magnific-popup.css">
    <link href="<?php echo base_url(); ?>assets/js/plugin/revolution/css/settings.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/js/plugin/revolution/css/layers.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/js/plugin/revolution/css/navigation.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	
	<style>
	.whycycle{
		color:#fff;
		font-size:23px;
	}
	@media (max-width: 767px){
.whycycle{
		color:#fff;
		font-size:16px;
	}
}
@media only screen and (max-width: 500px) { 
tp-parallax-wrap{
	font-size:25px;
}

div#slide-1596-layer-1.tp-caption.NotGeneric-Title,.NotGeneric-Title
{
	font-size:30px !important;
	
}
div#slide-1596-layer-2.tp-caption.NotGeneric-Title,.NotGeneric-Title
{
	font-size:30px !important;
	
}
}

@media only screen and (max-width:1180px)
{
	.cy_tour_heading h1 {
    font-size: 40px;
}
}



	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Loader Start-->
    <div class="loader_wrapper"> 
        <div class='loader'>
			<img src="<?php echo base_url(); ?>assets/images/loader.gif" alt="loader">	
        </div>
    </div>
    <!--Top bar start-->
    <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
							<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>CONTACT: 8308054000</li>
                                <li>
                                    <ul> 
                                        <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    
    
    <!--Banner section start-->
    <div class="cy_banner_wrapper">
        <div class="container-fluid">
            <div class="row">
                <div id="rev_slider_1068_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="levano4export" data-source="gallery" style="background-color:transparent;padding:0px;">
                    <div id="rev_slider_1068_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">
                        <ul>
                            <li data-index="rs-1596" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="500" data-thumb="" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo base_url(); ?>assets/images/Slider1.jpg" alt="banner" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-1596-layer-1" data-x="['left','left','left','left']" data-hoffset="['-110','50','50','50']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['65','55','45','45']" data-lineheight="['85','55','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2500,"to":"o:1;","delay":1000,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,20,20,20]" style="z-index: 5; white-space: nowrap;text-transform:left;color:#ffffff;font-family: 'Montserrat', sans-serif;text-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2);text-transform:uppercase;">
								
								A Big Thank you <br>to all Cyclists<br>for your <br>participation<br> in Ahmednagar <br>Cyclothon 2018<br>
								<!--<a href="<?php echo base_url(); ?>index.php/contact"><button class="cy_button next">Your Feedback</button><a>-->
								
								<a href="<?php echo base_url(); ?>index.php/contact" class="cy_button">Your Feedback is valuable to us</a>
								
								
                                </div>
                            </li>
							
							 <!--slide2-->
                            <li data-index="rs-3010" data-transition="zoom" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-thumb="" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo base_url(); ?>assets/js/plugin/revolution/assets/main_banner.jpg" alt="banner" data-bgposition="center center" data-kenburns="on" data-duration="2000" data-ease="Power2.easeOut" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 500" data-offsetend="0 -500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-1596-layer-2" data-x="['left','left','left','left']" data-hoffset="['-110','50','50','50']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['65','55','45','45']" data-lineheight="['85','55','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2500,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,20,20,20]" style="z-index: 5; white-space: nowrap;text-transform:left;color:#ffffff;font-family: 'Montserrat', sans-serif;text-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2);text-transform:uppercase;">don't limit <br> your  challenges,<br>  challenge<br> your limits!
                                </div>      
                            </li>
							
							<li data-index="rs-4596" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="500" data-thumb="" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo base_url(); ?>assets/js/plugin/revolution/assets/main_banner1.jpg" alt="banner" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-1596-layer-1" data-x="['left','left','left','left']" data-hoffset="['-110','50','50','50']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['65','55','45','45']" data-lineheight="['85','55','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2500,"to":"o:1;","delay":1000,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,20,20,20]" style="z-index: 5; white-space: nowrap;text-transform:left;color:#ffffff;font-family: 'Montserrat', sans-serif;text-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2);text-transform:uppercase;"> WHEN IN DOUBT,
                                    <br> PEDAL IT OUT!<br>
                                </div>
                            </li>
                         
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
	
	
    <!--Menus Start-->
    <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url(); ?>index.php/index"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>index.php/index" class="active">home</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url(); ?>index.php/join">join</a></li>
								 <li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url(); ?>index.php/routes">routes</a></li>
								  <li><a href="<?php echo base_url(); ?>index.php/events">events</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
    <!--About section start-->
    <div class="cy_about_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="cy_about_img">
                        <img src="<?php echo base_url(); ?>assets/images/about/about.png" alt="about" class="img-fluid"/>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">ABOUT US</h1>
                        <p>
						Ahmednagar Cyclothon is officially affiliated with <b>Cycling Federation of India(CFI)</b>, <b>Kalpattaru Enterprises India Limited</b> and <b>Cycling Association of Maharashtra(CAOM)</b>.
						The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of
							cycling in the Ahmednagar community. The Club was formed to promote the cause of
							keeping our city green, while keeping our bodies healthy and clean. We welcome
							members of the community to our club events such as group rides, Cyclothons and
							community service activities.
						</p>
						<center>
						<h5 class="cy_heading">PROUDLY AFFILIATED WITH</h5>
						
						<img src="<?php echo base_url(); ?>assets/images/cfi.jpg" alt="event" class="img-fluid">
						<img src="<?php echo base_url(); ?>assets/images/maha.png" alt="event" class="img-fluid">
						</center>
						</br>
                        <a href="<?php echo base_url(); ?>index.php/about" class="cy_button">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
	 <div class="cy_achieve_wrapper">
        <div class="cy_achieve_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="cy_achieve_img wow cy_anim_left">
                        <img src="<?php echo base_url(); ?>assets/images/achieve/cycle.png" alt="achievement" class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_counter_wrapper">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <h1 class="cy_heading">Why Cycle?</h1>
                            </div>
							
							
							
                            <div class="col-lg-12 col-md-12">
                                <div class="cy_count_box" style="margin-top:0px;">
								 
								 
								 <div class="widget widget_categories">
                           
									<ul>
										<li class="whycycle" >Cycling provides several health benefits.</li>
										<li class="whycycle" >Cycling is one of the best modes of transportation that is fast, safe and pollution-free.</li>
										
									</ul>
								</div>
								 
								 
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <!--Events Sevtion Start-->
   <!-- <div class="cy_event_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Upcoming Event</h1>
                </div>
            </div>
  <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/event1.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 6:00 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>

                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2  style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 20KM Cyclothon</h2>
							   <center>
							 <a href="<?php echo base_url(); ?>index.php/welcome" class="cy_button">Registration Fee: </b>&#8360; 400/-</a>
                            </center>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/event2.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 5:45 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2  style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 50KM Cyclothon</h2>
							  <center>
							 <a href="<?php echo base_url(); ?>index.php/welcome" class="cy_button">Registration Fee: </b>&#8360; 500/-</a>
                            </center>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/event3.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 5:30 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 100km Cyclothon</h2>
							
							 <center>
							 <a href="<?php echo base_url(); ?>index.php/welcome" class="cy_button">Registration Fee: </b>&#8360; 600/-</a>
                            </center>
							
                           
                        </div>
                    </div>
                </div>
            </div> 
			 </div>
    </div>-->
	
	
	 <div class="cy_gallery_wrapper cy_section_padding padder_bottom100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Our gallery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image1.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image1.jpg" class="fa fa-search"></a>
                           
                        </div>
                    </div>
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image2.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image2.jpg" class="fa fa-search"></a>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/large2.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/large2.jpg" class="fa fa-search"></a>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image4.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image4.jpg" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image5.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image5.jpg" class="fa fa-search"></a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/fullarge1.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/fullarge1.jpg" class="fa fa-search"></a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image7.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image7.jpg" class="fa fa-search"></a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image8.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image8.jpg" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/fullarge2.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/fullarge2.jpg" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/large1.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/large1.jpg" class="fa fa-search"></a>
                          
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image6.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image6.jpg" class="fa fa-search"></a>
                           
                        </div>
                    </div>
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image3.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image3.jpg" class="fa fa-search"></a>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
	
	
	 <!--Cycle Tour section start-->
    <div class="cy_tour_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="cy_tour_data">
                        <h1>RENT A BICYCLE FOR AHMEDNAGAR CYCLOTHON 2018!</h1>
                        <a href="<?php echo base_url(); ?>index.php/welcomerentbicycle" class="cy_button">RENT BICYCLE</a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <div class="cy_tour_heading">
                        <h1>Ahmednagar cyclothon</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!--Footer section start-->
    <div class="cy_footer_wrapper cy_section_padding padder_bottom75">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget cy_footer_about">
                        <img src="<?php echo base_url(); ?>assets/images/footer/footer_logo.png" alt="logo" class="img-fluid"/>
                       <p style="text-align:justify;">
						Ahmednagar Cycling Club actively encourages cyclists of all abilities to meet their personal goals. Whether you cycle for fun, for the health benefits or for the competition, Ahmednagar Cycling Club welcomes you.

						</p>
                    </div>  
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget widget_categories">
					<h1 class="widget-title">Important Links</h1>
						<ul style="list-style-type: none;">
							<li style="padding: 8px 0px;"><a href="<?php echo base_url(); ?>index.php/join">JOIN</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url(); ?>index.php/rides">RIDES</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url(); ?>index.php/routes">ROUTES</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url();?>index.php/welcome">REGISTRATION</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url(); ?>index.php/terms">TERMS & CONDITIONS</a></li>
						</ul>
					</div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h1 class="widget-title">Venue Details</h1>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url(); ?>assets/images/svg/map-mark.svg" alt="map-mark"></span>
                        <div class="cy_post_info">
                        <p>IMA Bhavan- Near Railway over bridge, Kalyan Road - Ahmednagar</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url(); ?>assets/images/svg/phone.svg" alt="phone"></span>
                        <div class="cy_post_info">
                        <p>Contact1 :- 8308054000</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url(); ?>assets/images/svg/email.svg" alt="email"></span>
                        <div class="cy_post_info">
                        <p><a href="#">support@nagarcycling.com</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                   <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAhmednagar-Cycling-Club-1900441513602454%2F&tabs=timeline&width=300&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1330903350339636" width="300" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>  
            </div>
        </div>
    </div>
    <!--Bottom footer start-->
    <div class="cy_btm_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <P>Copyright &copy;2018  <b style="color:#ff0000;">Ahmednagar Cycling Club</b>. All Rights Reserved. Developed By<a style="color:#e6c33e;" href="http://www.onevoicetransmedia.com" target="_blank"> OneVoice Transmedia Pvt. Ltd.</a></P>
                </div>
            </div>
        </div>
    </div>
        <!--Go to top start-->
    <div class="cy_go_to">
        <div class="cy_go_top">
            <img src="<?php echo base_url();?>assets/images/go_to_top.png" alt="Back to top">
        </div>  
    </div>
    <!--scripts start-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.countTo.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/appear.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wow.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/owl/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/magnific/jquery.magnific-popup.min.js"></script>
    <!--Revolution slider js-->
    <script src="<?php echo base_url(); ?>assets/js/plugin/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/revolution/js/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/revolution/js/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/revolution/js/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/revolution/js/revolution.extension.parallax.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/revolution/js/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

</body>

<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:13:41 GMT -->
</html>