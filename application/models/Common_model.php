<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model 
{
	
	public function __construct()
	{
		parent::__construct();
		$this->current_year=date("Y");
	} 
	public function add_records($table_name,$insert_array)
	{
		if (is_array($insert_array)) 
		{
			if ($this->db->insert($table_name,$insert_array))
				return true;
			else
				return false;
		}
		else 
		{
			return false; 
		}
	}
	
	
	public function update_records($table_name,$update_array,$where_array)
	{
		if (is_array($update_array) && is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->update($table_name,$update_array))
			{				 
				return true;
			}	
			else
			{
				return false;
			}	
		} 
		else 
		{
			return false;
		}
	}
	
	
	public function delete_records($table_name,$where_array)
	{ 
		if (is_array($where_array)) 
		{  
			$this->db->where($where_array);
			if($this->db->delete($table_name))
				return true;
			else
				return false;
		} 
		else 
		{
			return false;
		}
	}
	
	public function get_records($table_name,$filed_name_array=FALSE,$where_array=FALSE,$single_result=FALSE)
	{
		if(is_array($filed_name_array) && isset($filed_name_array))
	  	{
	  		$str=implode(',',$filed_name_array);
			$this->db->select($str);
		}
		
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
		}

		$result=$this->db->get($table_name);
		
		if($single_result==true && isset($single_result))
		{
			return $result->row_array();
		} 
		else 
		{
			return $result->result_array();			
		}		
	}
	
	public function get_records_with_sort($table_name,$filed_name_array=FALSE,$where_array=FALSE,$single_result=FALSE,$sort_order=FALSE)
	{
		if(is_array($filed_name_array) && isset($filed_name_array))
	  	{
	  		$str=implode(',',$filed_name_array);
			$this->db->select($str);
		}
		
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
		}
		
		if(isset($sort_order) && $sort_order!='')
		{
			$this->db->order_by($sort_order);
		}

		$result=$this->db->get($table_name);
		
		if($single_result==true && isset($single_result))
		{
			return $result->row_array();
		} 
		else 
		{
			return $result->result_array();			
		}		
	}
	
	public function get_records_subcat()
	{
		$this->db->select('wwc_manage_subcategory.*,wwc_manage_category.cat_name');
		$this->db->where('wwc_manage_subcategory.status !=','2');
		$this->db->where('wwc_manage_category.status ','1');
		$this->db->join('wwc_manage_category','wwc_manage_category.cat_id=wwc_manage_subcategory.cat_id');
		$result=$this->db->get('wwc_manage_subcategory');
		return $result->result_array();
	}
	
	public function get_records_testomonial()
	{
		$this->db->select('tbl_testomonial.*,tbl_client.*');
		$this->db->where('tbl_testomonial.status !=','2');
		$this->db->where('tbl_client.status ','1');
		$this->db->join('tbl_client','tbl_client.client_id=tbl_testomonial.client_id');
		$result=$this->db->get('tbl_testomonial');
		//print_r($result->result_array());exit;
		return $result->result_array();
	}
	
	public function get_category($category_id)
	{
		$this->db->where('tbl_category.category_id',$category_id);
		return $result=$this->db->get('tbl_category')->result_array();
	}
	public function get_gallery()
	{
		$this->db->limit('16');
		$this->db->where('status','1');
		$gallery1=$this->db->get('tbl_gallery');
		return $gallery1->result_array();
	}
	public function get_next_gal($last_record)
	{
		//print_r($last_record);exit;
		$this->db->limit('8');
		$this->db->where('status','1');
		$this->db->where('gallery_id >',$last_record);
		return $result=$this->db->get('tbl_gallery')->result_array();
		//print_r($result);exit;
	}
	public function get_all_gal($last_record)
	{
		$this->db->where('status','1');
		$this->db->where('gallery_id >',$last_record);
		return $result=$this->db->get('tbl_gallery')->result_array();
	}
	public function createThumb($file_name,$path,$width,$height,$maintain_ratio=FALSE)
	{
		$config_1['image_library']='gd2';
		$config_1['source_image']=$path.$file_name;		
		$config_1['create_thumb']=TRUE;
		$config_1['maintain_ratio']=$maintain_ratio;
		$config_1['thumb_marker']='';
		$config_1['new_image']=$path."thumb/".$file_name;
		$config_1['width']=$width;
		$config_1['height']=$height;
		$this->load->library('image_lib',$config_1);	
		if(!$this->image_lib->resize())
		echo $this->image_lib->display_errors();
	}
	public function getTotalRegistered()
	{
		$this->db->select('*');
		$this->db->where('status','success'); 
		$this->db->where('DATE_FORMAT(create_at, "%Y")=',$this->current_year); 
		$result=$this->db->get('tbl_register');
		return $result->result();
	}
	public function getTotalRentBicycle()
	{
		$this->db->select('*');
		$this->db->where('status','success'); 
		$this->db->where('DATE_FORMAT(created_at, "%Y")=',$this->current_year); 
		$result=$this->db->get('tbl_rentbicycle');
		return $result->result();
	}
	
	public function getTotalRegisteredPromo()
	{
		$this->db->select('*');
		$this->db->where('status','success'); 
		$this->db->where('DATE_FORMAT(create_at, "%Y")=',$this->current_year); 
		$result=$this->db->get('tbl_register_promo');
		return $result->result();
	}
	
	public function sendSMS($to_mobileno, $sendmsg) {
        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://control.msg91.com/api/postsms.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "<MESSAGE> <AUTHKEY>228445Am2S94aqL0L5b5aa9b7</AUTHKEY> <SENDER>URJAAD</SENDER> <ROUTE>4</ROUTE> <CAMPAIGN>XML API</CAMPAIGN> <COUNTRY>91</COUNTRY> <SMS TEXT=\"$sendmsg\" > <ADDRESS TO=\"$to_mobileno\"></ADDRESS> </SMS>  </MESSAGE>",
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"content-type: application/xml"
		  ),
		));
        //NGRCYG
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
    }
     public function update_registration_email_status($transaction_id,$updatedata){
            $this->db->where('transaction_id', $transaction_id);
        $this->db->update('tbl_register', $updatedata);
        return TRUE;
    }
     public function update_rent_email_status($transaction_id,$updatedata){
            $this->db->where('transaction_id', $transaction_id);
        $this->db->update('tbl_rentbicycle', $updatedata);
        return TRUE;
    }
     public function update_promo_status($transaction_id,$updatedata){
            $this->db->where('transaction_id', $transaction_id);
        $this->db->update('tbl_register_promo', $updatedata);
        return TRUE;
    }
}
