<?php
$this->load->view('common/header.php');
?>
   
   <link rel="stylesheet" href="assets/css/events.css">
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="cy_about_data">
                       <h1 class="cy_heading">Ahmednagar  Cyclothon  Event Info</h1>
                        
                    </div>
                </div>
              <!--  <div class="col-lg-6 col-md-12">
                    <div class="cy_about_img">
                        <img src="images/about/about1.jpg" alt="about" class="img-fluid" />
                    </div>
                </div>-->
            </div>
        </div>
    </div>
	
	<div class="cy_achieve_wrapper">
        <div class="cy_achieve_overlay"></div>
        <div class="container">
            <div class="row">
			   <div class="col-lg-6 col-md-12">
                    <div class="cy_counter_wrapper">
                        <div class="row">
                            
							
                            <div class="col-lg-12 col-md-12">
                                <div class="cy_count_box" style="margin-top:0px;">
								 
								<p style="text-transform: none;"> Come out and ride with us to promote awareness of cycling as an alternate mode of
									transportation that will help keep our precious city clean and green and lead to a
									healthier lifestyle.
															</p>
															<br><br>
								<center><p><b> Date: Sunday, February 17, 2018</b></p><br>
								<p><b>Race Categories </b></p><br>
								<p><b>20KM, 50KM &100KM </b></p><br>
								<p style="text-transform: none;">(Routes, reporting times and flag off times to be announced) </p><br>
								
								<p><b>RACE DIRECTOR</b></p>
								<p>Ironman Sunil Menon</p>
								
								
								<br><br>
								</center>
								
								 
								 
                                </div>
                            </div>
							
							
                        </div>
                    </div>
                </div>
           
               <div class="col-lg-6 col-md-12">
			   <br>
			   
			   
			   <span class="col-md-3"></span>
									<div class="col-lg-10 col-md-12">
										<div class="cy_price_box">
											
											<div class="cy_price_body">
											  <div class="cy_price_head">
												<h3>Organised By:</h3>
												
											</div>
											<div class="widget widget_categories">
											   
												<ul>
											<li style="color:#fff;"><b>Gaurav Firodia </b></li>
											<li style="color:#fff;"><b>Kalyani Firodia</b> </li>
											<li style="color:#fff;"><b>Dr. Abhijeet Pathak</b></li>
											<li style="color:#fff;"><b>Dr. Renuka Pathak </b></li>
											<li style="color:#fff;"><b>Dr. Mahesh Mulay </b></li>
											<li style="color:#fff;"><b>Dinesh Sanklecha</b></li>
											<li style="color:#fff;"><b>Tushar Patwa</b></li>
											<li style="color:#fff;"><b>Ravi Patre</b></li>
											<li style="color:#fff;"><b>Ravindra Pisore</b></li>
											<li style="color:#fff;"><b>Jyoti Pisore</b></li>
												</ul>
											</div>
											  
											</div>
										</div>
									</div>	
							<span class="col-md-1"></span>			
										
									</div>
              </div>
			  
			  
			 
        </div>
		
		 
    </div>
	<br><br>
	 <center>
	<h5 style="text-transform: none;"><b>Cyclothon participants will get:</b> T-shirt, Bib, Finisher medal, Participation certificate, Breakfast, and Hydration points.
								</h5>
   </center>
 <br><br>
 <?php
$this->load->view('common/footer');
        ?>