<?php
$this->load->view('common/header.php');
?>   
<script>
var base_url = '<?php echo base_url()?>index.php/';
</script>
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!-- checkout section start -->
    	<div class="" style="background-color: #a28f11;">
			<div class="container">
				<div class="row">
					<!--style="background-color: #93b3be;"-->
					<div class="col-lg-12 col-md-12 " style="background-color: #00000021;">
						<div class="cy_checkout_box">
						<!--<img src="<?php// echo base_url();?>assets/images/thanku.png" alt="Thank U" style="width:100%; height:auto;">-->
						<ul id="progressbar">
							<!--<li class="active">Register</li>-->
							<?php echo (@validation_errors()) ?  '<br><li class="active">'.validation_errors().'</li>' : '' ;?>
							<!--<?php// echo (@$this->session->userdata('success')) ?  '<br><li class="active">'.$this->session->userdata('success').'</li>' : '' ;?>-->
							<?php echo (@$this->session->userdata('error')) ?  '<br><li class="active">'.$this->session->userdata('error').'</li>' : '' ;?>
						</ul>
						<center><h3  style="margin-top: 2%; margin-bottom: 2%; color:#f3d001;"> <b>नोंदणी</b></h3></center>
					   <form method="post" id="product_info" name="product_info" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/Urja/check">  
						 <div class="woocommerce-billing-fields step">	
							<h4> <b>उमेदवाराचा तपशील  :-</b></h4><br>
							<div class="row">
								<div class="form-group col-sm-4"> नाव :	   
									<input type="text"  name="fname" id="fname" class="form-control" placeholder="Enter First Name (Only alphabets)" required />
								</div>
								<div class="form-group col-sm-4"> वडिलांचे / आईचे नाव :	   
									<input type="text"  name="mname" id="mname" class="form-control" placeholder="Enter Father's / Mother's Name (Only alphabets)" required />
								</div>
							
								<div class="form-group col-sm-4"> आडनाव :  
									<input type="text"  name="lname" id="lname" class="form-control" placeholder="Enter Last Name (Only alphabets)" required />
								</div>
								<div class="form-group col-sm-4"> मराठीत पूर्ण नाव :
									<input type="text"  name="fullname" id="fullname" class="form-control" placeholder="Enter Full Name (In Marathi)" required />
								</div>
							
								<div class="form-group col-sm-4"> जन्मतारीख :
									<input type="text" class="form-control" id="birthdate" name="dob" placeholder="Date of Birth(MM/DD/YYY)" required />
									<!--<span ><b>Note : </b>वय 40 पेक्षा मोठे असावे ( 31 मे 2019 पर्यंत पूर्ण असावे)</span>-->
								</div>
								<div class="form-group col-sm-4">
									<label>लिंग :</label><br>
									<input type="radio" name="gender" value="male" checked style="display:inline-block;"> पुरुष  &nbsp; &nbsp;
									<input type="radio" name="gender" value="female" style="display:inline-block;"> स्त्री
								</div>
							
								<div class="form-group col-sm-4"> ईमेल आय.डी. (पालक / विद्यार्थी) :
								  <input type="email"  name="email" id="email" class="form-control" placeholder="Email" required />
								  <span class="text-warning" id="emailError"></span>
								</div>
								<div class="form-group col-sm-8">पत्रव्यवहाराचा पत्ता :
								  <textarea class="form-control" name="address" id="address" placeholder="Enter Address" required ></textarea>
								</div>				
							
								<div class="form-group col-sm-4"> मोबाइल क्रमांक	:			
								  <input type="number"  name="mobilenumber" id="mobilenumber" class="form-control" placeholder="Mobile Number(10 digits)" required />
									<span class="text-warning" id="mobileNumberError"></span>
								</div>
								<div class="form-group col-sm-4"> पिनकोड :			
								  <input type="number"  name="pincode" id="pincode" class="form-control" placeholder="Enter pincode" required />
								</div>
							
								<!--<div class="form-group col-sm-4"> दूरध्वनी क्रमांक :			
								  <input type="number"  name="telephone" id="telephone" class="form-control" placeholder="Enter telephone no" />
								</div>-->
							</div>							
							<hr>							
							<h4><b> पालकांचा तपशील / कायदेशीर पालकांचा तपशील ( उमेदवार १८ वर्षांखालील असल्यास ) :-</b></h4>
							<br>
							<div class="row">
								<div class="form-group col-sm-4"> वडिलांचे नाव	:   
									<input type="text"  name="fathername" id="fathername" class="form-control" placeholder="Enter Father Name (Only alphabets)" />
								</div>
								<div class="form-group col-sm-4"> मोबाइल क्रमांक :			
								  <input type="number"  name="fathermobilenumber" id="fathermobilenumber" class="form-control" placeholder="Mobile Number(10 digits)" />
								</div>
								<div class="form-group col-sm-4"> दूरध्वनी क्रमांक :			
								  <input type="number"  name="fathertelephone" id="fathertelephone" class="form-control" placeholder="Enter Father telephone no" />
								</div>				
								<div class="form-group col-sm-4"> आईचे नाव	: 
									<input type="text"  name="mothername" id="mothername" class="form-control" placeholder="Enter Mother's Name (Only alphabets)" />
								</div>
								<div class="form-group col-sm-4"> मोबाइल क्रमांक :			
								  <input type="number"  name="mothermobilenumber" id="mothermobilenumber" class="form-control" placeholder="Mobile Number(10 digits)" />
								</div>
								<div class="form-group col-sm-4"> दूरध्वनी क्रमांक :				
								  <input type="number"  name="mothertelephone" id="mothertelephone" class="form-control" placeholder="Enter Mother telephone no" />
								</div>								
								<div class="form-group col-sm-4"> पालकांचे नाव	:   
									<input type="text"  name="guardianname" id="guardianname" class="form-control" placeholder="Enter Guardian Name (Only alphabets)" />
								</div>
								<div class="form-group col-sm-4"> मोबाइल क्रमांक :				
								  <input type="number"  name="guardianmobilenumber" id="guardianmobilenumber" class="form-control" placeholder="Mobile Number(10 digits)" />
								</div>
								<div class="form-group col-sm-4"> दूरध्वनी क्रमांक :				
								  <input type="number"  name="guardiantelephone" id="guardiantelephone" class="form-control" placeholder="Enter Guardian telephone no" />
								</div>
							</div>
							<hr>
							<h4><b> वैद्यकीय अटी :-</b></h4>
							<div class="form-group">आपल्याला कुठलाही दीर्घकालीन आजार किंवा आपल्या अपंगत्वाची आपणास जाणीव आहे का ?
								<small>( जर आपली निवड करण्यात आली तर, शक्य तेवढी मदत करण्यासाठी ही माहिती फायदेशीर ठरू शकते ) :</small>
								  <textarea class="form-control" name="medicalcondition" id="medicalcondition" placeholder="Medical Terms [Do you have any health issue or any disability? (If you are selected, this information can be useful to help you as much as possible)]" ></textarea>
							</div>
							
							<h4><b> स्वमत :-</b></h4>
							<div class="form-group">आपले मत हे आपण या अभ्यासक्रमासाठी कसे आणि का पात्र आहात ? याविषयी असावे  :
							  <textarea class="form-control" name="personalstatement" id="personalstatement" placeholder="Personal Statement [Please explain, how are you eligible for this course?]" required></textarea>
							</div>
							
							<h4> <b>शैक्षणिक पात्रता :-</b></h4>
							<div class="form-group">शिक्षण :
							  <textarea class="form-control" name="academiceducation" id="academiceducation" placeholder="Educational Qualifications" ></textarea>
							</div>
							
							<h4> <b>इतर पात्रता :-</b></h4>
							<div class="row">
								<div class="form-group col-sm-4">नाटक :
								  <textarea class="form-control" name="drama" id="drama" placeholder="Drama" ></textarea>
								</div>
								<div class="form-group col-sm-4">नृत्य :
								  <textarea class="form-control" name="dance" id="dance" placeholder="Dance" ></textarea>
								</div>
								<div class="form-group col-sm-4">संगीत :
								  <textarea class="form-control" name="music" id="music" placeholder="Music" ></textarea>
								</div>
							</div>
							
							<div class="form-group">वर्तमान शाळेचे नाव, महाविद्यालय किंवा इतर :
							  <input type="text" class="form-control" name="presentschool" id="presentschool" placeholder="Name of your current school or college" required></textarea>
							</div>
							
							<div class="form-group">काम व सादरीकरण व्यवसायिक
								<small> नाट्यगृह, शिकावू नाटक व इतर सहभाग ? :</small>
								<textarea class="form-control" name="work" id="work" placeholder="Where are you currently working? (Like business, theatre or any other participation)" ></textarea>
							</div>
							
							<div class="form-group">विशेष कौशल्य साधने 
								<small> खेळ, भाषा इत्यादी.  :</small>
								<textarea class="form-control" name="skill" id="skill" placeholder="Any other special skills like sports, language etc." ></textarea>
							</div>	
							<div class="form-group">
								<input type="checkbox" required>
								मी <a href="<?php echo site_url('index.php/urja/terms');?>" target="_blank" style="color : #d2d623;"> नियम व अटींशी सहमत आहे.</a>
							</div>
							<center>
								<div class="form-group">
								  <button type="submit" name="submit" id="nextBtn" class="btn btn-warning">समाविष्ट करा</button>
								  <button class="btn btn-secondary" type="reset">पुसून टाका</button>
								</div>
							</center>
							<div class="cy_about_data">
								<p style="font-size: 18px; float: right;">Initiative of Urja Academy</p>				
							</div>
							</div>
						</form> 			
									
					 </div>
					</div>
				</div>
			</div>
		</div>
    <!--Footer section start-->
	





	<script>
		function myFunction(e) {
		
		
		var ab=e.target.value;
		
		if(ab == "20KM")
		{
		var amt = 500;
		document.getElementById("payble_amount").value = amt;
		}
		else if(ab == "50KM")
		{
		var amt1 = 600;
		document.getElementById("payble_amount").value = amt1;
		}
		else if(ab == "100KM")
		{
		var amt2 = 700;
		document.getElementById("payble_amount").value = amt2;
		}
		else{
		//document.getElementById("amount").value = "Total Amount";
		}

		  
		}
		</script>
		<?php
$this->load->view('common/footer');
		?>
		<link rel="stylesheet" href="assets/calender/jquery-ui.css">

    <script>
      $( function() {
        $( "#birthdate" ).datepicker({
          readOnly: true,
          changeMonth: 1,
          changeYear: true,
          numberOfMonths: 1,
          maxDate:0,
          minDate:null,
          yearRange: '1979:2003',
          defaultDate: new Date(2003,05,01)
        });
      } );
  </script>
  <script src="assets/calender/jquery-ui.js"></script>
  <!--<script src="<?php //echo base_url();?>assets/js/register/register.js"></script>-->
  <script>
  $(document).ready(function(e) {
		$('#mobilenumber').change(function() {
			var mobilenumber = $('#mobilenumber').val();
			var data = {
						'check': mobilenumber,
						'col': 'mobile'
						};
			$.ajax({
					type: "POST",
					url: base_url + 'Urja/check_duplicate',
					data: data,
					success: function(result) {
						console.log(result);
						var obj = jQuery.parseJSON(result);
						if(obj.status == 'fail'){
							$('#nextBtn').attr("disabled",true);
						}else{
							$('#nextBtn').attr("disabled",false);
						}
						$('#mobileNumberError').html(obj.message);
					}
				});		
		});
		$('#email').change(function() {
			var email = $('#email').val();
			var data = {
						'check': email,
						'col': 'email'
						};
			$.ajax({
					type: "POST",
					url: base_url + 'Urja/check_duplicate',
					data: data,
					success: function(result) {
						console.log(result);
						var obj = jQuery.parseJSON(result);
						if(obj.status == 'fail'){
							$('#nextBtn').attr("disabled",true);
						}else{
							$('#nextBtn').attr("disabled",false);
						}
						$('#emailError').html(obj.message);
					}
				});		
		});
	});
  </script>