<?php
$this->load->view('common/header.php');
?>
   
    <!--About section start-->
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
			 <div class="col-lg-4 col-md-12">
                    <div class="cy_about_img">
                        <img src="<?php echo base_url(); ?>assets/images/about/join.png" alt="about" class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">Joining the club is easy!</h1>
                        <p>To ride with us, just submit your online registration This will allow you to join us on any rides
							and events you want. You can find out about rides on the “Rides” section of our website.
						</p>
                        <p>Clearly, riding with us will make you want more! If you want access to perks such as
							participating in coached skills clinics and training plans, then become a Preferred
							Member today!
						</p>
                       <!-- <a href="#" class="cy_button">read more</a>-->
                    </div>
                </div>
               
            </div>
        </div>
    </div>
	
	  <div class="cy_price_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Club Membership</h1>
                </div>
            </div>
            <div class="row">
			
				<span class="col-lg-2"></span>
			
                <div class="col-lg-4 col-md-12">
                    <div class="cy_price_box">
                        <div class="cy_price_head">
                            <h3>Basic Club Membership</h3>
                            <!--<h1>&#8377;1000</h1>-->
                        </div>
                        <div class="cy_price_body">
						
						<div class="widget widget_categories">
                           
                            <ul>
                                <li><a href="#"> Access to weekly club rides</a></li>
                                <li><a href="#">Cyclothon registration (20KM only)</a></li>
                                <li><a href="#">Club t-shirt</a></li>
                                <li></li>
								<li></li>
							</ul>
                        </div>
                           <!-- <ul>
                                <li>Access to weekly club rides</li>
                                <li>Cyclothon registration (20KM only)</li>
                                <li>Club t-shirt</li>
								<li></li>
								<li></li>
                                
                            </ul>-->
                            <!--<div class="cy_price_btn">
                                <a href="<?php echo base_url(); ?>index.php/register" class="cy_button">register now</a>
                            </div>-->
                        </div>
                    </div>
                </div>
				
                <div class="col-lg-4 col-md-12">
                    <div class="cy_price_box">
                        <div class="cy_price_head">
                            <h3>Preferred Club Membership</h3>
                            <!--<h1>&#8377;2000</h1>-->
                        </div>
                        <div class="cy_price_body">
						<div class="widget widget_categories">
                            <ul>
                                <li><a href="#">Access to coached skills clinics</a></li>
                                <li><a href="#">Access to training plans</a></li>
                                <li><a href="#">Access to weekly club rides</a></li>
                                <li><a href="#">Cyclothon registration (20KM only)</a></li>
								 <li><a href="#">Club t-shirt</a></li>
                            </ul>
						</div>
                            <!--<div class="cy_price_btn">
                                <a href="<?php echo base_url(); ?>index.php/register" class="cy_button">register now</a>
                            </div>-->
                        </div>
                    </div>
                </div>
                <span class="col-lg-2"></span>
            </div>
        </div>
    </div>
  
	
	
    <!--Footer section start-->
     <?php
$this->load->view('common/footer');
        ?>
	