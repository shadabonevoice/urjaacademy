<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->helper('security');
		$this->load->model('common_model');
	}  
	 
	public function index()
	{
		
				if(isset($_POST['myuser']))

		{
		
			//print_r($_POST);exit;

			$this->form_validation->set_rules('urname','Name','required');

			if($this->form_validation->run())

			{
				$insert_array=array(
				
				'urname'=>$this->input->post('urname'),

				'urmobile'=>$this->input->post('urmobile'),

				'uraddress'=>$this->input->post('uraddress'),
				
				'urcity'=>$this->input->post('urcity'),
				
				'urstate'=>$this->input->post('urstate'),
				
				'urpostcode'=>$this->input->post('urpostcode'),
				
				'urcountry'=>$this->input->post('urcountry'),
				
				'bodate'=>$this->input->post('bodate'),
				
				'urage'=>$this->input->post('urage'),
				
				'urblood'=>$this->input->post('urblood'),
				
				'race_category'=>$this->input->post('race_category'),
				
				'uramt'=>$this->input->post('uramt'),
				
				'uremail'=>$this->input->post('uremail'),
				
				'tshirt'=>$this->input->post('tshirt'),
				
				'uremrname'=>$this->input->post('uremrname'),
				
				/*'uremrnumber'=>$this->input->post('uremrnumber'),*/
				
				
				
				'created_at'=>date('Y-m-d H:i:s')

				);	
				//print_r($insert_array);exit;
				
				
				
				

				if($this->common_model->add_records('tbl_registration',$insert_array))

				{

					$this->session->set_flashdata('success','Your Message Submitted Sucessfully');

					redirect(base_url('index.php/register'));

				}

				else

				{

					$this->session->set_flashdata('error','Error While Submitting ,Try again');

					redirect(base_url('index.php/register'));

				}

			}
			
			
		
				 
				 
			
		}
		
		$this->load->view('register');
		$this->load->view('footer');
	}
}
