<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->
<!-- 
Template Name: Cycling- Html Template
Version: 1.0.0
Author: Kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:12:38 GMT -->
<head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
	
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url(); ?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	
	<style>
	
	.cy_price_box {
    overflow: hidden;
    background-color: rgba(0, 0, 0, 0.35);
    border-bottom: 2px solid #ff0000;
    transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.s ease-in-out;
    -ms-transition: all 0.4s ease-in-out;
    height: 400px;
}

@media only screen and (max-width:991px)
{
.cy_price_head {
  
    padding: 30px 0px 65px;
  
}
}
	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
   <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
							<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>PHONE: 8308054000</li>
                                <li>
                                    <ul>
                                          <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Join the Club</h1>
                </div>
            </div>
        </div>
    </div>
     <!--Menus Start-->
  <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url();?>index.php/index"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url(); ?>index.php/join" class="active">join</a></li>
								 <li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url(); ?>index.php/routes">route</a></li>
								  <li><a href="<?php echo base_url(); ?>index.php/events">events</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
    <!--About section start-->
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
			 <div class="col-lg-4 col-md-12">
                    <div class="cy_about_img">
                        <img src="<?php echo base_url(); ?>assets/images/about/join.png" alt="about" class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">Joining the club is easy!</h1>
                        <p>To ride with us, just submit your online registration This will allow you to join us on any rides
							and events you want. You can find out about rides on the “Rides” section of our website.
						</p>
                        <p>Clearly, riding with us will make you want more! If you want access to perks such as
							participating in coached skills clinics and training plans, then become a Preferred
							Member today!
						</p>
                       <!-- <a href="#" class="cy_button">read more</a>-->
                    </div>
                </div>
               
            </div>
        </div>
    </div>
	
	  <div class="cy_price_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Club Membership</h1>
                </div>
            </div>
            <div class="row">
			
				<span class="col-lg-2"></span>
			
                <div class="col-lg-4 col-md-12">
                    <div class="cy_price_box">
                        <div class="cy_price_head">
                            <h3>Basic Club Membership</h3>
                            <!--<h1>&#8377;1000</h1>-->
                        </div>
                        <div class="cy_price_body">
						
						<div class="widget widget_categories">
                           
                            <ul>
                                <li><a href="#"> Access to weekly club rides</a></li>
                                <li><a href="#">Cyclothon registration (20KM only)</a></li>
                                <li><a href="#">Club t-shirt</a></li>
                                <li></li>
								<li></li>
							</ul>
                        </div>
                           <!-- <ul>
                                <li>Access to weekly club rides</li>
                                <li>Cyclothon registration (20KM only)</li>
                                <li>Club t-shirt</li>
								<li></li>
								<li></li>
                                
                            </ul>-->
                            <!--<div class="cy_price_btn">
                                <a href="<?php echo base_url(); ?>index.php/register" class="cy_button">register now</a>
                            </div>-->
                        </div>
                    </div>
                </div>
				
                <div class="col-lg-4 col-md-12">
                    <div class="cy_price_box">
                        <div class="cy_price_head">
                            <h3>Preferred Club Membership</h3>
                            <!--<h1>&#8377;2000</h1>-->
                        </div>
                        <div class="cy_price_body">
						<div class="widget widget_categories">
                            <ul>
                                <li><a href="#">Access to coached skills clinics</a></li>
                                <li><a href="#">Access to training plans</a></li>
                                <li><a href="#">Access to weekly club rides</a></li>
                                <li><a href="#">Cyclothon registration (20KM only)</a></li>
								 <li><a href="#">Club t-shirt</a></li>
                            </ul>
						</div>
                            <!--<div class="cy_price_btn">
                                <a href="<?php echo base_url(); ?>index.php/register" class="cy_button">register now</a>
                            </div>-->
                        </div>
                    </div>
                </div>
                <span class="col-lg-2"></span>
            </div>
        </div>
    </div>
  
	
	
    <!--Footer section start-->
	