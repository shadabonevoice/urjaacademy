<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:12:38 GMT -->
<head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
	
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url();?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
	
	<style>
	.cy_price_wrapper {
   
    padding: 0px 0px;
}
	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
   <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
							<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>PHONE: 8308054000</li>
                                <li>
                                    <ul>
                                          <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container"> 
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Terms & Conditions</h1>
                </div>
            </div>
        </div>
    </div>
     <!--Menus Start-->
  <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url(); ?>index.php/index"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url();?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url();?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url();?>index.php/join">join</a></li>
								 <li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url();?>index.php/route">route</a></li>
								  <li><a href="<?php echo base_url();?>index.php/events">events</a></li>
								 
								
                                <li><a href="<?php echo base_url();?>index.php/sponsors">sponsors</a></li> 
                                <li><a href="<?php echo base_url();?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
    <!--About section start-->
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
			
                <div class="col-lg-12 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">IMPORTANT INSTRUCTIONS</h1>
                        

						<p>•	Carry water with you at all times and refill your bottle at the nearest hydration point.</p>
						<p>•	Throw your trash in the Garbage Bin placed at each hydration point. PLEASE DO NOT LITTER OUR BEAUTIFUL CITY.</p>
						<p>•	Smile at the photographers who will capture your pictures while cycling. Do not take any risks by trying stunts.</p>
						<p>•	Look out for signage/arrows and volunteers along your route.</p>
						<p>•	Recheck your fitness levels. If you do not feel well, opt out or just take it easy.</p>
						<p>•	If your cycle gets a flat, call the emergency contact number on your bib.</p>
						<p>•	Eat healthy, sleep well and hydrate before the event.</p>
						<p>•	There are no baggage counters, so keep your stuff with you or a friend.</p>
						<p>•	Wear your bib number on your chest for identification.</p>
						<p>•	After reaching the finishing point, stretch again and rehydrate.</p>


                       <!-- <a href="#" class="cy_button">read more</a>-->
                    </div>
                </div>
               
            </div>
        </div>
    </div>
	
	  <div class="cy_price_wrapper">
        <div class="container">
          
            <div class="row">
			 <div class="cy_about_data">
                <h1 class="cy_heading">TERMS & CONDITIONS</h1>

				<p style="color:#fff;">•	Please choose the event category carefully based on your fitness level.</p>
				<p style="color:#fff;">•	Participant, once registered under any category, cannot cancel his/her registration. There will be no refund of registration fees paid by the participant in case of his/her failure to participate in the event. This is non-negotiable.</p>
				<p style="color:#fff;">•	This is not a race but each distance category must be completed within the set time limit.</p>
				<p style="color:#fff;">•	The registration form must be completed with accurate and truthful information.</p>
				<p style="color:#fff;">•	The routes are subject to change based on anticipated/unanticipated circumstances and weather conditions.</p>
				<p style="color:#fff;">•	Helmet, front headlight, and rear lights/blinkers are compulsory for all cycles. Cyclists without helmets will not be allowed to participate.</p>
				<p style="color:#fff;">•	Cyclists assume all risks associated with participating in the Cyclothon including, but not limited to, falls, contact with other participants, the effects of the weather, including high heat or humidity, traffic and the condition of the road, arson or terrorist threats and all other risks associated with a public event. </p>
				<p style="color:#fff;">•	You understand that the Cyclothon takes place on roads where there is vehicular traffic and though our volunteers take care of managing the traffic, you are responsible for watching the traffic and cycling in accordance with all safety rules and standards.  </p>
				<p style="color:#fff;">•	You agree that the Ahmednagar Cycling Club and its volunteers will not be liable for any loss, damage, illness, or injury that might occur as a result of your participation in the event. </p>
				<p style="color:#fff;">•	You agree to abide by the rules and instructions provided by the organizers in the best interest of your health and safety. </p>
				<p style="color:#fff;">•	You agree to stop cycling if instructed by one of the event organizers. </p>

			</div>	
            </div>
        </div>
    </div>
  

	
    <!--Footer section start-->
	