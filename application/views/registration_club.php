



<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->
<!-- 
Template Name: Cycling- Html Template
Version: 1.0.0
Author: Kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:13:21 GMT -->
<head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url();?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
	
	<style>
	
	.cy_checkout_box ul#progressbar li:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0px;
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
}
	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
    <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
							<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>CONTACT: 8308054000</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Club Membership</h1>
                </div>
            </div>
        </div>
    </div>
    <!--Menus Start-->
  <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url();?>index.php/index"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url();?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url();?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url();?>index.php/join">join</a></li>
								<li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url();?>index.php/routes">route</a></li>
								  <li><a href="<?php echo base_url();?>index.php/events">events</a></li>
                                <li><a href="<?php echo base_url();?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url();?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!-- checkout section start -->
    <div class="cy_checkout_wrapper">
        <div class="container">
            <div class="row">
			<!--style="background-color: #93b3be;"-->
                <div class="col-lg-8 col-md-12 offset-lg-2" style="background-color: #93b3be;">
                    <div class="cy_checkout_box">
                      <ul id="progressbar">
                            <li class="active">Registration</li>
                        </ul>
						

        	
         <form method="post" id="product_info" name="product_info" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/Welcome_club/check">  
			 <div class="woocommerce-billing-fields step">	
               
               <div class="form-group">  
					Full Name (Only alphabets)			   
                  <input type="text"  name="customer_name" id="customer_name" class="form-control" placeholder="Full Name (Only alphabets)" onblur="return ValidateName('customer_name')"/>
               <span style="color:red;" id="errorurname"></span>
			   </div>
			
				
                <div class="form-group"> 
					Mobile Number				
                  <input type="text"  name="mobile_number" id="mobile_number" class="form-control" placeholder="Mobile Number(10 digits)"  onblur="return ValidateMobnumber('mobile_number')"/>
                <span  style="color:red;"  id="errormobile_number"></span>
				</div>
				
                <div class="form-group"> 
				Email
                  <input type="email"  name="customer_email" id="customer_email" class="form-control" placeholder="Email" onblur="return ValidateEmail('customer_email')"/>
                <span  style="color:red;"  id="errorcustomer_email"></span>
				</div>
				
                <div class="form-group">
				Address
                  <textarea class="form-control" name="customer_address" id="customer_address" placeholder="Address" onblur="return ValidateAddress('customer_address')"></textarea>
               <span  style="color:red;"  id="errorcustomer_address"></span>
			   </div>
				
				<div class="form-group">
				Enter Your State
                  <input type="text" class="form-control" name="state" id="state" placeholder="Enter Your State" onblur="return ValidateState('state')" />
                <span  style="color:red;"  id="errorstate"></span>
				</div>
				
				<div class="form-group">
				Enter Your City
                  <input type="text" class="form-control" name="city" id="city" placeholder="Enter Your City" onblur="return ValidateCity('city')" />
               <span  style="color:red;"  id="errorcity"></span>
			   </div>
				
				<div class="form-group">
				Enter Your Postcode
                  <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Enter Your Postcode" onblur="return ValidatePostcode('postcode')"/>
                <span  style="color:red;"  id="errorpostcode"></span>
			   </div>
				
				<div class="form-group">
				Enter Your Country
                  <input type="text" class="form-control" name="country" id="country" value="India" placeholder="Enter Your Country" />
                </div>
			
				 <div class="form-group checkbox" style="bottom: 20px;">
				   <label style="bottom: 20px;">Gender</label><br>
						 <input type="radio" name="gender" value="male" checked style="display:inline-block;"> Male
						  <input type="radio" name="gender" value="female" style="display:inline-block;"> Female
				</div>
				
				</br>
						<div class="form-group"> 
								
						</div>
						<div class="form-group date"> 
							Date of Birth
							<input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="Date of Birth(MM/DD/YYY)" onblur="return ValidateBirthdate('birthdate')" />
						  <span  style="color:red;"  id="errorbirthdate"></span>
						 </div>
						
					
					
						<div class="form-group">
						Enter Your Age
							<input type="text" placeholder="Enter Your Age" readonly name="age" id="age" class="form-control" >
						</div>
					
					
					
						
						
						
						<div class="form-group">
						Enter Your Blood Group
							<select class="form-control"  placeholder="Select Race Category" name="bloodgrp" id="bloodgrp" onblur="return ValidateBloodg('bloodgrp')">
								<option value="">Select Your Blood Group</option>
								<option  value="A+" >A+</option>
								<option  value="A-" >A-</option>
								<option  value="B+" >B+</option>
								<option  value="B+" >B-</option>
								<option  value="O+" >O+</option>
								<option  value="O-" >O-</option>
								<option  value="AB+" >AB+</option>
								<option  value="AB-" >AB-</option>
							</select>
							 <span  style="color:red;"  id="errorbloodgrp"></span>
						</div>
						
						<div class="form-group">
						Select T-Shirt Size
							<select  class="form-control" name="tshirt" id="tshirt" onblur="return ValidateTshirt('tshirt')">
								<option value="">Select T-Shirt Size</option>
								<option value="S" >S</option>
								<option value="M" >M</option>
								<option value="L" >L</option>
								<option value="XL" >XL</option>
								<option value="XXL" >XXL</option>
								<option value="XXXL" >XXXL</option>
							</select> 
							<span  style="color:red;"  id="errortshirt"></span>
						</div>
						
						<div class="form-group">
						Aadhar Card No.
							<input type="text" placeholder="Enter Your Aadhar No." name="productinfo" id="productinfo"  class="form-control" onblur="return ValidateAadhaar('productinfo')" />
						<span style="color:red;" id="erroraadhar"></span>
						</div>
				
				
						<div class="form-group">
						Total Amount
							<input type="text" placeholder="Total Amount." readonly name="payble_amount" id="payble_amount" value="100"  class="form-control">
						</div>
					   <div class="form-group">
							<input type="checkbox" name="condition" value="Accept">I accept the terms and conditions<br>
						</div>
					<center>
						<div class="form-group">
						  <button type="submit" class="btn btn-success">Submit</button>
						  <button class="btn btn-secondary" type="reset">Reset</button>
						</div>
					</center>
				</div>
            </form>  
			
			
			
			            
         </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->
	
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
		<script>
			$(document).ready(function(){
				$("#product_info").submit(function(){
					 
					return ValidateName('customer_name');
					
				});
			});
			
			
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateState('state');
					
				});
			});
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateCity('city');
					
				});
			});
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateMobnumber('mobile_number');
					
				});
			});
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateAddress('customer_address');
					
				});
			}); 
			
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidatePostcode('postcode');
					
				});
			}); 
			
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateBirthdate('birthdate');
					
				});
			});
			
			
			
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateAadhaar('productinfo');
					
				});
			});
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateBloodg('bloodgrp');
					
				});
			}); 
		
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateTshirt('tshirt');
					
				});
			});
			
			
			
			$(document).ready(function(){
				$("#product_info").submit(function(){
					return ValidateEmail('customer_email');  
					
				});
			});
			
			
			
			</script>
	<script type="text/javascript">
			function ValidateName(customer_name) {
				
			  var fld = document.getElementById(customer_name);
			   var charactersOnly = document.getElementById(customer_name).value;
			  
			  if (fld.value == "") {
			   document.getElementById("errorurname").textContent = "You didn't enter your name.";
			  fld.value = "";
			  fld.focus();
			  return false;
			 }
			  else if (charactersOnly.search(/^[a-zA-Z ]+$/) === -1) {
			  document.getElementById("errorurname").textContent = "The name contains Invalid characters.";
			  fld.value = "";
			  fld.focus();
			  return false;
			 }
			 else if ((fld.value.length > 35)) {
			 document.getElementById("errorurname").textContent = "The Name lenght is too long.";
			  mob.value = "";
			  mob.focus();
			  return false;
			 }
			 else{
			document.getElementById("errorurname").textContent = " ";	 
			 }

			}
	

	</script>
	<script type="text/javascript">

			function ValidateState(state) {
			  var mystate = document.getElementById(state);
			   var charactersOnly = document.getElementById(state).value;
			  
			  if (mystate.value == "") {
			   document.getElementById("errorstate").textContent = "You didn't enter State.";
			  mystate.value = "";
			 
			  return false;
			 }
			  else if (charactersOnly.search(/^[a-zA-Z ]+$/) === -1) {
			  document.getElementById("errorstate").textContent = "The name contains illegal characters.";
			  mystate.value = "";
			  mystate.focus();
			  return false;
			 }
			 else{
			document.getElementById("errorstate").textContent = " ";	 
			 }

			}

	</script>
	
	<script type="text/javascript">

			function ValidateCity(city) {
			  var mycity = document.getElementById(city);
			   var charactersOnly = document.getElementById(city).value;
			  
			  if (mycity.value == "") {
			   document.getElementById("errorcity").textContent = "You didn't enter City.";
			  mycity.value = "";
			 
			  return false;
			 }
			  else if (charactersOnly.search(/^[a-zA-Z ]+$/) === -1) {
			  document.getElementById("errorcity").textContent = "The name contains illegal characters.";
			  mycity.value = "";
			  mycity.focus();
			  return false;
			 }
			 else{
			document.getElementById("errorcity").textContent = " ";	 
			 }

			}

	</script>
	
	<!-- Mobile no -->
		<script type="text/javascript">

			function ValidateMobnumber(mobile_number) {
				
			   var mob = document.getElementById(mobile_number);
			   var charactersOnly = document.getElementById(mobile_number).value;
			  
			  if (mob.value == "") {
			   document.getElementById("errormobile_number").textContent = "You didn't enter your mobile number.";
			  mob.value = "";
			  
			  return false;
			 }
			  else if (isNaN(mob.value)) {
			  document.getElementById("errormobile_number").textContent = "The Mobile number contains Invalid characters.";
			  mob.value = "";
			  mob.focus();
			  return false;
			 }
			  else if ((mob.value.length < 10)) {
			 document.getElementById("errormobile_number").textContent = "The Wrong number";
			  mob.value = "";
			  mob.focus();
			  return false;
			 }
			 else{
			document.getElementById("errormobile_number").textContent = " ";	 
			 }


			}

		</script>
		
		<!-- Mobile no -->
		<script type="text/javascript">

			function ValidatePostcode(postcode) {
				
			   var pin = document.getElementById(postcode);
			   var charactersOnly = document.getElementById(postcode).value;
			  
			  if (pin.value == "") {
			   document.getElementById("errorpostcode").textContent = "You didn't enter your Pincode.";
			  pin.value = "";
			  
			  return false;
			 }
			  else if (isNaN(pin.value)) {
			  document.getElementById("errorpostcode").textContent = "Only Numbers";
			  pin.value = "";
			  pin.focus();
			  return false;
			 }
			  else if ((pin.value.length < 6) || (pin.value.length > 6)) {
			 document.getElementById("errorpostcode").textContent = "The Wrong Pincode";
			  pin.value = "";
			  pin.focus();
			  return false;
			 }
			 else{
			document.getElementById("errorpostcode").textContent = " ";	 
			 }


			}

		</script>

		<script>
		
			function ValidateBirthdate(birthdate) {
				
				 var mybirth = document.getElementById(birthdate);
				var	dateFirst = mybirth.value.split('-');
				var dobYear = dateFirst[0];
				var calage = todaysYear - dobYear;
				
			var todayDate = new Date();
			var todaysYear = todayDate.getFullYear();//Current Year
			var fifteenYrsBack = todaysYear - 15;//Current Year - 15
				
				 if(mybirth.value == ""){
		
			document.getElementById("errorbirthdate").textContent = "Select Your Birthdate";
			 mybirth.value = "";
			  mybirth.focus();
			  return false;
				 }
				 else if(dobYear > fifteenYrsBack){
		
			document.getElementById("errorbirthdate").textContent = "Check Birthdate";
			 mybirth.value = "";
			  mybirth.focus();
			  return false;
				 }
			
			else{
				document.getElementById("age").value = todaysYear - dobYear;
				document.getElementById("errorbirthdate").textContent = "";
			
			}
			}
			
			</script>
		
		
		
		
		<script type="text/javascript">
			function ValidateAddress(customer_address){
			   
			   var myaddr = document.getElementById(customer_address);
				
			  if (myaddr.value == "") {
			   document.getElementById("errorcustomer_address").textContent = "You didn't enter your Address";
			  myaddr.value = "";
			 
			  return false;
			 }
			 else if ((myaddr.value.length > 160)) {
			 document.getElementById("errorcustomer_address").textContent = "Address too long";
			  myaddr.value = "";
			  myaddr.focus();
			  return false;
			 }
			 else{
			document.getElementById("errorcustomer_address").textContent = " ";	 
			 }
				
			  

			}


		</script>
		
		<script type="text/javascript">
		
		
		
		
			function ValidateAadhaar(productinfo){
			 var aadId = document.getElementById(productinfo);
			   var charactersOnly = document.getElementById(productinfo).value;
			  
			  if (aadId.value == "") {
			   document.getElementById("erroraadhar").textContent = "You didn't enter your Aadhar ID ";
			  aadId.value = "";
			  
			  return false;
			 }
			  else if (isNaN(aadId.value)) {
			  document.getElementById("erroraadhar").textContent = "The Mobile number contains Invalid characters.";
			  aadId.value = "";
			  aadId.focus();
			  return false;
			 }
			  else if ((aadId.value.length < 12)) {
			 document.getElementById("erroraadhar").textContent = "The Wrong number";
			  aadId.value = "";
			  aadId.focus();
			  return false;
			 }
			 else{
			document.getElementById("erroraadhar").textContent = " ";	 
			 }

			}


		</script>
		
		<script type="text/javascript">
		
		
		
		
			function ValidateBloodg(bloodgrp){

			 var aadId = document.getElementById(bloodgrp);
			   var charactersOnly = document.getElementById(bloodgrp).value;
			  
			  if (aadId.value == "") {
			   document.getElementById("errorbloodgrp").textContent = "Select Blood Group";
			 aadId.value = "";
			  return false;
			 }
			 else 
			 {
			document.getElementById("errorbloodgrp").textContent = " ";	 
			 }

			}
			
			function ValidateTshirt(tshirt){

			 var tshirtId = document.getElementById(tshirt);
			   var charactersOnly = document.getElementById(tshirt).value;
			  
			  if (tshirtId.value == "") {
			   document.getElementById("errortshirt").textContent = "Select T-Shirt Size";
			 tshirtId.value = "";
			  return false;
			 }
			 else 
			 {
			document.getElementById("errortshirt").textContent = " ";	 
			 }

			}


		</script>
		
		
		<!-- Email Validation-->
		<script type="text/javascript">
		
		function ValidateEmail(customer_email)
			{
				var x = document.getElementById(customer_email).value;
				var atpos = x.indexOf("@");
				var dotpos = x.lastIndexOf(".");
			  
			  if (x == "") {
			   document.getElementById("errorcustomer_email").textContent = "You didn't enter your email";
			  x.value = "";
			  x.focus();
			  return false;
			 }
			  else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
			  document.getElementById("errorcustomer_email").textContent = "Email Invalid.";
		
			 }
			  
			 else{
			document.getElementById("errorcustomer_email").textContent = " ";	 
			 }
				
				
			}

			
		</script>
		





	
		
	
		
		
