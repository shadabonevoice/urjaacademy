<!DOCTYPE html>
<head>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width">
  	<base href="<?php echo $base_url = load_class('Config')->config['base_url'];?>"></base>
  	<title>IloveNGR 404 | Page Not Found</title>
  	<meta name="author" content="Ilovengr">
  	<meta name="description" content="404 error. Sorry, but this page does not exist on IloveNGR.">
  	<meta name="keywords" content="IloveNGR">
  	<link rel="shortcut icon" href="<?php echo $base_url;?>/images/start_page/favicon.png">
  	<link rel="stylesheet" href="css/404style.css">
</head>
<body>

	<div class="class404">
		<div class="contain">
		
			<h1>Not a valid link</h1>
			<span>This page does not exist on IloveNGR.</span>
			<a href="<?php echo $base_url;?>">Go to IloveNGR</a>
		</div>
	</div>
</body>
</html>
