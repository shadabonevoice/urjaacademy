<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include ("class.phpmailer.php");
include ("class.smtp.php");
class Welcomepromo extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
       $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('security');
        $this->load->library('upload');
        $this->load->library('email');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('common_model');
    }

	public function index()
	{
		redirect('cyclothonRegister');
		$data['header_title']=headTitle;
		$data['page_title']=" Promo Ride";
		$this->load->view('user_register/registration_promo',$data);	
		//$this->load->view('registration');	
		//$this->load->view('footer');	
		
        	
	}

    
    
    
	   
	public function check()
	{
		 $this->load->model('common_model');
	//	$amount =  $this->input->post('payble_amount');
	    $product_info = $this->input->post('product_info');
	    $customer_name = $this->input->post('customer_name');
	    $customer_emial = $this->input->post('customer_email');
	    $customer_mobile = $this->input->post('mobile_number');
	    $customer_address = $this->input->post('customer_address');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$postcode = $this->input->post('postcode');
		$country = $this->input->post('country');
		$gender = $this->input->post('gender');
		$birthdate = $this->input->post('birthdate');
		$age = $this->input->post('age');
		$bloodgrp = $this->input->post('bloodgrp');
		$tshirt = $this->input->post('tshirt');
		$uremrname = $this->input->post('uremrname');
		$uremrnumber = $this->input->post('uremrnumber');
		date_default_timezone_set('Asia/Kolkata');
	    $maxid = $this->db->query('SELECT MAX(registration_no) AS `maxid` FROM `tbl_register`')->row()->maxid;
		$myid11 = $maxid + 1;
		$myid = sprintf("%02d", $myid11);
		$mycat = $product_info;
		$mycat_slice = substr($mycat, 0, -2); // returns "abcde"
		$bibstring = $mycat_slice . $myid;
		// $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$insert_array = array('bibid' => $bibstring, 'name' => $customer_name, 'race_category' => $product_info, 'email_addr' => $customer_emial, 'mobile_no' => $customer_mobile, 'full_address' => $customer_address, 'state' => $state, 'city' => $city, 'postcode' => $postcode, 'country' => $country, 'gender' => $gender, 'birthdate' => $birthdate, 'age' => $age, 'bloodgrp' => $bloodgrp, 'emergency_name' => $uremrname, 'emergency_number' => $uremrnumber, "status" =>"success", 'create_at' => date('Y-m-d H:i:s'));
	      // $this->common_model->add_records('tbl_register_promo', $insert_array);
	       
	       $insertquery =  $this->common_model->add_records('tbl_register_promo', $insert_array);
	       if($insertquery){	       
	        $success_msg= "Dear ".$customer_name.",
				You have successfully registered for Republic Day Ride.
				Address: IMA Bhavan, Near Railway Over Bridge, Kalyan Road, Ahmednagar. Time: 7.00 AM. 
				Breakfast arranged for all riders.
				For updates, follow us on - https://bit.ly/2ApDHNp or call - 8308054000";
				$this->common_model->sendSMS($customer_mobile, $success_msg);  

				// $this->sendRegistrationMail($customer_emial ,$product_info, $customer_name);
				$this->config->load('fetchemail', TRUE);
				$adminemail = $this->config->item('admin_email', 'fetchemail'); 
				$mail = new PHPMailer ();
				$mail->IsSMTP (); // set mailer to use SMTP
				$mail->Host = smtpHost; // specify main and backup server
				$mail->Port = smtpPortNo; // set the port to use
				$mail->SMTPAuth = true; // turn on SMTP authentication
				$mail->Username = smtpUserName; // your SMTP username or your gmail username
				$mail->Password = smtpPassword; // your SMTP password or your gmail password

				$from = "ahmednagarcyclothon@nagarcycling.com"; // Reply to this email
				$to= $customer_emial; // Recipients email ID
				$to2= mailCopy; // Recipients email ID
				$name = ""; // Recipient's name
				$mail->From = $from;
				$mail->FromName = smtpFromName; // Name to indicate where the email came from when the recepient received
				$mail->AddAddress ( $to, $name );
				$mail->AddAddress ( $to2, $name );
				$mail->AddReplyTo ( $from, smtpFromName );
				$mail->IsHTML ( true ); // send as HTML
				$mail->Subject = "Republic Day Ride";
				$data['mailname']=$customer_name;
				$data['mycat']=$product_info;
				$htmlMessage = $this->load->view('user_register/registrationpromo_email.php', $data, TRUE);
				$mail->Body = $htmlMessage ; // HTML Body
				$mail->AltBody = strip_tags (""); // Text Body
				$mail->Sender = contactAdminEmail;
				$mail->Send();
				
				 
			}


	 //  exit();     
		redirect('http://www.nagarcycling.com/thankyou');
     
	}

	public function help()
	{
		
		$this->load->view('help');
		$this->load->view('footer');
	}
}
