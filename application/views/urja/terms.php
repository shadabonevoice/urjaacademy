<?php $this->load->view('common/header.php'); ?>   
    <!--About section start-->
	<div class="" style="background-color: #a28f11;">
        <div class="container">          
            <div class="row">
				<div class="cy_about_data">
                    <img src="<?php echo base_url(); ?>assets/images/logo_natak.png" width="200px" alt="about" class="img-fluid" />                    
                </div>
				<div class="cy_about_data">
					
					<!--<p style="font-weight: bold;">सूचना</p>-->
					<p style="font-weight: bold;">काही महत्वाच्या तारखा</p>
					<p style="font-size: 18px;">प्रवेश अर्ज उपलब्धता दिनांक:  <b> ६ एप्रिल, २०१९</b></p>
					<p style="font-size: 18px;">प्रवेश अर्ज स्वीकारण्याची अंतिम दिनांक : <b> ३० एप्रिल २०१९</b></p>
					<p style="font-size: 18px;">लेखी आणि तोंडी परीक्षा दिनांक : <b> संडे ५  मे २०१९</b></p>
					<p style="font-size: 18px;">निकाल:  <b> १० मे २०१९</b></p>
					<p style="font-size: 18px;">प्रत्यक्ष नाटक शाळेस सुरुवात:  <b> जून २०१९</b></p>
					<p style="font-size: 18px;">परीक्षेचे ठिकाण :  <b> अहमदनगर ३१ मे २०१९</b></p><br>
					
					<p style="font-weight: bold;">अंतिम प्रवेश प्रक्रिया </p>
					<!--<p style="font-weight: bold;">नियम व अटी </p>-->
					<p style="font-size: 18px;">▪ लेखी आणि तोंडी परीक्षेतून उत्तीर्ण झाल्यावरच उमेदवार प्रवेशास पात्र होईल.</p>
					<p style="font-size: 18px;">▪ पात्र उमेदवारानी ४५ दिवसांच्या आत संपूर्ण प्रवेश शुल्क भरल्यानंतरच त्याचा प्रवेश निश्चित केला जाईल.</p>
					<p style="font-size: 18px;">▪ कुठल्याही परिस्थितीत प्रवेश शुल्क परत केले जाणार नाही.</p>
					<p style="font-size: 18px;">▪ प्रवेशासंबंधी सर्व अधिकार व्यवस्थापनाकडे राखीव ठेवलेले आहेत.</p>
					<p style="font-size: 18px;">▪ जर उमेदवाराचे वय ३१ मे २०१९ तारखेपर्यंत १६ वर्ष पूर्ण नसेल. तर तो प्रवेशास अपात्र असेल.</p>
					<p style="font-size: 18px;">▪ नियमांच्या अर्थाबाबत (Interpretation) व्यवस्थापनाचा निर्णय अंतिम राहील.</p>
					<p style="font-size: 18px;">▪ प्रवेश शुल्क : ३०,००० रुपये</p>
					<hr>
					<p style="font-weight: bold;">Notice</p>
					<p style="font-weight: bold;">Some Important Dates</p>
					<p style="font-size: 18px;">Date of admission :  <b> 6th April 2019</b></p>
					<p style="font-size: 18px;">Last date of admission : <b> 30th April 2019</b></p>
					<p style="font-size: 18px;">Written and oral exam date : <b> Sunday 5th May 2019</b></p>
					<p style="font-size: 18px;">Results :  <b> 10th May 2019</b></p>
					<p style="font-size: 18px;">Date of commencement :  <b> Until 31st May 2019</b></p><br>
					
					<p style="font-weight: bold;">Last admission procedure </p>
					<p style="font-weight: bold;">Terms & Condition </p>
					<p style="font-size: 18px;">▪ Candidates will be eligible for admission only after they pass the written and oral examination.</p>
					<p style="font-size: 18px;">▪ Once the candidate is eligible for admission, He/she needs to pay the entire fees within 45 days. </p>
					<p style="font-size: 18px;">▪ Fees is not refundable </p>
					<p style="font-size: 18px;">▪ All rights for entries are reserved for management </p>
					<p style="font-size: 18px;">▪ If the candidate does not have completed 16 years till May 31, 2019. Then he will be not eligible for admission.</p>
					<p style="font-size: 18px;">▪ All the decisions & Interpretation are reserved with the management.</p>
					<p style="font-size: 18px;">▪ Academic Fee : Rs.30,000</p>
				</div>	
				<div class="cy_about_data">
					<button type="button" class="btn btn-lg btn-lg btn-warning"><a style="color:white;" href="<?php echo site_url('index.php/urja/register');?>">नोंदणी करा / Register</a></button>
				</div>
				<div class="cy_about_data">
					<p style="font-size: 18px; float: right;">Initiative of Urja Academy</p>				
				</div>
            </div>
        </div>
    </div>
		
    <!--Footer section start-->
<?php $this->load->view('common/footer'); ?>
	