<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->helper('security');
		$this->load->model('common_model');
	}  
	 
	public function index()
	{
		$data['header_title']=headTitle;
		$data['page_title']="Routes";
		$this->load->view('routes/routes',$data);
		//$this->load->view('footer');
	}
}
