
<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->

<html lang="en">
<!--<![endif]-->


<head>
   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url();?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
	
	<style>
	
	.cy_checkout_box ul#progressbar li:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0px;
    border-left: 20px solid transparent;
    border-right: 20px solid transparent;
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;
}
	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
    <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
								<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>CONTACT: 8308054000</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Rent a Bicycle</h1>
                </div>
            </div>
        </div>
    </div>
    <!--Menus Start-->
  <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url();?>index.php/index"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url();?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url();?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url();?>index.php/join">join</a></li>
								 <li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 
								 
								 
								 <li><a href="<?php echo base_url();?>index.php/routes">route</a></li>
								  <li><a href="<?php echo base_url();?>index.php/events">events</a></li>
                                <li><a href="<?php echo base_url();?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url();?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!-- checkout section start -->
    <div class="cy_checkout_wrapper">
        <div class="container">
            <div class="row">
			
			
             <div class="col-lg-8 col-md-12 offset-lg-2" >
                    <div class="cy_checkout_box">
					
						<?php 
							$maxidd	= $this->db->query('SELECT MAX(rent_id) AS `maxidd` FROM `tbl_rentbicycle`')->row()->maxidd;
							if(empty($maxidd)){
								$bid11 = 125;
							}else{
								$bid = $this->db->select('bicycle_count as bid')->from('tbl_rentbicycle')->where('rent_id',$maxidd)->get(); 
								$bid1 = $bid->row()->bid;
								$bid11 = $bid1 - 1;
							}
							
							if($bid11 == 0){
						?>
								<img src="<?php echo base_url();?>assets/images/thanku.png" alt="Thank U" style="width:100%; height:auto;">
								
						<?php }else{?>	
						
						<ul id="progressbar">
							<li class="active">Register to Rent a Bicycle</li>
					   
						</ul>
						
						<h4 class="cy_heading" style="color:#2f3942;text-align:center;">Only <?php echo $bid11; ?> bicycles left for Rent</h4>	
							
						<br><br>

				
						<form method="post" id="product_info" name="product_info" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/Welcomerentbicycle/check">  
						 <div class="woocommerce-billing-fields step">	
						   
						   <div class="form-group">  
								Full Name (Only alphabets)			   
							  <input type="text"  name="customer_name" id="customer_name" class="form-control" placeholder="Full Name (Only alphabets)" required />
							</div>
							<span id="errorurname"></span>
							
							<div class="form-group"> 
								Mobile Number				
							  <input type="number"  name="mobile_number" id="mobile_number" maxlength="10" class="form-control" placeholder="Mobile Number(10 digits)" required />
							</div>
							
							<div class="form-group"> 
							Email
							  <input type="email"  name="customer_email" id="customer_email" class="form-control" placeholder="Email" required/>
							</div>
							
							<div class="form-group">
							Address
							  <textarea class="form-control" name="customer_address" id="customer_address" placeholder="Address" required ></textarea>
							</div>
							
						
						
							<div class="form-group checkbox" style="bottom: 20px;">
							   <label style="bottom: 20px;">Gender</label><br>
									 <input type="radio" name="product_info" value="male" checked style="display:inline-block;"> Male
									  <input type="radio" name="product_info" value="female" style="display:inline-block;"> Female
							</div>
							
							</br>
								<div class="form-group"> 
									
								</div>
						
									<div class="form-group">
									Enter Your Age
										<input type="number" placeholder="Enter Your Age" maxlength="2" name="age" id="age" class="form-control" required>
									</div>
							
							
									<div class="form-group">
									Total Amount(INR)
										<input type="text" value="350" readonly name="payble_amount" id="payble_amount"  class="form-control">
									</div>
								   
								   <p>
								  <b style="color:red;"> Note* </b> - * You will have to pay INR. 1,000/- as Security Deposit for the bicycle at the EXPO. Which will be refunded on handing over the bicycle after the race.
								   </p>
									<p>
									* Please carry valid Government issued ID proofs and Address proofs.
									</p>
								</br>
							
								<center>
									<div class="form-group">
									  <button type="submit" class="btn btn-success">Submit</button>
									  <button class="btn btn-secondary" type="reset">Reset</button>
									</div>
								</center>
							</div>
						</form> 
			<?php }?>
			            
         </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->

		
		
		
