<?php
$this->load->view('common/header.php');
?>
   
   
    <div class="cy_about_wrapper cy_about_page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">who we are</h1>
                        <p>
						Ahmednagar Cyclothon is officially affiliated with <b>Cycling Federation of India(CFI)</b>,  <b>Kalpattaru Enterprises India Limited</b> and
						<b>Cycling Association of Maharashtra(CAOM)</b>.
					<!--	Ahmednagar Cycling Club is a friendly cycling club based in Ahmednagar, Maharashtra. It is dedicated to encouraging
						enjoyment of all types of cycling within the Ahmednagar Community. The Club was framed to promote the reason for 
						keeping our city green, while keeping our bodies healthy and clean. We invite individuals
						from the all the communities to our club occasions, for example, 
						gather rides, Cyclothons, etc. and group benefit exercises.-->
						The Ahmednagr Cycling Club is dedicated for organising a cyclothon every year, encouraging people from all walks of life to promote a healthier lifestyle within the community.
						Ahmednagar Cyclothon had witnessed a positive participation with 400+ riders in its 1st season.
					
						</p>
                      
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_about_img">
                        <img src="<?php echo base_url(); ?>assets/images/about/about1.jpg" alt="about" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="cy_sponsors_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                   <div class="cy_ride_text">
                        <h3>Next Ride Starts On</h3>
                        <h2>February 17, 2019</h2>
						<center>
						<table style="color:#fff;float: left;">
						<tr>
							<td>
								<h6 style="color:#fff;">Category:</h6>
							</td>
							<td></td>
							<td></td>
							<td>
								100KM
							</td>
							<td></td>
							<td></td>
							<td>
								50KM
							</td>
							<td></td>
							<td></td>
							<td>
								20KM
							</td>
						</tr>
						
						<tr>
						<td>
							<h6 style="color:#fff;">Reporting Time:</h6>
							</td>
							<td></td>
							<td></td>
							<td>
								5:30AM
							</td>
							<td></td>
							<td></td>
							<td>
								5:45AM
							</td>
							<td></td>
							<td></td>
							<td>
								6:00AM
							</td>
						
						</tr>
						
						<tr>
						
						<td>
							<h6 style="color:#fff;">Flag-Off Time:</h6>
							</td>
							<td></td>
							<td></td>
							<td>
								6:00AM
							</td>
							<td></td>
							<td></td>
							<td>
								6:15AM
							</td>
							<td></td>
							<td></td>
							<td>
								6:30AM
							</td>
						</tr>
						
						</table>
                        </center>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_sponsor_slider cy_section_padding">
                        <h1 class="cy_heading">our sponsors</h1>
                        <div class="owl-carousel owl-theme">
                              <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/patanjali02.png" alt="sponsor" />
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/lakme_salon02.png" alt="sponsor" />
                            </div>
							<div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/golds_gym03.png" alt="sponsor" />
                            </div>
                           <!-- <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/radi_partner04.png" alt="sponsor" />
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/energy_drink05.png" alt="sponsor" />
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/oxycool06.png" alt="sponsor" />
                            </div>-->
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/ilovengr08.png" alt="sponsor" />
                            </div>
                         <!--   <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/IMA07.png" alt="sponsor" />
                            </div>-->
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/nagar_rising08.png" alt="sponsor" />
                            </div>
							
							 <div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/sole_sisters08.png" alt="sponsor" />
                            </div>
							<!--<div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/bhagirathi_09.png" alt="sponsor" />
                            </div>-->
							<div class="item">
                                <img src="<?php echo base_url(); ?>assets/images/sponsors/giant_starkenn01.png" alt="sponsor" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
   <!--Team section start-->
    <div class="cy_team_wrapper padder_top100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">meet our team</h1>
                </div>
            </div>
            <div class="row">
				<div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/sunil_menon.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Sunil Menon</a></h2>
								<h4 style="color:#fff;">Race Director</h4>
                               
                            </div>
                        </div>
                    </div>
                </div>
			
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/gaurav_firodia.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Gaurav Firodia</a></h2>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/kalyani_firodia.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Kalyani Firodia</a></h2>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/mahesh_mulay.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Mahesh Mulay</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
			
			<!-- 2nd row-->
			 <div class="row">
			 
			   <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/ravi_patre.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Ravi Patre</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/abhijeet_pathak.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Abhijeet Pathak</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/dinesh_sanklecha.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Dinesh Sanklecha</a></h2>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/jyoti_pisore.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Jyoti Pisore</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
			
		<div class="row">
		
		  <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/ravi_pisore.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Ravi Pisore</a></h2>
                             
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/renuka_pathak.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Renuka 	</a></h2>
                             
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <div class="cy_team_box">
                        <img src="<?php echo base_url(); ?>assets/images/team/tushar_patwa.jpg" alt="team" class="img-fluid" />
                        <div class="cy_team_overlay">
                            <div class="cy_team_text">
                                <h2><a href="#">Tushar Patwa</a></h2>
                               
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        
			
        </div>
    </div>
    <!--Club History section css start-->
    <!--<div class="cy_club_wrapper cy_section_padding padder_bottom124">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">our club history</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="cy_club_history">
                        <ul class="cy_timeline">
                            <li>
                                <div class="cy_tl-item dir-r">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content">
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img.jpg" alt="history" class="img-fluid" />
                                        </div>
                                        <div class="cy_club_data">
                                            <span> 4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="cy_tl-item">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content text-right">
                                        <div class="cy_club_data">
                                            <span>4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img1.jpg" alt="history" class="img-fluid" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="cy_tl-item dir-r">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content">
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img2.jpg" alt="history" class="img-fluid" />
                                        </div>
                                        <div class="cy_club_data">
                                            <span>4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="cy_tl-item">
                                    <div class="cy_tl-icon"></div>
                                    <div class="cy_tl-content text-right">
                                        <div class="cy_club_data">
                                            <span>4th March, 2018</span>
                                            <h3><a href="#">Top Riders Of The Race</a></h3>
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
                                        </div>
                                        <div class="cy_club_img">
                                            <img src="<?php echo base_url(); ?>assets/images/about/club_img3.jpg" alt="history" class="img-fluid" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
 	<!--Footer section start-->
   <?php
$this->load->view('common/footer');
        ?>