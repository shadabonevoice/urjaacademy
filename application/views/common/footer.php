

<!--Footer section start-->
    
    <!--Bottom footer start-->
    <div class="cy_btm_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <p>
						Copyright &copy;2019  <b style="color:#e6c33e;">Urja Academy</b>. All Rights Reserved. Developed By<a style="color:#e6c33e;" href="<?php echo oneVoiceTransmediaURL;?>" target="_blank"> OneVoice Transmedia Pvt. Ltd.</a>
						<!--<span class="float-right" id="google_translate_element"></span>-->
					</p>
                </div>
            </div>
        </div>
    </div>
	<script type="text/javascript">
	function googleTranslateElementInit() {
	  new google.translate.TranslateElement({pageLanguage: 'mr'}, 'google_translate_element');
	}
	</script>

	<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <!--scripts start-->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/tether.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.countTo.js"></script>
    <script src="assets/js/appear.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/plugin/owl/owl.carousel.min.js"></script>
    <script src="assets/js/plugin/magnific/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/jquery.nice-select.min.js"></script>
    <script src="assets/js/custom.js"></script>
 <script src="assets/js/plugin/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="assets/js/plugin/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="assets/js/plugin/revolution/js/revolution.extension.navigation.min.js"></script>
    <script src="assets/js/plugin/revolution/js/revolution.extension.slideanims.min.js"></script>
    <script src="assets/js/plugin/revolution/js/revolution.extension.layeranimation.min.js"></script>
    <script src="assets/js/plugin/revolution/js/revolution.extension.parallax.min.js"></script>
    <script src="assets/js/plugin/revolution/js/revolution.extension.kenburn.min.js"></script>
	 <script src="assets/js/common.js"></script>
</body>


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:12:58 GMT -->
</html>