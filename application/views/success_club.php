<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->
<!-- 
Template Name: Cycling- Html Template
Version: 1.0.0
Author: Kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:13:21 GMT -->
<head>
   <!--<meta http-equiv="refresh" content="4;url= <?php echo base_url(); ?>index.php/index">-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url();?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom-animation.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
    <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
								<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>PHONE: 8308054000</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Registration</h1>
                </div>
            </div>
        </div>
    </div>
    <!--Menus Start-->
  <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url();?>index.php/index"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url();?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url();?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url();?>index.php/join">join</a></li>
								 <li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url();?>index.php/routes">route</a></li>
								  <li><a href="<?php echo base_url();?>index.php/events">events</a></li>
                                <li><a href="<?php echo base_url();?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url();?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!-- checkout section start -->
    <div class="cy_checkout_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 offset-lg-2">
                    <div class="cy_checkout_box">
                        <ul id="progressbar">
                            <li class="active">Transaction Success</li>
                         <!--   <li>Payment</li>
                            <li>Receipt</li> -->   
                        </ul>

					 <div class="col-lg-8 col-md-12 offset-lg-2">
							<div class="row">
							
							<div class="cy_about_data" style="padding-top: 10px">
								<p style="text-align:center;">
								
									<?php 
										echo "<p style='text-align:center;'>Thank You! Your transaction is successful.</br>";
										echo "Your Transaction ID for this transaction is <b>".$txnid."</b></br>";
										echo "We have received a payment of <b> Rs. " . $amount . ".</b></p>";
									?>
								</p>
							</div>
								
									
									
								 
							</div>
						</div>	

						 </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->
	<div class="cy_footer_wrapper cy_section_padding padder_bottom75">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget cy_footer_about">
                        <img src="<?php echo base_url();?>assets/images/footer/footer_logo.png" alt="logo" class="img-fluid"/>
                        <p style="text-align:justify;">
						Ahmednagar Cycling Club actively encourages cyclists of all abilities to meet their personal goals. Whether you cycle for fun, for the health benefits or for the competition, Ahmednagar Cycling Club welcomes you.

						</p>
                    </div>  
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="widget widget_categories">
					<h1 class="widget-title">Important Links</h1>
						<ul style="list-style-type: none;">
							<li style="padding: 8px 0px;"><a href="<?php echo base_url();?>index.php/join">JOIN</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url();?>index.php/rides">RIDES</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url();?>index.php/routes">ROUTE</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url();?>index.php/welcome">REGISTRATION</a></li>
							<li style="padding: 8px 0px;"><a href="<?php echo base_url();?>index.php/terms">TERMS & CONDITIONS</a></li>
						</ul>
					</div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h1 class="widget-title">Venue Details</h1>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url();?>assets/images/svg/map-mark.svg" alt="map-mark"></span>
                        <div class="cy_post_info">
                        <p>IMA Bhavan- Near Railway over bridge, Kalyan Road - Ahmednagar</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url();?>assets/images/svg/phone.svg" alt="phone"></span>
                        <div class="cy_post_info">
                        <p>Contact :- 8308054000</p>
                        </div>
                    </div>
                    <div class="cy_foo_contact">
                        <span><img src="<?php echo base_url();?>assets/images/svg/email.svg" alt="email"></span>
                        <div class="cy_post_info">
                        <p><a href="#">support@nagarcycling.com</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                   <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAhmednagar-Cycling-Club-1900441513602454%2F&tabs=timeline&width=300&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1330903350339636" width="300" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>  
            </div>
        </div>
    </div>
    <!--Bottom footer start-->
    <div class="cy_btm_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <P>Copyright &copy;2018  <b style="color:#ea64b2;">AhmednagarCyclingClub</b>. All Rights Reserved. Developed By<a style="color:#e6c33e;" href="www.onevoicetransmedia.com" target="_blank"> OneVoice Transmedia Pvt. Ltd.</a></P>
                </div>
            </div>
        </div>
    </div>
	<!--Go to top start-->
	<div class="cy_go_to">
		<div class="cy_go_top">
			<img src="<?php echo base_url();?>assets/images/go_to_top.png" alt="back to top">
		</div>	
	</div>
    <!--scripts start-->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/js/tether.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.countTo.js"></script>
    <script src="<?php echo base_url();?>assets/js/appear.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugin/owl/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugin/magnific/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>

</body>
<script>
   history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
</script>	



<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:12:58 GMT -->
</html>


