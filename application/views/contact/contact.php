<?php
$this->load->view('common/header.php');
?>
    <!--Menus Start-->

   
    <!-- search section -->
    <div class="cy_search_form">
        <button class="search_close"><i class="fa fa-times"></i></button>
        <div class="cy_search_input">
        <input type="search" placeholder="search">
        <i class="fa fa-search"></i>
        </div>
    </div>
    <!--Contact section start-->
    <div class="cy_contact_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Get In touch</h1>
                </div>
            </div>
            <div class="row padder_top50">
                <div class="col-lg-4 col-md-12">
                    <div class="cy_con_box">
                        <img src="assets/images/contact/con_img1.jpg" alt="phone" class="img-fluid" />
                        <div class="cy_con_overlay">
                            <div class="cy_con_data">
                                <h3>phone</h3>
                               <p>Contact :- <?php echo nagarCyclingContact;?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_con_box">
                        <img src="assets/images/contact/con_img2.jpg" alt="email" class="img-fluid" />
                        <div class="cy_con_overlay">
                            <div class="cy_con_data">
                                <h3>Email</h3>
                                <p><?php echo nagarCyclingEmail;?></p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_con_box">
                        <img src="assets/images/contact/con_img3.jpg" alt="address" class="img-fluid" />
                        <div class="cy_con_overlay">
                            <div class="cy_con_data">
                                <h3>Venue</h3>
								
                                <p><?php echo nagarCyclingAddress1;?> </p>
								<p><?php echo nagarCyclingAddress2;?></p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Map section start-->
    <div class="cy_form_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h1 class="cy_heading">How can we help you ?</h1>
                    <div class="cy_form padder_top50">
					
					<center>
						
						<h5 style="color:green;" id="successbuy5"></h5>
						
					</center>
                    <br>
                        <form enctype="multipart/form-data" name="contact_submit" id="contact_submit" method="post">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 padder_left">
                                    <div class="form-group">
                                        <input type="text" name="first_name" id="first_name" maxlength="40" class="form-control require" placeholder="Name" maxlength="60">
                                       
                                        <span class="contact_form_error first_name"></span>
                                    </div>

                                </div>
                               <!-- <div class="col-lg-6 col-md-6 padder_right">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control require" placeholder="Email" data-valid="email" data-error="Email should be valid." required>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 padder_left padder_right">
                                    <div class="form-group">
                                        <input type="text" name="subject" class="form-control" placeholder="Subject" required>
                                    </div>
                                </div>-->
                                <div class="col-lg-12 col-md-12 padder_left padder_right">
                                    <div class="form-group">
                                        <textarea name="message" id="message" maxlength="300" class="form-control" placeholder="Message"></textarea>

                                        <span class="contact_form_error message_name"></span>
                                    </div>
                                </div>
                                <div class="response"></div>
                                <div class="col-lg-12 col-md-12 padder_left padder_right">
                                    <input type="submit" name="contact_submit_data" id="contact_submit_data" class="cy_button submitForm" value="Submit">
                                   <!-- <button type="button" name="contact_submit" id="contact_submit" class="cy_button submitForm">Submit</button>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.3229330922723!2d74.75725211446817!3d19.09348388707785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bdcb01572612b15%3A0x6471b31788420748!2sAhmadnagar+Club+Ltd.!5e0!3m2!1sen!2sin!4v1518087860708" width="auto" height="450" frameborder="0" style="border:0" allowfullscreen ></iframe>
                    </div>
                    <div class="cy_map" style="display: none;">
                        <img width="250" height="350"  style="border:0" src="assets/images/coming_soon2.png"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer section start-->


<script src="assets/js/jquery.js"></script>
<script src="assets/js/contact/contact.js"></script>

   
    <?php
$this->load->view('common/footer');
        ?>
	
	
