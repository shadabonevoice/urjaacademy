<!DOCTYPE html>
<html lang="en">
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<head>
  <?php 
if($page_title == "Success"){
    ?>
   <meta http-equiv="refresh" content="3;url= <?php echo base_url(); ?>">
   <?php
}
?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo base_url();?>">
    <title><?php echo 'Urja Academy';?></title>
    <meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
    <meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links
    <link rel="icon" type="image/icon" href="assets/favicon.png">-->
    <!-- main css -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/custom-animation.css">
    <link rel="stylesheet" href="assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="assets/js/plugin/magnific/magnific-popup.css">
      <link href="assets/js/plugin/revolution/css/settings.css" rel="stylesheet" type="text/css">
    <link href="assets/js/plugin/revolution/css/layers.css" rel="stylesheet" type="text/css">
    <link href="assets/js/plugin/revolution/css/navigation.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/custom-animation.css">
    <link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="assets/css/error.css">
    <link rel="stylesheet" href="assets/css/inline.css">
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
 