<!DOCTYPE html>
<!--
**********************************************************************************************************
    Copyright (c) 
********************************************************************************************************** -->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mirrored from kamleshyadav.in/html/cycling/version-1/event_single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Feb 2018 11:13:41 GMT -->
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	
    <title>Ahmednagar Cyclothon - Get a bike, get a life.</title>
	<meta name="Description" content="The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of cycling in the Ahmednagar community.">
	<meta name="Keywords" content="Ahmednagar Cyclothon, Ahmednagar Cycling. Ahmednagar Cycling Club, Ahmednagar Race">
    <meta name="author" content="OneVoice Transmedia Pvt. Ltd.">
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="<?php echo base_url();?>assets/favicon.png">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/owl/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/plugin/magnific/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
	
	<style>
	@media (max-width: 767px){
.cy_gal_img {
    float: none;
    width: 400px;
    margin: 0px auto;
}
	}
	</style>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1330903350339636&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!--Top bar start-->
    <div class="cy_top_wrapper">
        <div class="cy_top_info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="cy_top_detail">
                            <ul>
							<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>RENT BICYCLE</b></a></li>
								<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>REGISTER</b></a></li>
                                <li><a href="#">EMAIL: support@nagarcycling.com</a></li>
                                <li>PHONE: 8308054000</li>
                                <li>
                                    <ul>
                                         <li><a href="https://www.facebook.com/Ahmednagar-Cycling-Club-1900441513602454/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--Banner section start-->
    <div class="cy_bread_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Routes</h1>
                </div>
            </div>
        </div>
    </div>
   <div class="cy_menu_wrapper">
        <div class="cy_logo_box">
            <a href="<?php echo base_url();?>assets/index"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-fluid"/></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <button class="cy_menu_btn">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
                    <div class="cy_menu">
                        <nav>
                            <ul>
                                <li><a href="<?php echo base_url();?>index.php/index">home</a></li>
                                <li><a href="<?php echo base_url();?>index.php/about">about</a></li>
								 <li><a href="<?php echo base_url();?>index.php/join">join</a></li>
								<li class="dropdown"><a href="<?php echo base_url();?>index.php/welcome">Register</a>
                                    <ul class="sub-menu">
										<li> <a href="<?php echo base_url(); ?>index.php/welcome"><b>Register</b></a></li>
										<li><a href="<?php echo base_url(); ?>index.php/welcomerentbicycle"><b>Rent Bicycle</b></a></li>
										<li> <a href="<?php echo base_url(); ?>index.php/rides"><b>Rides</b></a></li>
										
                                    </ul>
                                </li>
								 <li><a href="<?php echo base_url();?>index.php/routes" class="active">route</a></li>
								  <li><a href="<?php echo base_url();?>index.php/events">events</a></li>
								 
								
                                <li><a href="<?php echo base_url();?>index.php/sponsors">sponsors</a></li>
                                <li><a href="<?php echo base_url();?>index.php/contact">contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
   <!--Events Section Start-->
    <div class="cy_event_wrapper cy_event_single">
        <div class="container">
            <div class="row">
			
			
			
			
			 <div class="col-lg-12 col-md-12">
                    <div class="cy_event_box">
                       <!-- <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>-->
                    <!--    <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map Coming Soon....</a></h2>
                           
                            
                        </div>
						
					</div></div>-->
			
			
			
			
                <!--<div class="col-lg-6 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map for 20KM</a></h2>
                           
                            
                        </div>
						
					</div></div>
						
						 <div class="col-lg-6 col-md-12">
                    <div class="cy_event_box">
                  
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map for 50KM</a></h2>
                           
                            
                        </div>
						
						</div></div>
                    
                	
						 <div class="col-lg-6 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map for 100KM</a></h2>
                           
                            
                        </div>
                    </div>
					</div>-->
					
		   <div class="cy_gallery_wrapper cy_section_padding padder_bottom100">
        <div class="container">			
					<center><img src="<?php echo base_url();?>assets/routes/coming_soon.jpg" alt="Thank U" style="width:50%; height:auto;"></center>
			<!--<div class="row">
            <div class="col-lg-12 col-md-12">
			
				
				<div class="row">
					<div class="col-lg-6 col-md-12">
					<center><h4 class="cy_heading">Route Map For 20KM</h4></center>
						<div class="row">
						
							<div class="col-lg-12 col-md-12">
								<div class="cy_gal_img">
									<img src="<?php echo base_url();?>assets/routes/10km_map.jpg" alt="Route" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="<?php echo base_url();?>assets/routes/10km_map.jpg" class="fa fa-search"></a>
										
									</div>
								</div>
								
								<div class="cy_event_data">
                            <p style="text-align:center;">
							Start at IMA Bhavan - Take a U turn from the Turn around sign after crossing Jakhangaon
							</p>
                           
                            
                        </div>
								
							</div>
						
						</div>
						
					</div>	
					
					
				<div class="col-lg-6 col-md-12">
				<center><h4 class="cy_heading">Route Map For 50KM</h4></center>
				<center>
                 <div class="row">
						
							<div class="col-lg-12 col-md-12">
								<div class="cy_gal_img">
									<img src="<?php echo base_url();?>assets/routes/50km_map.jpg" alt="Route" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="<?php echo base_url();?>assets/routes/50km_map.jpg" class="fa fa-search"></a>
										
									</div>
								</div>
								<div class="cy_event_data">
								<p style="text-align:center;">
							Start at IMA Bhavan - Take a U turn from the Turn around sign at the Bhairawnath Devasthaan Kamaan (nearly 10 KM before Takli Dhokeshwar)
							Finish at the start point

							</p>
							</div>	
							</div>
						
						</div>
					</center>
                </div>
				</div>
				
				
				<div class="row">
				<span class="col-lg-3"></span>
					<div class="col-lg-6 col-md-12">
					<center><h4 class="cy_heading">Route Map For 100KM</h4></center>
						<div class="row">
						
							<div class="col-lg-12 col-md-12">
								<div class="cy_gal_img">
									<img src="<?php echo base_url();?>assets/routes/50km_map.jpg" alt="Route" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="<?php echo base_url();?>assets/routes/50km_map.jpg" class="fa fa-search"></a>
										
									</div>
								</div>
								
								<div class="cy_event_data">
							<p style="text-align:center;">
							Start at IMA Bhavan - Take a U turn from the Turn around sign at the Bhairawnath Devasthaan Kamaan (nearly 10 KM before Takli Dhokeshwar) Note -
							Do 4 loops to Finish at the start point
							</p>
								</div>
							</div>
						
						</div>
					</div>
					<span class="col-lg-3"></span>
				</div>
				
				
					
                </div>
            </div>   -->
              
			</div>		
		</div>			
			</div>
               </div>
        </div>
    </div>
	</div>
    <!--Footer section start-->
