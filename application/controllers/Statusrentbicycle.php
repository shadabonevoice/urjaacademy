<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include ("class.phpmailer.php");
include ("class.smtp.php");
class Statusrentbicycle extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('security');
        $this->load->library('upload');
        $this->load->library('email');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('common_model');
    }
    public function index() {
       //  if(empty($this->input->post('status')) || $this->input->post('status') == '') {
       //     redirect('rentBicycle');
       // }
        $status = $this->input->post('status');
        $firstname = $this->input->post('firstname');
        $amount = $this->input->post('amount');
        $txnid = $this->input->post('txnid');
        $posted_hash = $this->input->post('hash');
        $key = $this->input->post('key');
        $productinfo = $this->input->post('productinfo');
        $email = $this->input->post('email');
        $salt = "hOTRLB5LMS"; //  Your salt hOTRLB5LMS
        $add = $this->input->post('additionalCharges');
        $myphone = $this->input->post('phone');
        $myaddr = $this->input->post('address1');
        $myage = $this->input->post('age');
        If (isset($add)) {
            $additionalCharges = $this->input->post('additionalCharges');
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $data['hash'] = hash("sha512", $retHashSeq);
        $data['amount'] = $amount;
        $data['firstname'] = $firstname;
        $data['txnid'] = $txnid;
        $data['posted_hash'] = $posted_hash;
        $data['status'] = $status;
        $data['mailid'] = $email;
        $data['phoneno'] = $myphone;
        $data['address'] = $myaddr;
        $data['age'] = $myage;
        
        
        if ($status == 'success') {
            $data['header_title']=headTitle;
        $data['page_title']="Success";
        $this->load->view('success_rent_bicycle', $data);
           // $this->load->view('footer');
            $sess_data1 = $this->session->userdata();
            //print_r($sess_data1); exit;
            // Name / Email / tax id
            $mailname = $sess_data1['name'];
            $mailids = $sess_data1['mailid'];
            $myid = $sess_data1['tid'];
            $bikecount;
            $maxidd = $this->db->query('SELECT MAX(rent_id) AS `maxidd` FROM `tbl_rentbicycle`')->row()->maxidd;
            if ($maxidd <= 0) {
                $bikecount = 125;
            } else {
                $maxidd = $this->db->query('SELECT MAX(rent_id) AS `maxidd` FROM `tbl_rentbicycle`')->row()->maxidd;
                $bid = $this->db->select('bicycle_count as bid')->from('tbl_rentbicycle')->where('rent_id', $maxidd)->get();
                $bid1 = $bid->row()->bid;
                $bikecount = $bid1 - 1;
            }
            //recipt no
            $mycat_slice = substr(54354564, 0, 2); //print_r($mycat_slice);exit;
            $mycat_slice = substr($myid, 0, 2);
            $randomno = rand(100, 999);
            $reciept_no = $mycat_slice . $randomno;
            
            
            $query = $this->db->get_where('tbl_rentbicycle', array('transaction_id' => $myid));
            $foundRows = $query->num_rows();
            // if($foundRows >= 1) {
            //  redirect(base_url('index.php/index'));
            // }
            // else 
            {
            
            
           // $success_msg="Your Birth Application Submitted Sucessfully";
                $success_msg="Dear ".$mailname.",
You have successfully ordered bicycle on rent for the ahmednagar cyclothon 2019-Season2
Your Riceipt ID - ".$reciept_no."
For updates, follow us on - https://bit.ly/2ApDHNp or call - 8308054000";
             $this->common_model->sendSMS($sess_data1['phoneno'],$success_msg); 
            $myinsert_array = array('transaction_id' => $sess_data1['tid'], 'rent_name' => $sess_data1['name'], 'rent_mobile' => $sess_data1['phoneno'], 'rent_email' => $sess_data1['mailid'], 'rent_gender' => $sess_data1['productinfo'], 'rent_age' => $sess_data1['age'], 'rent_amount' => $sess_data1['amount'], 'rent_address' => $sess_data1['address'], 'bicycle_count' => $bikecount, 'reciept_no' => $reciept_no,'dateOfBirth' =>$sess_data1['dateofbirth'], 'created_at' => date('Y-m-d H:i:s'));
            //print_r($myinsert_array); exit;
            $updatedatarent = array(
                "status" =>"success"
            );
            
             $result22=$this->common_model->update_rent_email_status($this->session->userdata('userrenttxnid'),$updatedatarent);
            //if ($this->common_model->add_records('tbl_register', $insert_array)) {
              if($result22){
           // if ($this->common_model->add_records('tbl_rentbicycle', $myinsert_array)) {
                $this->session->sess_destroy();
                
                $this->session->set_flashdata('success_rent_bicycle', 'Your Birth Application Submitted Sucessfully');
                $this->sendRentBicycleRegistrationMail($sess_data1['mailid'],$reciept_no,$mailname);
                
            } else {
                $this->session->set_flashdata('error', 'Error While Submitting ');
            }
            }
        } else {
            $data['header_title']=headTitle;
       $data['page_title']="Failure";
         $this->load->view('failure_rent_bicycle', $data);
           // $this->load->view('footer');
        }
    }
    public function sendRentBicycleRegistrationMail($mail_id,$reciept_no,$mailname){
         $this->config->load('fetchemail', TRUE);
$adminemail = $this->config->item('admin_email', 'fetchemail'); 
$mail = new PHPMailer ();
        $mail->IsSMTP (); // set mailer to use SMTP
        $mail->Host = smtpHost; // specify main and backup server
        $mail->Port = smtpPortNo; // set the port to use
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = smtpUserName; // your SMTP username or your gmail username
        $mail->Password = smtpPassword; // your SMTP password or your gmail password
        
        $from = contactAdminEmail; // Reply to this email
        $to= $mail_id; // Recipients email ID
        $to2= mailCopy; // Recipients email ID
        $name = ""; // Recipient's name
        $mail->From = $from;
        $mail->FromName = smtpFromName; // Name to indicate where the email came from when the recepient received
        $mail->AddAddress ( $to, $name );
        $mail->AddAddress ( $to2, $name );
        $mail->AddReplyTo ( $from, smtpFromName );
        $mail->IsHTML ( true ); // send as HTML
        $mail->Subject = "Rent a Bicycle - Ahmednagar Cyclothon 2019 -Season 2  ";
                   $data['reciept_no']=$reciept_no;
                $data['mailname']=$mailname;
       $htmlMessage = $this->load->view('user_register/registration_email_rent_bicycle', $data, TRUE);
        $mail->Body = $htmlMessage ; // HTML Body
        $mail->AltBody = strip_tags (""); // Text Body
        $mail->Sender = contactAdminEmail;
                $mail->Send();
    }
}
