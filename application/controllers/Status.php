<?php
defined('BASEPATH') OR exit('No direct script access allowed');
     include ("class.phpmailer.php");
include ("class.smtp.php");

class Status extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('security');
        $this->load->library('upload');
        $this->load->library('email');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('common_model');
    }
    public function index() {

       // if(empty($this->input->post('status')) || $this->input->post('status') == '') {
       //     redirect(base_url());
       // }
        $status = $this->input->post('status');
        $firstname = $this->input->post('firstname');
        $amount = $this->input->post('amount');
        $txnid = $this->input->post('txnid');
        $posted_hash = $this->input->post('hash');
        $key = $this->input->post('key');
        $productinfo = $this->input->post('productinfo');
        $email = $this->input->post('email');
        $salt = "hOTRLB5LMS"; //  Your salt
        $add = $this->input->post('additionalCharges');
        $myphone = $this->input->post('phone');
        $myaddr = $this->input->post('address1');
        $mystate = $this->input->post('state');
        $mycity = $this->input->post('city');
        $mycountry = $this->input->post('country');
        $mygender = $this->input->post('gender');
        $mypostcode = $this->input->post('zipcode');
        $mybirthdate = $this->input->post('birthdate');
        $myage = $this->input->post('age');
        $mybloodgrp = $this->input->post('bloodgrp');
        $mytshirt = $this->input->post('tshirt');
        $myemergency_name = $this->input->post('uremrname');
        $myemergency_number = $this->input->post('uremrnumber');
        If (isset($add)) {
            $additionalCharges = $this->input->post('additionalCharges');
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {
            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
        $data['hash'] = hash("sha512", $retHashSeq);
        $data['amount'] = $amount;
        $data['firstname'] = $firstname;
        $data['txnid'] = $txnid;
        $data['posted_hash'] = $posted_hash;
        $data['status'] = $status;
        $data['mailid'] = $email;
        $data['phoneno'] = $myphone;
        $data['address'] = $myaddr;
        $data['state'] = $mystate;
        $data['city'] = $mycity;
        $data['zipcode'] = $mypostcode;
        $data['country'] = $mycountry;
        $data['gender'] = $mygender;
        $data['birthdate'] = $mybirthdate;
        $data['age'] = $myage;
        $data['bloodgrp'] = $mybloodgrp;
        $data['tshirt'] = $mytshirt;
        $data['uremrname'] = $myemergency_name;
        $data['uremrnumber'] = $myemergency_number;
            
        if ($status == 'success') {
   
        $data['header_title']=headTitle;
       $data['page_title']="Success";
          $this->load->view('success', $data);
            $sess_data = $this->session->userdata();
            //$myid= rand(10,500);
            $maxid = $this->db->query('SELECT MAX(registration_no) AS `maxid` FROM `tbl_register`')->row()->maxid;
            $myid11 = $maxid + 1;
            $myid = sprintf("%02d", $myid11);
            $mailname = $sess_data['name'];
            $mailids = $sess_data['mailid'];
            $mycat = $sess_data['productinfo'];
            //$success_msg= "Hi".$mailname.", Thank You for Registering ".$mycat." Race Category for the Ahmednagar Cyclothon 2018 - Season Two";
            $success_msg= "Dear ".$mailname.",
You have successfully registered for the Ahmednagar Cyclothon 2019 - Season 2.
Your Ride - ".$mycat."
For updates, follow us on - https://bit.ly/2ApDHNp or call - 8308054000";
             $this->common_model->sendSMS($sess_data['phoneno'],$success_msg);  
              
            $mycat_slice = substr($mycat, 0, -2); // returns "abcde"
            $bibstring = $mycat_slice . $myid;
            $trans_id = $sess_data['tid'];
            
            $query = $this->db->get_where('tbl_register', array('transaction_id' => $trans_id));
            $foundRows = $query->num_rows();
            if($foundRows >= 1) {
             redirect(base_url());
            } else {
             
            $insert_array = array('bibid' => $bibstring, 'transaction_id' => $sess_data['tid'], 'total_amount' => $sess_data['amount'], 'name' => $sess_data['name'], 'race_category' => $sess_data['productinfo'], 'email_addr' => $sess_data['mailid'], 'mobile_no' => $sess_data['phoneno'], 'full_address' => $sess_data['address'], 'state' => $sess_data['state'], 'city' => $sess_data['city'], 'postcode' => $sess_data['postcode'], 'country' => $sess_data['country'], 'gender' => $sess_data['gender'], 'birthdate' => $sess_data['birthdate'], 'age' => $sess_data['age'], 'bloodgrp' => $sess_data['bloodgrp'], 'tshirt' => $sess_data['tshirt'], 'emergency_name' => $sess_data['uremrname'], 'emergency_number' => $sess_data['uremrnumber'], 'create_at' => date('Y-m-d H:i:s'));
             $updatedatarent = array(
                "status" =>"success"
            );
              $result2=$this->common_model->update_registration_email_status($this->session->userdata('usertxnid'),$updatedatarent);
            //if ($this->common_model->add_records('tbl_register', $insert_array)) {
              if($result2){
                $this->session->sess_destroy();
                
                $this->session->set_flashdata('success', 'Your Birth Application Submitted Sucessfully');
                 $this->sendRegistrationMail($sess_data['mailid'],$mycat,$bibstring,$mailname);
              
            } else {
                $this->session->set_flashdata('error', 'Error While Submitting');
            }
            }
        } else {
            $data['header_title']=headTitle;
       $data['page_title']="Failure";
         $this->load->view('failure', $data);
            //$this->load->view('footer');
        }
    }
    public function sendRegistrationMail($mail_id,$mycat,$bibstring,$mailname){
         $this->config->load('fetchemail', TRUE);
$adminemail = $this->config->item('admin_email', 'fetchemail'); 
$mail = new PHPMailer ();
        $mail->IsSMTP (); // set mailer to use SMTP
        $mail->Host = smtpHost; // specify main and backup server
        $mail->Port = smtpPortNo; // set the port to use
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = smtpUserName; // your SMTP username or your gmail username
        $mail->Password = smtpPassword; // your SMTP password or your gmail password
        
        $from = contactAdminEmail; // Reply to this email
        $to= $mail_id; // Recipients email ID
        $to2= mailCopy; // Recipients email ID
        $name = ""; // Recipient's name
        $mail->From = $from;
        $mail->FromName = smtpFromName; // Name to indicate where the email came from when the recepient received
        $mail->AddAddress ( $to, $name );
        $mail->AddAddress ( $to2, $name );
        $mail->AddReplyTo ( $from, smtpFromName );
        $mail->IsHTML ( true ); // send as HTML
        $mail->Subject = "Registration - Ahmednagar Cyclothon 2019 -Season 2";
                   $data['mailname']=$mailname;
                $data['mycat']=$mycat;
                 $data['bibstring']=$bibstring;
       $htmlMessage = $this->load->view('user_register/registration_email', $data, TRUE);
        $mail->Body = $htmlMessage ; // HTML Body
        $mail->AltBody = strip_tags (""); // Text Body
        $mail->Sender = contactAdminEmail;
                $mail->Send();
    }
}