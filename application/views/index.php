<?php
$this->load->view('common/header.php');
?>
    <!--Loader Start-->
    <div class="loader_wrapper"> 
        <div class='loader'>
			<img src="<?php echo base_url(); ?>assets/images/loader.gif" alt="loader">	
        </div>
    </div>
    <!--Top bar start-->
   
    
    <!--Banner section start-->
    <div class="cy_banner_wrapper">
        <div class="container-fluid">
            <div class="row">
                <div id="rev_slider_1068_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="levano4export" data-source="gallery" style="background-color:transparent;padding:0px;">
                    <div id="rev_slider_1068_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">
                        <ul>
                            <li data-index="rs-1596" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="500" data-thumb="" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo base_url(); ?>assets/images/slider2019.png" alt="banner" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-1596-layer-1" data-x="['left','left','left','left']" data-hoffset="['-110','50','50','50']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['65','55','45','45']" data-lineheight="['85','55','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2500,"to":"o:1;","delay":1000,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,20,20,20]" style="z-index: 5; white-space: nowrap;text-transform:left;color:#ffffff;font-family: 'Montserrat', sans-serif;text-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2);text-transform:uppercase;">
								
								Get Ready<br> for <br> Ahmednagar <br>Cyclothon 2019<br> <span style="color:#ff0000;"> Season Two</span>
						<!--	<a href="<?php echo base_url(); ?>/promoRegister><button class="cy_button next">Your Feedback</button><a>-->
								<!--	-->
							<br>
								
								<a style="padding: 5px 34px;font-size: 15px !important;" href="<?php echo base_url(); ?>promoRegister" class="cy_button">
								    <b style="font-size: 15px !important;">Republic Day Ride - Register Here</b></a>
							
								
								
                                </div>
                            </li>
							
							 <!--slide2-->
                            <li data-index="rs-3010" data-transition="zoom" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1000" data-thumb="" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo base_url(); ?>assets/js/plugin/revolution/assets/main_banner.jpg" alt="banner" data-bgposition="center center" data-kenburns="on" data-duration="2000" data-ease="Power2.easeOut" data-scalestart="110" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 500" data-offsetend="0 -500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-1596-layer-2" data-x="['left','left','left','left']" data-hoffset="['-110','50','50','50']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['65','55','45','45']" data-lineheight="['85','55','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2500,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,20,20,20]" style="z-index: 5; white-space: nowrap;text-transform:left;color:#ffffff;font-family: 'Montserrat', sans-serif;text-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2);text-transform:uppercase;">don't limit <br> your  challenges,<br> <span style="color:#ff0000;">challenge</span><br> your limits!
                                </div>      
                            </li>
							
							<li data-index="rs-4596" data-transition="zoomout" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="500" data-thumb="" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo base_url(); ?>assets/js/plugin/revolution/assets/main_banner1.jpg" alt="banner" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-1596-layer-1" data-x="['left','left','left','left']" data-hoffset="['-110','50','50','50']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['65','55','45','45']" data-lineheight="['85','55','50','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2500,"to":"o:1;","delay":1000,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,20,20,20]" style="z-index: 5; white-space: nowrap;text-transform:left;color:#ffffff;font-family: 'Montserrat', sans-serif;text-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2);text-transform:uppercase;"> WHEN IN DOUBT,
                                    <br> <span style="color:#ff0000;">PEDAL</span> IT OUT!<br>
                                </div>
                            </li>
                         
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
	
	
    <!--Menus Start-->
    
   
    <!--About section start-->
    <div class="cy_about_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="cy_about_img">
                        <img src="<?php echo base_url(); ?>assets/images/about/about.png" alt="about" class="img-fluid"/>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="cy_about_data">
                        <h1 class="cy_heading">ABOUT US</h1>
                        <p>
						Ahmednagar Cyclothon is officially affiliated with <b>Cycling Federation of India(CFI)</b>, <b>Kalpattaru Enterprises India Limited</b> and <b>Cycling Association of Maharashtra(CAOM)</b>.
						The Ahmednagar Cycling Club is dedicated to encouraging the enjoyment of all types of
							cycling in the Ahmednagar community. The Club was formed to promote the cause of
							keeping our city green, while keeping our bodies healthy and clean. We welcome
							members of the community to our club events such as group rides, Cyclothons and
							community service activities.
						</p>
						<center>
						<h5 class="cy_heading">PROUDLY AFFILIATED WITH</h5>
						
						<img src="assets/images/cfi.jpg" alt="event" class="img-fluid">
						<img src="assets/images/maha.png" alt="event" class="img-fluid">
						</center>
						</br>
                        <a href="about" class="cy_button">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
	 <div class="cy_achieve_wrapper">
        <div class="cy_achieve_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="cy_achieve_img wow cy_anim_left">
                        <img src="<?php echo base_url(); ?>assets/images/achieve/cycle.png" alt="achievement" class="img-fluid" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="cy_counter_wrapper">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <h1 class="cy_heading">Why Cycle?</h1>
                            </div>
							
							
							
                            <div class="col-lg-12 col-md-12">
                                <div class="cy_count_box" style="margin-top:0px;">
								 
								 
								 <div class="widget widget_categories">
                           
									<ul>
										<li class="whycycle" >Cycling provides several health benefits.</li>
										<li class="whycycle" >Cycling is one of the best modes of transportation that is fast, safe and pollution-free.</li>
										
									</ul>
								</div>
								 
								 
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <!--Events Sevtion Start-->
   <!-- <div class="cy_event_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Upcoming Event</h1>
                </div>
            </div>
  <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/event1.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 6:00 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>

                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2  style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 20KM Cyclothon</h2>
							   <center>
							 <a href="<?php echo base_url(); ?>index.php/welcome" class="cy_button">Registration Fee: </b>&#8360; 400/-</a>
                            </center>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/event2.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 5:45 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2  style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 50KM Cyclothon</h2>
							  <center>
							 <a href="<?php echo base_url(); ?>index.php/welcome" class="cy_button">Registration Fee: </b>&#8360; 500/-</a>
                            </center>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/event3.jpg" alt="event" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> Reporting : 5:30 AM </li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>IMA Bhavan</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><b style="color:#ff0000">Race Category:</b> 100km Cyclothon</h2>
							
							 <center>
							 <a href="<?php echo base_url(); ?>index.php/welcome" class="cy_button">Registration Fee: </b>&#8360; 600/-</a>
                            </center>
							
                           
                        </div>
                    </div>
                </div>
            </div> 
			 </div>
    </div>-->
	
	
	 <div class="cy_gallery_wrapper cy_section_padding padder_bottom100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="cy_heading">Our gallery</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image1.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image1.jpg" class="fa fa-search"></a>
                           
                        </div>
                    </div>
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image2.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image2.jpg" class="fa fa-search"></a>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/large2.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/large2.jpg" class="fa fa-search"></a>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image4.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image4.jpg" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image5.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image5.jpg" class="fa fa-search"></a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/fullarge1.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/fullarge1.jpg" class="fa fa-search"></a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image7.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image7.jpg" class="fa fa-search"></a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/image8.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/image8.jpg" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="cy_gal_img">
                                <img src="<?php echo base_url(); ?>assets/images/gallery/fullarge2.jpg" alt="gallery" class="img-fluid" />
                                <div class="img_gal_ovrly"></div>
                                <div class="gal_buttons">
                                    <a href="<?php echo base_url(); ?>assets/images/gallery/fullarge2.jpg" class="fa fa-search"></a>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/large1.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/large1.jpg" class="fa fa-search"></a>
                          
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image6.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image6.jpg" class="fa fa-search"></a>
                           
                        </div>
                    </div>
                    <div class="cy_gal_img">
                        <img src="<?php echo base_url(); ?>assets/images/gallery/image3.jpg" alt="gallery" class="img-fluid" />
                        <div class="img_gal_ovrly"></div>
                        <div class="gal_buttons">
                            <a href="<?php echo base_url(); ?>assets/images/gallery/image3.jpg" class="fa fa-search"></a>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
	
	
	 <!--Cycle Tour section start-->
    <div class="cy_tour_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="cy_tour_data">
                        <h1>RENT A BICYCLE FOR AHMEDNAGAR CYCLOTHON 2019!</h1>
                        <a href="rentBicycle" class="cy_button">RENT BICYCLE</a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <div class="cy_tour_heading">
                        <h1>Ahmednagar cyclothon</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!--Footer section start-->
   <?php
$this->load->view('common/footer');
        ?>