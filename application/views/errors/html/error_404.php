<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <base href="<?php echo $base_url = load_class('Config')->config['base_url'];?>"></base>
    <title>Nagarcycling 404 | Page Not Found</title>
    <meta name="author" content="Nagarcycling">
    <meta name="description" content="404 error. Sorry, but this page does not exist on Nagarcycling.">
    <meta name="keywords" content="Nagarcycling">
    <link rel="shortcut icon" href="assets/favicon.png">
    <link rel="stylesheet" href="assets/css/404style.css">
</head>
<body>
    <div class="box">
        <div class="box-content">
           <img src="assets/images/404.png" height="250px">
           <p>This page does not exist on Nagarcycling.</p>
           <br>
           <a href="<?php echo $base_url;?>" id="gotolink">Go to Nagarcycling</a>
        </div>
    </div>
</body>     
<style>

    </style>
    </body>
</html>
