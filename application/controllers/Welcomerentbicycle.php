<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcomerentbicycle extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('upload');
		$this->load->library('email');
		$this->load->helper('form');
		$this->load->library('session');  
		$this->load->model('common_model');
		
    }

	public function index()
	{
		
		redirect('rentBicycle');
		$this->load->view('registration_rent_bicycle');	
		//$this->load->view('footer');	
		
        	
	}

	public function check()
	{
		
		$amount =  $this->input->post('payble_amount');
	    $product_info = $this->input->post('product_info');
	    $customer_name = $this->input->post('customer_name');
	    $customer_emial = $this->input->post('customer_email');
	    $customer_mobile = $this->input->post('mobile_number');
	    $customer_address = $this->input->post('customer_address');
	    $birthdate_rent_bicycle_date = $this->input->post('birthdate_rent_bicycle');
	    $birthdate_rent_bicycle=date("m/d/Y",strtotime($birthdate_rent_bicycle_date));
		$age = $this->input->post('age');
	date_default_timezone_set('Asia/Kolkata');
		
	    
	    	//payumoney details
	    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	         $maxidd = $this->db->query('SELECT MAX(rent_id) AS `maxidd` FROM `tbl_rentbicycle`')->row()->maxidd;
            if ($maxidd <= 0) {
                $bikecount = 125;
            } else {
                $maxidd = $this->db->query('SELECT MAX(rent_id) AS `maxidd` FROM `tbl_rentbicycle`')->row()->maxidd;
                $bid = $this->db->select('bicycle_count as bid')->from('tbl_rentbicycle')->where('rent_id', $maxidd)->get();
                $bid1 = $bid->row()->bid;
                $bikecount = $bid1 - 1;
            }
            $mycat_slice = substr(54354564, 0, 2); //print_r($mycat_slice);exit;
            $mycat_slice = substr($txnid, 0, 2);
            $randomno = rand(100, 999);
            $reciept_no = $mycat_slice . $randomno;
			$myinsert_array = array('transaction_id' => $txnid, 'rent_name' => $customer_name, 'rent_mobile' => $customer_mobile, 'rent_email' => $customer_emial, 'rent_gender' => $product_info, 'rent_age' => $age, 'rent_amount' => $amount, 'rent_address' => $customer_address, 'bicycle_count' => $bikecount, 'reciept_no' => $reciept_no,'dateOfBirth' =>$birthdate_rent_bicycle, 'created_at' => date('Y-m-d H:i:s'));
			$this->common_model->add_records('tbl_rentbicycle', $myinsert_array);
			$MERCHANT_KEY = "v98vlTWB"; //change  merchant with yours
	        $SALT = "hOTRLB5LMS";  //change salt with yours 
		
		
					//Live
	        //$MERCHANT_KEY = "v98vlTWB"; //change  merchant with yours
	       // $SALT = "hOTRLB5LMS";  //change salt with yours 

	        
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        
	         $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
	         $hash = strtolower(hash('sha512', $hashstring));
	         
	       $success = base_url() .'index.php/Statusrentbicycle';  
	        $fail = base_url() .'index.php/Statusrentbicycle';
	        $cancel = base_url() .'index.php/Statusrentbicycle';
	         $this->session->unset_userdata('userrenttxnid');
	        $this->session->set_userdata('userrenttxnid',$txnid);
	       // print_r($_POST);exit;
	       $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'productinfo' => $product_info,
	            'mailid' => $customer_emial,
	            'phoneno' => $customer_mobile,
	            'dateofbirth' => $birthdate_rent_bicycle,
	            'address' => $customer_address,
				'age' => $age,
	            'action' => "https://secure.payu.in", //for live change action  https://secure.payu.in
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel            
	        );
		
			$this->session->set_userdata($data);
			//print_r($data); exit;
			$data['header_title']=headTitle;
		$data['page_title']="Rent a bicycle";
	        $this->load->view('confirmation_rent_bicycle',$data);  
		//	$this->load->view('footer');			
     
	}

	public function help()
	{
		
		$this->load->view('help');
		$this->load->view('footer');
	}
}
