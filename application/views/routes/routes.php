<?php
$this->load->view('common/header.php');
?>

   <!--Events Section Start-->
    <div class="cy_event_wrapper cy_event_single">
        <div class="container">
            <div class="row">
			
			
			
			
			 <div class="col-lg-12 col-md-12">
                    <div class="cy_event_box">
                       <!-- <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>-->
                    <!--    <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map Coming Soon....</a></h2>
                           
                            
                        </div>
						
					</div></div>-->
			
			
			
			
                <!--<div class="col-lg-6 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map for 20KM</a></h2>
                           
                            
                        </div>
						
					</div></div>
						
						 <div class="col-lg-6 col-md-12">
                    <div class="cy_event_box">
                  
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map for 50KM</a></h2>
                           
                            
                        </div>
						
						</div></div>
                    
                	
						 <div class="col-lg-6 col-md-12">
                    <div class="cy_event_box">
                        <div class="cy_event_img">
                            <img src="<?php echo base_url();?>assets/images/event/route1.jpg" alt="event single" class="img-fluid" />
                            <div class="cy_event_detail">
                                <div class="cy_event_time">
                                    <ul>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/clock.svg" alt="event time"></i> 5:00 AM to 11:00AM</li>
                                        <li><i><img src="<?php echo base_url();?>assets/images/svg/map.svg" alt="event address"></i>Ahmednagar</li>
                                    </ul>
                                </div>
                                <div class="cy_event_date">
                                    <span class="ev_date">04 march</span>
                                    <span class="ev_yr">2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cy_event_data">
                            <h2 style="text-align:center;"><a href="#">Route Map for 100KM</a></h2>
                           
                            
                        </div>
                    </div>
					</div>-->
					
		   <div class="cy_gallery_wrapper cy_section_padding padder_bottom100">
        <div class="container">			
					
			<div class="row">
               <div class="col-lg-12 col-md-12">
			
				
				<div class="row">
					<div class="col-lg-6 col-md-12">
					<center><h4 class="cy_heading">Route Map For 20KM</h4></center>
						<div class="row">
						
							<div class="col-lg-12 col-md-12">
								<div class="cy_gal_img">
									<img src="<?php echo base_url();?>assets/routes/10km_map.jpg" alt="Route" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="<?php echo base_url();?>assets/routes/10km_map.jpg" class="fa fa-search"></a>
										
									</div>
								</div>
								
								<div class="cy_event_data">
                            <p style="text-align:center;">
							Start at IMA Bhavan - Take a U turn from the Turn around sign after crossing Jakhangaon
							</p>
                           
                            
                        </div>
								
							</div>
						
						</div>
						
					</div>	
					
					
				<div class="col-lg-6 col-md-12">
				<center><h4 class="cy_heading">Route Map For 50KM</h4></center>
				<center>
                 <div class="row">
						
							<div class="col-lg-12 col-md-12">
								<div class="cy_gal_img">
									<img src="<?php echo base_url();?>assets/routes/50km_map.jpg" alt="Route" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="<?php echo base_url();?>assets/routes/50km_map.jpg" class="fa fa-search"></a>
										
									</div>
								</div>
								<div class="cy_event_data">
								<p style="text-align:center;">
							Start at IMA Bhavan - Take a U turn from the Turn around sign at the Bhairawnath Devasthaan Kamaan (nearly 10 KM before Takli Dhokeshwar)
							Finish at the start point

							</p>
							</div>	
							</div>
						
						</div>
					</center>
                </div>
				</div>
				
				
				<div class="row">
				<span class="col-lg-3"></span>
					<div class="col-lg-6 col-md-12">
					<center><h4 class="cy_heading">Route Map For 100KM</h4></center>
						<div class="row">
						
							<div class="col-lg-12 col-md-12">
								<div class="cy_gal_img">
									<img src="<?php echo base_url();?>assets/routes/50km_map.jpg" alt="Route" class="img-fluid" />
									<div class="img_gal_ovrly"></div>
									<div class="gal_buttons">
										<a href="<?php echo base_url();?>assets/routes/50km_map.jpg" class="fa fa-search"></a>
										
									</div>
								</div>
								
								<div class="cy_event_data">
							<p style="text-align:center;">
							Start at IMA Bhavan - Take a U turn from the Turn around sign at the Bhairawnath Devasthaan Kamaan (nearly 10 KM before Takli Dhokeshwar) Note -
							Do 4 loops to Finish at the start point
							</p>
								</div>
							</div>
						
						</div>
					</div>
					<span class="col-lg-3"></span>
				</div>
				
				
					
                </div>
           </div>   
              
			</div>		
		</div>			
			</div>
               </div>
        </div>
    </div>
</div>
    <!--Footer section start-->
    <?php
$this->load->view('common/footer');
        ?>
