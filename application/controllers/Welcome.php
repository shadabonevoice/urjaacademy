<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include ("class.phpmailer.php");
include ("class.smtp.php");
class Welcome extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
       $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('security');
        $this->load->library('upload');
        $this->load->library('email');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('common_model');
    }

	public function index()
	{
		redirect('cyclothonRegister');
		$data['header_title']=headTitle;
		$data['page_title']="Registration";
		$this->load->view('user_register/registration',$data);	
		//$this->load->view('registration');	
		//$this->load->view('footer');	
		
        	
	}
	public function testsms(){
		$mob=7875426234;
		$success_msg="Thank You for Registering for the Ahmednagar Cyclothon 2018";
             $this->common_model->sendSMS($mob,$success_msg);  
	}
	public function send_email(){
$this->config->load('fetchemail', TRUE);
$adminemail = $this->config->item('admin_email', 'fetchemail'); 
$mail = new PHPMailer ();
        $mail->IsSMTP (); // set mailer to use SMTP
        $mail->Host = "mail.deltainfomedia.com"; // specify main and backup server
        $mail->Port = 25; // set the port to use
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
        
        $from = "support@nagarcycling.com"; // Reply to this email
        $to= "shadabjnb786y@yahoo.com"; // Recipients email ID
        $name = ""; // Recipient's name
        $mail->From = $from;
        $mail->FromName = smtpFromName; // Name to indicate where the email came from when the recepient received
        $mail->AddAddress ( $to, $name );
        $mail->AddReplyTo ( $from, smtpFromName );
        $mail->IsHTML ( true ); // send as HTML
        $mail->Subject = "Feedback- Ahmednagar Cyclothon";

       $htmlMessage ="hieee";
        $mail->Body = $htmlMessage ; // HTML Body
        $mail->AltBody = strip_tags (""); // Text Body
        $mail->Sender = $from;
                if($mail->Send()){
                	echo "send";
                }
                else{
                	echo "not sendd";
                }
    }
    public function send_email1111(){
$this->config->load('fetchemail', TRUE);
$adminemail = $this->config->item('admin_email', 'fetchemail'); 
$mail = new PHPMailer ();
        $mail->IsSMTP (); // set mailer to use SMTP
        $mail->Host = smtpHost; // specify main and backup server
        $mail->Port = smtpPortNo; // set the port to use
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->Username = smtpUserName; // your SMTP username or your gmail username
        $mail->Password = smtpPassword; // your SMTP password or your gmail password
        
        $from = $adminemail; // Reply to this email
        $to= "shadabjnb786y@yahoo.com"; // Recipients email ID
        $name = ""; // Recipient's name
        $mail->From = $from;
        $mail->FromName = smtpFromName; // Name to indicate where the email came from when the recepient received
        $mail->AddAddress ( $to, $name );
        $mail->AddReplyTo ( $from, smtpFromName );
        $mail->IsHTML ( true ); // send as HTML
        $mail->Subject = "Feedback- Ahmednagar Cyclothon";

       $htmlMessage ="hieee";
        $mail->Body = $htmlMessage ; // HTML Body
        $mail->AltBody = strip_tags (""); // Text Body
        $mail->Sender = $adminemail;
                if($mail->Send()){
                	echo "send";
                }
                else{
                	echo "not send";
                }
    }
	    public function send_email_old(){
         $from_email = "ahmednagarcyclothon@nagarcycling.com"; 
         $to_email = "shadab@onevoicetransmedia.com"; 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Your Name'); 
         $this->email->to($to_email);
         $this->email->subject('Email Test'); 
         $this->email->message('Testing the email class.'); 
                        if($this->email->send() == true){
                            echo "sendd";
                        }
                        else{
                            echo "not send";
                        }
    }
	public function check()
	{
		
		$amount =  $this->input->post('payble_amount');
	    $product_info = $this->input->post('product_info');
	    $customer_name = $this->input->post('customer_name');
	    $customer_emial = $this->input->post('customer_email');
	    $customer_mobile = $this->input->post('mobile_number');
	    $customer_address = $this->input->post('customer_address');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$postcode = $this->input->post('postcode');
		$country = $this->input->post('country');
		$gender = $this->input->post('gender');
		$birthdate = $this->input->post('birthdate');
		$age = $this->input->post('age');
		$bloodgrp = $this->input->post('bloodgrp');
		$tshirt = $this->input->post('tshirt');
		$uremrname = $this->input->post('uremrname');
		$uremrnumber = $this->input->post('uremrnumber');
		date_default_timezone_set('Asia/Kolkata');
	    $maxid = $this->db->query('SELECT MAX(registration_no) AS `maxid` FROM `tbl_register`')->row()->maxid;
            $myid11 = $maxid + 1;
            $myid = sprintf("%02d", $myid11);
            $mycat = $product_info;
             $mycat_slice = substr($mycat, 0, -2); // returns "abcde"
              $bibstring = $mycat_slice . $myid;
               $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	       $insert_array = array('bibid' => $bibstring, 'transaction_id' => $txnid, 'total_amount' => $amount, 'name' => $customer_name, 'race_category' => $product_info, 'email_addr' => $customer_emial, 'mobile_no' => $customer_mobile, 'full_address' => $customer_address, 'state' => $state, 'city' => $city, 'postcode' => $postcode, 'country' => $country, 'gender' => $gender, 'birthdate' => $birthdate, 'age' => $age, 'bloodgrp' => $bloodgrp, 'tshirt' => $tshirt, 'emergency_name' => $uremrname, 'emergency_number' => $uremrnumber, 'create_at' => date('Y-m-d H:i:s'));
	       $this->common_model->add_records('tbl_register', $insert_array);
	       $this->session->unset_userdata('usertxnid');
	        $this->session->set_userdata('usertxnid',$txnid);
	    	//payumoney details
	    
	    
	        $MERCHANT_KEY = "v98vlTWB"; //change  merchant with yours
	        $SALT = "hOTRLB5LMS";  //change salt with yours 

	        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	        //optional udf values 
	        $udf1 = '';
	        $udf2 = '';
	        $udf3 = '';
	        $udf4 = '';
	        $udf5 = '';
	        
	         $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
	         $hash = strtolower(hash('sha512', $hashstring));
	         
	       $success = base_url() .'index.php/Status';  
	        $fail = base_url() .'index.php/Status';
	        $cancel = base_url() .'index.php/Status';
	        
	       // print_r($_POST);exit;
	       $data = array(
	            'mkey' => $MERCHANT_KEY,
	            'tid' => $txnid,
	            'hash' => $hash,
	            'amount' => $amount,           
	            'name' => $customer_name,
	            'productinfo' => $product_info,
	            'mailid' => $customer_emial,
	            'phoneno' => $customer_mobile,
	            'address' => $customer_address,
				'state' => $state,
				'city' => $city,
				'postcode' => $postcode,
				'country' => $country,
				'gender' => $gender,
				'birthdate' => $birthdate,
				'age' => $age,
				'bloodgrp' => $bloodgrp,
				'tshirt' => $tshirt,
				'uremrname' => $uremrname,
				'uremrnumber' => $uremrnumber,
	            'action' => "https://secure.payu.in", //for live change action  https://secure.payu.in
	            'sucess' => $success,
	            'failure' => $fail,
	            'cancel' => $cancel            
	        );
	        
			 $data['header_title']=headTitle;
		$data['page_title']="Registration";
			$this->session->set_userdata($data);
			//print_r($data); exit;
	        $this->load->view('confirmation',$data);   
     
	}

	public function help()
	{
		
		$this->load->view('help');
		$this->load->view('footer');
	}
}
